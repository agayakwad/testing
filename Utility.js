// This file contains the Utility functions which are used as the helper functions.

//
// Helper function for multiselect dropdown
//

function getMultiSelectDropDownList(valueArray, idArray, divTagId) {

    var HiddenVirtualPath = document.getElementById("HiddenFieldVirtualPath");
    if (HiddenVirtualPath == null || HiddenVirtualPath == "undefined") {
       if(GetClientId("HiddenFieldVirtualPath") != null)
           HiddenVirtualPath = document.getElementById(GetClientId("HiddenFieldVirtualPath"));
    }
    if (HiddenVirtualPath != null)
        virtualPath = HiddenVirtualPath.value
       
    //virtualPath = getVirtualPathField();  asd;      
    if(virtualPath == null)
    {
        var objClientId = GetClientId(objClientId);
        if(objClientId  != null)
        {
            virtualPath = document.getElementById(objClientId);
        }
    }    
    if ( virtualPath == null )
    {
        if ( document.getElementById("ctl01_HiddenFieldVirtualPath") != null )
            virtualPath = document.getElementById("ctl01_HiddenFieldVirtualPath").value;
    }    
 
    if ( document.getElementById("HiddenFieldVirtualPath") != null )
        virtualPath = document.getElementById("HiddenFieldVirtualPath").value
        
    if(virtualPath == null)
    {
        var objClientId = GetClientId(objClientId);
        if(objClientId  != null)
        {
            virtualPath = document.getElementById(objClientId);
        }
    }
    
    if ( virtualPath == null )
    {
        if ( document.getElementById("ctl01_HiddenFieldVirtualPath") != null )
            virtualPath = document.getElementById("ctl01_HiddenFieldVirtualPath").value;
    }
    if(virtualPath == null)
    {
        virtualPath = "";
    }
    if(virtualPath.charAt(virtualPath.length - 1) == "/")
    {
        virtualPath = virtualPath.substring(0,virtualPath.length-1);
    }
    if(virtualPath.charAt(virtualPath.length - 1) == "/")
    {
        virtualPath = virtualPath.substring(0,virtualPath.length-1);
    }  
    
    if(valueArray.length == idArray.length)
    {
        var tableMain = document.createElement("table");
        tableMain.setAttribute("width", "100%");
        tableMain.cellpadding ="0";
        tableMain.cellspacing ="0";

        var tdTxtBox = document.createElement("td");
        tdTxtBox.setAttribute("width","95%");
        var txtBox = document.createElement("textarea");
        txtBox.style.height = "13px";
        txtBox.style.width = "98%";
        //txtBox.type = "text";
        txtBox.className = "formInputs";
        txtBox.readonly = "readonly";
        txtBox.id ="textbox" + divTagId;
        txtBox.setAttribute("ValueIds", "");
        txtBox.setAttribute("HasSelectedValuesSet", "false");
        txtBox.setAttribute("OriginalSelectedValueIds", "");
       // txtBox.setAttribute("width", "100%");
        txtBox.value = "";
        tdTxtBox.appendChild(txtBox);
        
        var trMain = new Array();
        trMain[0] = document.createElement("tr"); 
        trMain[0].appendChild(tdTxtBox);

        var tdButton = document.createElement("td");
        tdButton.setAttribute("width", "5%");
        tdButton.setAttribute("valign", "bottom");
        var img = document.createElement("img");
        img.id = "button" + divTagId;
        img.style.cursor = "pointer";
        img.src = virtualPath + "/App_Themes/HiRePro/Images/combo.gif";
        img.setAttribute("onclick","showDropDownList('"+ divTagId + "')");
        tdButton.appendChild(img);
        trMain[0].appendChild(tdButton);

        var tdMain = document.createElement("td");
        tdMain.colSpan = "2";
        dv = document.createElement('div');
        dv.id = "slidingDiv" + divTagId;
        dv.style.display = "none";
        dv.style.position = "relative";
        dv.style.overflow = "scroll";
        //dv.setAttribute("overflow", "scroll");
        dv.style.width = "90%";
        dv.style.height = "100px";
       // dv.setAttribute("width" , "98%");
       // dv.setAttribute("pixelHeight" ,"80px");
       // dv.style.backgroundColor = "white";
        dv.appendChild(buildDropDownWithValues(valueArray, idArray, divTagId));
        tdMain.appendChild(dv);
        
        trMain[1] = document.createElement("tr");
        trMain[1].appendChild(tdMain);
        
        tableMain.appendChild(trMain[0]);
        tableMain.appendChild(trMain[1]);
        
        var div = document.createElement("div");
        div.setAttribute("height" , "100px");
        div.setAttribute("overflow" , "scroll");
        div.appendChild(tableMain);
        if(document.getElementById(divTagId) != null)
            document.getElementById(divTagId).innerHTML = div.innerHTML;
    }
}

function showDropDownList(divTagId) {
    if(divTagId){
        var tag = document.getElementById("slidingDiv" + divTagId);
        if (tag.style.display == "none") {
            //document.getElementById("innerTable" + divTagId).style.display = "block";
            slidedown("slidingDiv"+ divTagId);
        }
        else {
            //document.getElementById("innerTable" + divTagId).style.display = "none";
            slideup("slidingDiv" + divTagId);
        }
    }
}

function buildDropDownWithValues(valueArray, idArray, divTagId)
{
    var table = document.createElement("table");
    table.setAttribute("width", "90%");
    table.id = "innerTable" + divTagId;
    //table.style.position = "absolute";
    table.style.backgroundColor = "white";
    table.style.height = "100";
    table.setAttribute("overflow" , "scroll");

    
    for(var i=0;i<valueArray.length;i++)
    {
        var tr = document.createElement("tr");
        
        var td1 = document.createElement("td");
        var checkBox = document.createElement("input");
        checkBox.id ="checkBox" + divTagId + idArray[i];
        checkBox.type = "checkbox";
        checkBox.setAttribute("ValueId", trim(idArray[i]));
        checkBox.setAttribute("Value", trim(valueArray[i]));
        checkBox.setAttribute("onclick","checkBoxChecked('" + checkBox.id + "','" + divTagId + "')");
        td1.appendChild(checkBox);
        tr.appendChild(td1);

        var td2 = document.createElement("td");
        var label = document.createElement("label");
        label.innerHTML = valueArray[i];
        td2.appendChild(label);
        tr.appendChild(td2);
        
        table.appendChild(tr);
    }
    return table;
}

function checkBoxChecked(checkBoxId, divTagId)
{
    var checkBox = document.getElementById(checkBoxId);
    var textBox = document.getElementById("textbox" + divTagId);
    var valueId =  checkBox.getAttribute("ValueId");
    var value =  checkBox.getAttribute("Value");
    var textBox = document.getElementById("textbox" + divTagId);
    var ids = textBox.getAttribute("ValueIds");
    var originalSelectedIds = textBox.getAttribute("OriginalSelectedValueIds");
    if(originalSelectedIds == null)
    {
        originalSelectedIds = "";
    }
    var originalLength = 0;
    
    if(checkBox.checked)
    {
        if(textBox.value.length == 0 || textBox.value == "")
        {
            textBox.value = value;
            ids = valueId;
        }
        else
        {
            textBox.value = textBox.value + ", " + value;
            ids = ids + "," + valueId;
        }
        textBox.setAttribute("ValueIds", ids);
        
        //
        // For removed ids
        //
        
        var tempArray = originalSelectedIds.split(",");
        
        if(originalSelectedIds != null && originalSelectedIds.length > 0 && isValuePresentInArray(tempArray, valueId) == true)
        {
            var removedIds = textBox.getAttribute("RemovedIds");
            originalLength = removedIds.length;
            
            removedIds =  removedIds.replace(", " + valueId, "");
            if(removedIds.length == originalLength)
            {
                removedIds =  removedIds.replace(" " + valueId, "");
            }
            if(removedIds.length == originalLength)
            {
                removedIds =  removedIds.replace("," + valueId, "");
            }
            if(removedIds.length == originalLength)
            {
                removedIds =  removedIds.replace(valueId, "");
            }
            if(removedIds.indexOf(",",0) == 0)
            {
                removedIds = removedIds.replace(", ","");
                removedIds = removedIds.replace(",","");
            }
            textBox.setAttribute("RemovedIds", removedIds);
        }
        else
        
        //
        // For added ids
        //
        
        tempArray = originalSelectedIds.split(",");
        
        if(originalSelectedIds != null  && isValuePresentInArray(tempArray, valueId) == false)
        {
            var addedIds = textBox.getAttribute("AddedIds");
            if(addedIds == null)
            {
                addedIds = "";
            }
            if(addedIds.length == 0 || addedIds == "")
            {
                addedIds = valueId;
            }
            else
            {
                addedIds = addedIds + "," + valueId;
            }
            textBox.setAttribute("AddedIds", addedIds);
        }
    }
    else
    {
        var textBoxValue = textBox.value;
        originalLength = textBoxValue.length;
        tempArray = textBoxValue.split(",");
        if(isValuePresentInArray(tempArray,value) == true)
        {
            textBoxValue =  textBoxValue.replace(", " + value +",", ",");
            if(textBoxValue.length == originalLength)
            {
                textBoxValue =  textBoxValue.replace(value +",", ",");
            }
            if(textBoxValue.length == originalLength)
            {
                textBoxValue =  textBoxValue.replace(" " + value, "");
            }
            if(textBoxValue.length == originalLength)
            {
                textBoxValue =  textBoxValue.replace("," + value, "");
            }
            if(textBoxValue.length == originalLength)
            {
                textBoxValue =  textBoxValue.replace(value, "");
            }
            
            if(textBoxValue.indexOf(", ",0) == 0)
            {
                textBoxValue = textBoxValue.substring(2, textBoxValue.length);
            }
            else if(textBoxValue.indexOf(",",0) == 0)
            {
                textBoxValue = textBoxValue.substring(1, textBoxValue.length);
            }
            textBox.value = $.trim(textBoxValue);
            document.getElementById("textbox" + divTagId).title = textBoxValue;
        }
        
        originalLength = ids.length;
        tempArray = ids.split(",");
        if(isValuePresentInArray(tempArray,valueId) == true)
        {
            ids =  ids.replace(", " + valueId, "");
            if(ids.length == originalLength)
            {
                ids = ids.replace(" " + valueId, "");
            }
            if(ids.length == originalLength)
            {
                ids = ids.replace("," + valueId, "");
            }
            if(ids.length == originalLength)
            {
                ids = ids.replace(valueId, "");
            }
            if(ids.indexOf(",",0) == 0)
            {
                ids = ids.replace(", ","");
                ids = ids.replace(",","");
            }
            textBox.setAttribute("ValueIds", ids);
        }
        
        //
        // For removed ids
        //
        
        tempArray = originalSelectedIds.split(",");
        
        if(originalSelectedIds != null && originalSelectedIds.length > 0 && isValuePresentInArray(tempArray,valueId) == true)
        {
            var removedIds = textBox.getAttribute("RemovedIds");
            if(removedIds.length == 0)
            {
                removedIds = valueId;
            }
            else
            {
                removedIds = removedIds + ", " + valueId;
            }
            textBox.setAttribute("RemovedIds", removedIds);
        }
        else
        
        //
        // For added ids
        //
        
        tempArray = originalSelectedIds.split(",");
        
        if(originalSelectedIds != null && isValuePresentInArray(tempArray,valueId) == false)
        {
            var addedIds = textBox.getAttribute("AddedIds");
            if(addedIds == null)
            {
                addedIds = "";
            }
            tempArray = addedIds.split(",");
            if(isValuePresentInArray(tempArray,valueId) == true)
            {
                addedIds =  addedIds.replace(", " + valueId, "");
                addedIds =  addedIds.replace(" " + valueId, "");
                addedIds =  addedIds.replace("," + valueId, "");
                addedIds =  addedIds.replace(valueId, "");
                if(addedIds.indexOf(",",0) == 0)
                {
                    addedIds = addedIds.replace(", ","");
                    addedIds = addedIds.replace(",","");
                }
                textBox.setAttribute("AddedIds", addedIds);
            }
            textBox.setAttribute("AddedIds", addedIds);
        }
    }
    getCheckedValues(divTagId);
    getCheckedValueIds(divTagId);
}

function setSelectedValues(idArray, divTagId)
{
    var textBox = document.getElementById("textbox" + divTagId);  
      
      if(textBox != null)
      {
        textBox.setAttribute("ValueIds", "");
        textBox.setAttribute("HasSelectedValuesSet", "false");
        textBox.setAttribute("OriginalSelectedValueIds", "");
        textBox.value = "";
    }
     for(var i=0;i<idArray.length;i++)
     {
     
      if(document.getElementById("checkBox" + divTagId + idArray[i]) != null)
      {
         document.getElementById("checkBox" + divTagId + idArray[i]).checked = true;
         checkBoxChecked("checkBox" + divTagId + idArray[i], divTagId);  
      }   
     }
               
    if(textBox!=null)
    {
        var ids = textBox.getAttribute("ValueIds");   
        textBox.setAttribute("RemovedIds", "");
        textBox.setAttribute("AddedIds", "");
        textBox.setAttribute("HasSelectedValuesSet", "true");
        textBox.setAttribute("OriginalSelectedValueIds", ids);
   } 
}

function getRemovedIds(id)
{  
    if(id){
        var txtBox = document.getElementById("textbox" + id)
        if(txtBox != null)
        {
            var removedIdsArray = new Array();
            if(txtBox.getAttribute("RemovedIds") != null)
            {
                removedIdsArray = txtBox.getAttribute("RemovedIds").split(",");
            }
            if(removedIdsArray.length == 1 && removedIdsArray[0] == "")
            {
               var removedIdsArray = txtBox.getAttribute("RemovedIds").split(",");        
                if(removedIdsArray.length == 1 && removedIdsArray[0] == "")
                {
                    removedIdsArray = new Array();
                }
            }
            return removedIdsArray;
        }
    }
    return null;
}

function getAddedIds(id)
{
    if(id){
        var txtBox = document.getElementById("textbox" + id)
        if(txtBox != null)
        {
            var addedIdsArray = new Array();
            if(txtBox.getAttribute("AddedIds") != null)
            {
                addedIdsArray = txtBox.getAttribute("AddedIds").split(",");
            }
            if(addedIdsArray.length == 1 && addedIdsArray[0] == "")
            {
               var addedIdsArray = txtBox.getAttribute("AddedIds").split(",");        
                if(addedIdsArray.length == 1 && addedIdsArray[0] == "")
                {
                    addedIdsArray = new Array();
                }
            }
            return addedIdsArray;
        }
    }
    return null;
}

function getCheckedValues(id)
{
    if(id){
        var textBox = document.getElementById("textbox" + id);
        if(textBox != null)
        {
            var valuesArray =  textBox.value.split(",");
            if(valuesArray.length == 1 && valuesArray[0] == "")
            {
                valuesArray = new Array();
            }
            return valuesArray;
        }
        else
        {
            return new Array();
        }
    }
}

function getCheckedValueIds(id)
{
    if(id){
        var textBox = document.getElementById("textbox" + id);
        if(textBox != null)
        {
            var idsArray = textBox.getAttribute("ValueIds").split(",");
            if(idsArray.length == 1 && idsArray[0] == "")
            {
                idsArray = new Array();
            }
            return idsArray;
        }
        else
        {
            return new Array();
        }
    }
}

function checkAll(id)
{
    if(id){
        for(var i=0;;i++)
        {
            var checkBox = document.getElementById("checkBox" + id + i);
            if(checkBox == null)
            {
                break;
            }
            checkBox.checked = true;
            checkBoxChecked("checkBox" + id + i, id);
        }
    }
}

function uncheckAll(id)
{
    var textBox = document.getElementById("textbox" + id);
    if (textBox != null) {
        var checkedIds = textBox.getAttribute("ValueIds");
        if (checkedIds != null) {
            var tempArray = checkedIds.split(",");
            for (var i = 0; i < tempArray.length; i++) {
                var checkBox = document.getElementById("checkBox" + id + tempArray[i]);
                if (checkBox == null) {
                    break;
                }
                checkBox.checked = false;
            }
        }
        textBox.setAttribute("ValueIds", "");
        textBox.setAttribute("OriginalSelectedValueIds", "");
        textBox.value = "";
    }
}

function trim(str)
{
    if(str == null)
        return "";
    if(str.length > 0)
    {
        while(str.charAt(0) == " " && str.length > 0)
        {
            str = str.substring(1, str.length);
        }
    }
    if(str.length > 0)
    {
        while(str.charAt(str.length - 1) == " " && str.length > 0)
        {
            str = str.substring(0, str.length - 1);
        }
    }
    
    return str;
}

function isValuePresentInArray(array, value)
{
    if(array.length == 1 && array[0] == "")
        return false;
    for(var i=0;i<array.length;i++)
    {
        if(trim(value) == trim(array[i]))
            return true;
    }
    return false;
}


//************** Sliding Script *************//

// JavaScript Document
var timerlen = 5;
var slideAniLen = 250;

var timerIDUtility = new Array();
var startTime = new Array();
var obj = new Array();
var endHeight = new Array();
var moving = new Array();
var dir = new Array();

function slidedown(objname) {
    if (moving[objname])
        return;

    if (document.getElementById(objname).style.display != "none")
        return; // cannot slide down something that is already visible

    moving[objname] = true;
    dir[objname] = "down";
    startslide(objname);
}

function slideup(objname) {
    if (moving[objname])
        return;

    if (document.getElementById(objname).style.display == "none")
        return; // cannot slide up something that is already hidden

    moving[objname] = true;
    dir[objname] = "up";
    startslide(objname);
}

function startslide(objname) {
    obj[objname] = document.getElementById(objname);

    endHeight[objname] = parseInt(obj[objname].style.height);
    startTime[objname] = (new Date()).getTime();

    if (dir[objname] == "down") {
        obj[objname].style.height = "1px";
    }

    obj[objname].style.display = "block";

    timerIDUtility[objname] = setInterval('slidetick(\'' + objname + '\');', timerlen);
}

function slidetick(objname) {
    var elapsed = (new Date()).getTime() - startTime[objname];

    if (elapsed > slideAniLen)
        endSlide(objname)
    else {
        var d = Math.round(elapsed / slideAniLen * endHeight[objname]);
        if (dir[objname] == "up")
            d = endHeight[objname] - d;

        obj[objname].style.height = d + "px";
    }

    return;
}

function endSlide(objname) {
    if(objname){
        clearInterval(timerIDUtility[objname]);

        if (dir[objname] == "up")
            obj[objname].style.display = "none";
            
        if(endHeight[objname]!= NaN)
            obj[objname].style.height = endHeight[objname] + "px";

        delete (moving[objname]);
        delete (timerIDUtility[objname]);
        delete (startTime[objname]);
        delete (endHeight[objname]);
        delete (obj[objname]);
        delete (dir[objname]);
    }
    return;
}



//************* End of Sliding Script **************//



function getAttachmentInnerHtml(attachmentCollection, getFileByIdFunctionName, deleteAttachmentFunctionName, mode)
{
    if(attachmentCollection != null )
    {
        var div = document.createElement("div");
        if(attachmentCollection.Attachments !=null)
        {
            for(var i=0;i<attachmentCollection.Attachments.length;i++)
            {
                var anchor = document.createElement("a");
                anchor.id = "anchorAttachment1" + i;
                anchor.innerHTML = attachmentCollection.Attachments[i].FileName;
                anchor.setAttribute("onclick",getFileByIdFunctionName+"('" + attachmentCollection.Attachments[i].TargetItemId + "')");
                anchor.style.color="Blue";
                anchor.setAttribute("href","javascript:void(0)");
                anchor.style.textDecoration = "underline";
                //anchor.style.cursor = "pointer";
                div.appendChild(anchor);
                
                if(mode != "ReadOnly")
                {
                    var anchorDelete = document.createElement("a");
                    anchorDelete.id = "anchorDelete" + i;
                    anchorDelete.innerHTML = "    X";
                    anchorDelete.setAttribute("onclick", deleteAttachmentFunctionName+"('" + attachmentCollection.Attachments[i].AttachmentId + "','" + anchor.id + "','"+anchorDelete.id+"')");
                    anchorDelete.style.color="Red";
                    anchorDelete.title = "Delete";
                    //anchorDelete.style.cursor = "pointer";
                    anchorDelete.setAttribute("href","javascript:void(0)");
                    div.appendChild(anchorDelete);
                }
                
                div.appendChild(document.createElement("br"));
            }
        }
        if(attachmentCollection.FileAttachments !=null)
        {
            for(var i=0;i<attachmentCollection.FileAttachments.length;i++)
            {
                var anchor = document.createElement("a");
                var targetPath = attachmentCollection.FileAttachments[i].TargetPath;
                anchor.setAttribute("onclick", "javascript:window.open('/"+targetPath+"')");
                anchor.innerHTML = targetPath.substring(targetPath.lastIndexOf("/", targetPath.length)+1);
                anchor.style.color="Blue";
                //anchor.style.cursor = "pointer";
                 anchorDelete.setAttribute("href","javascript:void(0)");
                div.appendChild(anchor);
                div.appendChild(document.createElement("br"));
            }
        }
        return div.innerHTML;
    }
    return "";
}

//
// Encrypt the given string
//

function encodeString(stringToBeEncoded, length)
{
    if(stringToBeEncoded == "0" || stringToBeEncoded == null || stringToBeEncoded.length == 0)
        return stringToBeEncoded;
    var encodedString = "";
    for(var i=0;i<stringToBeEncoded.length;i++)
    {
        if(i==0)
        {
            if(stringToBeEncoded.charAt(i) != "0")
            {
                encodedString += String.fromCharCode(stringToBeEncoded.charCodeAt(i) * 2) ;
            }
            else
            {
                encodedString += "z";
            }
        }       
        else 
        {
            if((stringToBeEncoded.charCodeAt(i) + stringToBeEncoded.charCodeAt(i - 1)) != 96)
            {
                encodedString += String.fromCharCode(stringToBeEncoded.charCodeAt(i) + stringToBeEncoded.charCodeAt(i - 1)) ;
            }
            else
            {
                encodedString += "z";
            }
        }
        
        
    }
    for(var i=encodedString.length;i<length;i++)
    {
        if((encodedString.length % Math.floor(Math.random()* 5)) > Math.floor(Math.random()* 2))
        {
            var rand = String.fromCharCode(Math.floor(Math.random()*26) + 97);
            encodedString += rand;
        }
        else
        {
            var rand = String.fromCharCode(Math.floor(Math.random()*9) + 48);
            encodedString += rand;
        }
    }
    encodedString += stringToBeEncoded.length;
    encodedString += stringToBeEncoded.length.toString().length;
    encodedString += "rtz!";
    return encodedString;
}

function decodeString(stringToBeDecoded)
{
    if(!isEncoded(stringToBeDecoded))
    {
        return stringToBeDecoded;
    }
    var decodedString = "";
    if(stringToBeDecoded == "0")
        return stringToBeDecoded;
    if(stringToBeDecoded.length > 4)
    {
        var last4 = stringToBeDecoded.substring(stringToBeDecoded.length - 4);
        if(last4 == "rtz!")
        {
            stringToBeDecoded = stringToBeDecoded.substring(0,stringToBeDecoded.length -4);
            var lengthOfLength = stringToBeDecoded.charAt(stringToBeDecoded.length - 1);
            var validLength = stringToBeDecoded.substr(stringToBeDecoded.length - lengthOfLength - 1, lengthOfLength);
            stringToBeDecoded = stringToBeDecoded.substring(0, validLength);
            var temp = String.fromCharCode(stringToBeDecoded.charCodeAt(i) / 2);
            for(var i=0;i<stringToBeDecoded.length;i++)
            {
                if(i == 0)
                {
                    if(stringToBeDecoded.charAt(i) != 'z')
                    {
                        decodedString += String.fromCharCode(stringToBeDecoded.charCodeAt(i) / 2);
                    }
                    else
                    {
                        decodedString += "0";
                    }
                }
                else 
                {
                    if(stringToBeDecoded.charAt(i) != 'z')
                    {
                        decodedString += String.fromCharCode(stringToBeDecoded.charCodeAt(i) - temp.charCodeAt(0));
                    }
                    else
                    {
                        decodedString += "0";
                    }
                    temp = decodedString.charAt(decodedString.length - 1);
                }
                
            }
        }
    }
    return decodedString;
}

function isEncoded(str)
{   
    if(str){
        if(str.length > 4)
        {
            var last4 = str.substring(str.length - 4);
            if(last4 == "rtz!")
            {
                return true;
            }
        }
    }
    return false;
}


function GetClientId(strid) {
    if(strid){
        var count = document.forms[0].length;
        var i = 0;
        var eleName;
        for (i = 0; i < count; i++) {
            eleName = document.forms[0].elements[i].id;
            pos = eleName.indexOf(strid);
            if (pos >= 0)
                return eleName;
        }
    }
    return null;
}



function getCustomCombo(comboInfo)
{
    
    var divCombo = document.createElement("div");
    
    var table = document.createElement("table");
    var tr = table.insertRow(table.rows.length);
    var td1 = tr.insertCell(tr.cells.length);
    var td2 = tr.insertCell(tr.cells.length);
    
    var textBox = document.createElement("input");
    textBox.setAttribute("type", "text");
    if(comboInfo.DefaultValue != null)
    {
        textBox.setAttribute("value", comboInfo.DefaultValue);
    }
    td1.appendChild(textBox);
    
    var buttonOpen = document.createElement("input");
    buttonOpen.setAttribute("type", "button");
    buttonOpen.id = "buttonOpenCombo";
    buttonOpen.setAttribute("value", "|");
    //buttonOpen.setAttribute("onclick", "constructCustomComboDropDownDiv(this, " + comboInfo.ComboItems  +  "," + comboInfo.OnDataBound + ")");
    //buttonOpen.onclick =  constructCustomComboDropDownDiv("buttonOpenCombo", comboInfo);
    var onclick = "";
    onclick = "showCurrentControl('buttonOpenCombo', 'divDropDown')";
    buttonOpen.onclick = onclick;
    td2.appendChild(buttonOpen);
    
    if(comboInfo.Width != null)
    {
        textBox.style.width = comboInfo.Width;
    }
        
    divCombo.appendChild(table);
    constructCustomComboDropDownDiv("buttonOpenCombo", comboInfo);
    return divCombo;
}

function constructComboInfo(defaultValue, onDataBound, width)
{
    var comboInfo =
    {
        "DefaultValue":defaultValue,
        "ComboItems":null,
        "OnDataBound":onDataBound,
        "Width":width
    }
    return comboInfo;
}

function constructCustomComboItem(text, value, isHyperLink, url, onclick)
{
    var comboItem =
    {
        "Text":text,
        "IsHyperLink":isHyperLink,
        "URL":url,
        "Value":value,
        "OnClick":onclick
    }
    return comboItem;
}

function constructCustomComboDropDownDiv(controlId, comboInfo)
{
    var control = document.getElementById(controlId);
//    var comboInfo = control.getAttribute("ComboInfo");
    var divDropDown = document.createElement("div");
    divDropDown.id  = "divDropDown";
    divDropDown.style.display = "none";
    if(comboInfo != null)
    {
        var comboItemsLength = 0;
        if(comboInfo.ComboItems != null)
        {
            comboItemsLength = comboInfo.ComboItems.length;
        }
        var table = document.createElement("table");
        for(var i=0; i<comboItemsLength;i++)
        {
            var comboItem = comboInfo.ComboItems[i];
            var tr = table.insertRow(table.rows.length);
            var td = tr.insertCell(tr.cells.length);
            var elementToBeCreated = "label";
            if(comboItem.IsHyperLink || comboItem.OnClick  != null)
            {
                elementToBeCreated = "anchor";
            }
            var element =  null;
            if(elementToBeCreated == "anchor")
            {
                element = createAnchor(comboItem.Text, comboItem.OnClick, null, comboItem.Text, comboItem.URL, null);
            }
            else
            {
                element = document.createElement("label");
                element.setAttribute("innerHTML", comboItem.Text);
            }
            
            td.appendChild(element);
        }
        divDropDown.appendChild(table);
    }
    document.getElementById("divMiscellanious").appendChild(divDropDown);
}

function showCurrentControl(controlId, dropDownDivId)
{
    var divDropDown = document.getElementById(dropDownDivId);
    divDropDown.className = "but_st1";
    if(divDropDown.style.display == "none")
    {
        divDropDown.style.display = "inline";
        var control = document.getElementById(controlId);
        var offsetHeight = 0;
        var offsetWidth = 0;
        if(control != null)
        {
            var pos = position(controlId);
            offsetHeight = pos.top;
            offsetWidth = pos.left;
        }
        divDropDown.style.position = "absolute";
        divDropDown.style.zIndex = 100;
        divDropDown.style.backgroundColor = "#f2f2d2";
        showControl(divDropDown, offsetWidth - 70, offsetHeight + 23, 90, 75);
    }
    else
    {
        divDropDown.style.display = "none";
    }
    
}

function position(controlId)
{
    var control = document.getElementById(controlId);
    if ( !control ) return { top: 0, left: 0 };
		if ( control === control.ownerDocument.body ) return jQuery.offset.bodyOffset( control );
		var box  = control.getBoundingClientRect(), doc = control.ownerDocument, body = doc.body, docElem = doc.documentElement,
			clientTop = docElem.clientTop || body.clientTop || 0, clientLeft = docElem.clientLeft || body.clientLeft || 0,
			top  = box.top  + (self.pageYOffset || jQuery.boxModel && docElem.scrollTop  || body.scrollTop ) - clientTop,
			left = box.left + (self.pageXOffset || jQuery.boxModel && docElem.scrollLeft || body.scrollLeft) - clientLeft;
		return { top: top, left: left };
}


