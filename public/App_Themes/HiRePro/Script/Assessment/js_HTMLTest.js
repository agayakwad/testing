﻿var processId = null;
var serviceCallMethodNames = new Array();
serviceCallMethodNames["GetTenantByIdOrAlias"] = "../JSONServices/JSONTenantManagementService.svc/GetTenantByIdOrAlias";
serviceCallMethodNames["GetCatalogValues"] = "../JSONServices/JSONCommonManagementService.svc/GetCatalogValues";
serviceCallMethodNames["ViewTestTranscriptReport"] = "../JSONServices/JSONAssessmentManagementService.svc/ViewTestTranscriptReport"; //serviceCallMethodNames["LoginToTest"] = "../JSONServices/JSONAssessmentManagementService.svc/LoginToTest";
serviceCallMethodNames["LoginToTest"] = "../JSONServices/JSONAssessmentManagementService.svc/LoginToTest";
serviceCallMethodNames["GetQuestionsForOnlineAssessment"] = "../JSONServices/JSONAssessmentManagementService.svc/GetQuestionsForOnlineAssessment";
serviceCallMethodNames["SubmitTestResult"] = "../JSONServices/JSONAssessmentManagementService.svc/SubmitTestResult";

function enableLoading() {
    if (processId) {
        window.clearTimeout(processId);
        processId = null;
    }
    var browserName = navigator.appName;
    var loadingDiv = document.getElementById("LoadingDivTag");
    if (!loadingDiv) return;
    loadingDiv.innerHTML = "Loading...";
    loadingDiv.style.fontWeight = "bold";
    loadingDiv.style.width = "85px";
    loadingDiv.style.display = "block";
    loadingDiv.style.zIndex = "99";
    var lblError = document.getElementById("ctl00_lblError");
    if (lblError != null) {
        lblError.innerHTML = String.Empty;
    }
    staticText = 'Loading';
    dynamicText = "...";
    currentText = staticText;
    timeSpent = 0;
    if (browserName == "Microsoft Internet Explorer") {
        var wDim = getBrowserWindowSize();
        var dDim = {
            width: loadingDiv.clientWidth,
            height: loadingDiv.clientHeight
        }; //Element.getDimensions(div);        
        loadingDiv.style.posTop = document.documentElement.scrollTop + 1;
        loadingDiv.style.left = ((wDim.width - dDim.width) / 2) + 'px';
    } else if (browserName == "Netscape") {
        var wDim = getBrowserWindowSize();
        var dDim = {
            width: loadingDiv.clientWidth,
            height: loadingDiv.clientHeight
        }; //Element.getDimensions(div);        
        loadingDiv.style.posTop = document.documentElement.scrollTop + "px";
        loadingDiv.style.left = ((wDim.width - dDim.width) / 2) + 'px';
    }
    delayDuration = 250;
    rotateLoading();
}

function disableLoading() {
    var loadingDiv = document.getElementById("LoadingDivTag");
    if (loadingDiv != null) {
        loadingDiv.style.display = "none";
        delayDuration = 0;
        if (processId) {
            window.clearTimeout(processId);
            processId = null;
        }
    }
}

function getBrowserWindowSize() {
    var winW = 630,
    winH = 460;
    if (parseInt(navigator.appVersion) > 3) {
        if (navigator.appName == "Netscape") {
            winW = window.innerWidth;
            winH = window.innerHeight;
        }
        if (navigator.appName.indexOf("Microsoft") != -1) {
            winW = document.body.offsetWidth;
            winH = document.body.offsetHeight;
        }
    }
    var rval = {
        width: winW,
        height: winH
    };
    return rval;
}

function encodeString(stringToBeEncoded, length) { //alert(stringToBeEncoded);
    if (stringToBeEncoded == "0" || stringToBeEncoded == null || stringToBeEncoded.length == 0) return stringToBeEncoded;
    var encodedString = "";
    for (var i = 0; i < stringToBeEncoded.length; i++) {
        if (i == 0) {
            if (stringToBeEncoded.charAt(i) != "0") {
                encodedString += String.fromCharCode(stringToBeEncoded.charCodeAt(i) * 2);
            } else {
                encodedString += "z";
            }
        } else { if ((stringToBeEncoded.charCodeAt(i) + stringToBeEncoded.charCodeAt(i - 1)) != 96) {
                encodedString += String.fromCharCode(stringToBeEncoded.charCodeAt(i) + stringToBeEncoded.charCodeAt(i - 1));
            } else {
                encodedString += "z";
            }
        }
    }
    for (var i = encodedString.length; i < length; i++) {
        if ((encodedString.length % Math.floor(Math.random() * 5)) > Math.floor(Math.random() * 2)) {
            var rand = String.fromCharCode(Math.floor(Math.random() * 26) + 97);
            encodedString += rand;
        } else {
            var rand = String.fromCharCode(Math.floor(Math.random() * 9) + 48);
            encodedString += rand;
        }
    }
    encodedString += stringToBeEncoded.length;
    encodedString += stringToBeEncoded.length.toString().length;
    encodedString += "rtz!";
    return encodedString;
}

function GetTenantByIdOrAlias() {       
    var tenantAlias = location.search.substring(location.search.indexOf('=') + 1);    
    tenantId = 0; //Default value 
    if (tenantAlias != "" || (tenantId != null && tenantId != 0)) {
        var getTenantByIdOrAliasRequest = {
            "TenantId": tenantId,
            "UserId": 0,
            "TenantAlias": tenantAlias
        }
        callAjaxService("GetTenantByIdOrAlias", getTenantByIdOrAliasRequest, getTenantByIdOrAliasCallBack, callBackFailMethod); //hirepro.JSONServiceLayer.ServiceContracts.IJsonTenantManagementService.GetTenantByIdOrAlias(tenantId, 10, tenantAlias, getTenantByIdOrAliasCallBack);
    } else {
        alert("Invalid URL");
        loginHideLoading();
    }
}

function ErrorMessageHandlerForModelWindow(divid, msgid, result) {
    disableLoading();
    var failedTxtMsgBox = divid; //alert(divid);
    var displayMessage;
    if (!result) {
        result = "Your request cannot be processed at the moment!!! Sorry for the inconvinience";
    }
    displayMessage = result;
    if (failedTxtMsgBox != null && displayMessage != null) {
        if (displayMessage.length > 130) {
            displayMessage = displayMessage.toString().substr(0, 128) + ".......";
        }
        failedTxtMsgBox.innerHTML = displayMessage;
        failedTxtMsgBox.setAttribute("title", result);
        var divErrorMsg = msgid;
        divErrorMsg.style.display = "inline";
    }
}

function getTenantByIdOrAliasCallBack(response) {
    if (response != null && (response.IsException == true || response.IsFailure == true)) {
        ErrorMessageHandlerForModelWindow(document.getElementById("FailedModelTextMessage"), document.getElementById("message_b_small"), response.Message);
    }
        var Alogin=readCookie("ALoginId");
        if(Alogin==document.getElementById('txtUserName').value)
        {
            createCookie("EnableLastSession","true",100);
        }
        else
        {
            createCookie("EnableLastSession","false",100);
        }
        createCookie("ALoginId", document.getElementById('txtUserName').value,100)
    tenantId = response.Tenant.Id;
    var LoginName = document.getElementById('txtUserName').value;
    var Password = document.getElementById('txtPassword').value;
    if (LoginName != '' && Password != '') {
        var loginValidateRequest = {
            "LoginName": LoginName,
            "Password": Password,
            "TenantId": tenantId
        };
        callAjaxService("LoginToTest", loginValidateRequest, callBackLoginValidate, callBackLoginValidateFail);
    } else { if (LoginName == '') {
            document.getElementById('submit_result').innerHTML = "Enter Login Name";
            loginHideLoading();
        } else {
            document.getElementById('submit_result').innerHTML = "Enter Password";
            loginHideLoading();
        }
    }
}

function setFormElementsAsVisible(type) {
    var elements = document.getElementsByTagName(type);
    if (elements != null) {
        var i = 0;
        while (i < elements.length) {
            var setForHidden = elements[i].getAttribute("setForHidden");
            if (setForHidden == "true") {
                elements[i].style.display = elements[i].getAttribute("valueToResetDisplay");
                elements[i].setAttribute("setForHidden", "false");
            }
            i++;
        }
    }
}

function setFormElementsAsHidden(type, parentControl) {
    var elements = document.getElementsByTagName(type);
    if (elements != null) {
        var i = 0;
        while (i < elements.length) {
            if (!isElementPresentInThControl(elements[i].parentNode, parentControl)) {
                if (elements[i].style.display != "none") {
                    elements[i].setAttribute("setForHidden", "true");
                    elements[i].setAttribute("valueToResetDisplay", elements[i].style.display);
                    elements[i].style.display = "none";
                }
            }
            i++;
        }
    }
}

function isElementPresentInThControl(elementsParent, control) {
    if (control == null) return false;
    while (elementsParent != null) {
        if (elementsParent.id == null) return false;
        if (elementsParent.id.length > 0 && elementsParent.id == control.id) return true;
        if (elementsParent.parentNode == null) return false;
        elementsParent = elementsParent.parentNode;
    }
    return false;
}

function displayId(id) {
    if (document.getElementById(id) != null) document.getElementById(id).style.display = "block";
}

function hideId(id) {
    if (document.getElementById(id) != null) document.getElementById(id).style.display = "none";
}

function getTenantId() {
    return readCookie('TenantId');
}

function getUserId() {
    return readCookie('UserId');
}

function getTenantAdamInfoCandidatePage() {
    if (UserId == "") UserId = "0";
    TenantId = readCookie('TenantId');
    var tenantAdamInfo = {
        "TenantId": TenantId,
        //document.getElementById("HiddenFieldTenantId").value,
        "UserId": UserId // document.getElementById("HiddenFieldTenantUserId").value
    };
    return tenantAdamInfo;
}

function setCookie(c_name, value, expiredays) { //alert(value);
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + expiredays);
    document.cookie = c_name + "=" + escape(value) + ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString());
}

function readCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}

function SuccessMessageHandler(result) {
    document.getElementById("registerationDiv").style.display = "none";
    document.getElementById("registerSuccessDiv").innerHTML = "<div class=\"width_a\"><h2 style=\"color:#006600\">Successfully Registered!</h2><p>Your details have been saved with us. We will email you the test details, link & username & password.</p><p>Wish you All the best.</p></div>";
    disableLoading(); //var divErrorMsg = document.getElementById("message_a");
    //divErrorMsg.style.display = "inline";
    //document.getElementById('btnSave').style.display="none";
}

function ErrorMessageHandler(result) {
    disableLoading();
    var failedTxtMsgBox = document.getElementById("FailedTextMessage");
    var displayMessage;
    if (!result) {
        result = "Your request cannot be processed at the moment!!! Sorry for the inconvinience";
    }
    displayMessage = result;
    if (failedTxtMsgBox != null && displayMessage != null) {
        if (displayMessage.length > 130) {
            displayMessage = displayMessage.toString().substr(0, 128) + ".......";
        }
        if (displayMessage == 'Error: Object reference not set to an instance of an object.') displayMessage = "Select Technology From the List";
        failedTxtMsgBox.innerHTML = displayMessage;
        failedTxtMsgBox.setAttribute("title", result);
        var divErrorMsg = document.getElementById("message_b");
        divErrorMsg.style.display = "inline";
    }
}

//Loading image
function rotateLoading() {
    var browserName = navigator.appName;
    var loadingDiv = document.getElementById("LoadingDivTag");
    var updatedText = currentText;
    var lengthOfDynamicTextInCurrentText = currentText.toString().length - staticText.toString().length;
    if (lengthOfDynamicTextInCurrentText >= dynamicText.toString().length) {
        updatedText = staticText;
    } else {
        updatedText = staticText + dynamicText.toString().substr(0, lengthOfDynamicTextInCurrentText + 1);
    }
    if (browserName == "Microsoft Internet Explorer") {
        var wDim = getBrowserWindowSize();
        var dDim = {
            width: loadingDiv.clientWidth,
            height: loadingDiv.clientHeight
        }; //Element.getDimensions(div);        
        loadingDiv.style.posTop = eval(document.documentElement.scrollTop + 1) + "%";
        loadingDiv.style.left = ((wDim.width - dDim.width) / 2) + 'px';
        loadingDiv.innerHTML = updatedText;
    } else if (browserName == "Netscape") {
        var wDim = getBrowserWindowSize();
        var dDim = {
            width: loadingDiv.clientWidth,
            height: loadingDiv.clientHeight
        }; //Element.getDimensions(div);        
        loadingDiv.style.posTop = eval(document.documentElement.scrollTop + 1) + "%";
        loadingDiv.style.left = ((wDim.width - dDim.width) / 2) + 'px';
        loadingDiv.innerHTML = updatedText;
    }
    currentText = updatedText;
    if (delayDuration > 0) {
        processId = setTimeout(rotateLoading, delayDuration);
    }
    timeSpent += delayDuration;
}

//To hide any div
function hide(divid, txtBoxId) {
    if (document.getElementById(txtBoxId)) document.getElementById(txtBoxId).innerHTML = "";
    if (document.getElementById(divid)) {
        document.getElementById(divid).style.height = '0px';
        document.getElementById(divid).style.display = "none";
    }
    window.location = "http://www.hirepro.in";
}

// To Get virtual path 
function getVirtualPathField() {
    var virtualHiddenField = document.getElementById("ctl00_HiddenFieldVirtualPath");
    if (virtualHiddenField) return virtualHiddenField.value;
    virtualHiddenField = this.parent.document.getElementById("ctl00_HiddenFieldVirtualPath");
    if (virtualHiddenField) return virtualHiddenField.value;
    else return "";
}

function callBackFailMethod() {}

/// Note: You must add serviceCallMethodName in the serviceCallMethodNames array which is present in the js_Metadata.js
/// serviceCallMethodName: The array index name which you added in the array
/// request: json Object to be passed as an argument
/// successHandler: success message handler
/// errorHandler: error message handler
function callAjaxService(serviceCallMethodName, request, successHandler, errorHandler, dataType) {
    var returnDataType = "json";
    if (dataType) {
        returnDataType = dataType;
    }
    var ajaxValue = {
        "type": "POST",
        "url": serviceCallMethodNames[serviceCallMethodName],
        "data": JSON.stringify(request),
        "contentType": "application/json; charset=utf-8",
        "dataType": returnDataType,
        "success": successHandler,
        "error": errorHandler,
        "complete": function (XMLHttpRequest, textStatus) {
            this; // the options for this ajax request
        }
    }
    if ($.ajax) {
        $.ajax(ajaxValue);
    } else {
        jQuery.ajax(ajaxValue);
    }
}

// Create a JSON object only if one does not already exist. We create the
// methods in a closure to avoid creating global variables.
if (!this.JSON) {
    JSON = {};
} (function () {
    function f(n) { // Format integers to have at least two digits.
        return n < 10 ? '0' + n : n;
    }
    if (typeof Date.prototype.toJSON !== 'function') {
        Date.prototype.toJSON = function (key) {
            return this.getUTCFullYear() + '-' + f(this.getUTCMonth() + 1) + '-' + f(this.getUTCDate()) + 'T' + f(this.getUTCHours()) + ':' + f(this.getUTCMinutes()) + ':' + f(this.getUTCSeconds()) + 'Z';
        };
        String.prototype.toJSON = Number.prototype.toJSON = Boolean.prototype.toJSON = function (key) {
            return this.valueOf();
        };
    }
    var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
    escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
    gap, indent, meta = { // table of character substitutions
        '\b': '\\b',
        '\t': '\\t',
        '\n': '\\n',
        '\f': '\\f',
        '\r': '\\r',
        '"': '\\"',
        '\\': '\\\\'
    },
    rep;
    function quote(string) { // If the string contains no control characters, no quote characters, and no
        // backslash characters, then we can safely slap some quotes around it.
        // Otherwise we must also replace the offending characters with safe escape
        // sequences.
        escapable.lastIndex = 0;
        return escapable.test(string) ? '"' + string.replace(escapable, function (a) {
            var c = meta[a];
            return typeof c === 'string' ? c : '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
        }) + '"' : '"' + string + '"';
    }
    function str(key, holder) { // Produce a string from holder[key].
        var i, // The loop counter.
        k, // The member key.
        v, // The member value.
        length, mind = gap,
        partial, value = holder[key]; // If the value has a toJSON method, call it to obtain a replacement value.
        if (value && typeof value === 'object' && typeof value.toJSON === 'function') {
            value = value.toJSON(key);
        } // If we were called with a replacer function, then call the replacer to
        // obtain a replacement value.
        if (typeof rep === 'function') {
            value = rep.call(holder, key, value);
        } // What happens next depends on the value's type.
        switch (typeof value) {
        case 'string':
            return quote(value);
        case 'number':
            // JSON numbers must be finite. Encode non-finite numbers as null.
            return isFinite(value) ? String(value) : 'null';
        case 'boolean':
        case 'null':
            // If the value is a boolean or null, convert it to a string. Note:
            // typeof null does not produce 'null'. The case is included here in
            // the remote chance that this gets fixed someday.
            return String(value); // If the type is 'object', we might be dealing with an object or an array or
            // null.
        case 'object':
            // Due to a specification blunder in ECMAScript, typeof null is 'object',
            // so watch out for that case.
            if (!value) {
                return 'null';
            } // Make an array to hold the partial results of stringifying this object value.
            gap += indent;
            partial = []; // Is the value an array?
            if (Object.prototype.toString.apply(value) === '[object Array]') { // The value is an array. Stringify every element. Use null as a placeholder
                // for non-JSON values.
                length = value.length;
                for (i = 0; i < length; i += 1) {
                    partial[i] = str(i, value) || 'null';
                } // Join all of the elements together, separated with commas, and wrap them in
                // brackets.
                v = partial.length === 0 ? '[]' : gap ? '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']' : '[' + partial.join(',') + ']';
                gap = mind;
                return v;
            } // If the replacer is an array, use it to select the members to be stringified.
            if (rep && typeof rep === 'object') {
                length = rep.length;
                for (i = 0; i < length; i += 1) {
                    k = rep[i];
                    if (typeof k === 'string') {
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            } else { // Otherwise, iterate through all of the keys in the object.
                for (k in value) {
                    if (Object.hasOwnProperty.call(value, k)) {
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            } // Join all of the member texts together, separated with commas,
            // and wrap them in braces.
            v = partial.length === 0 ? '{}' : gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}' : '{' + partial.join(',') + '}';
            gap = mind;
            return v;
        }
    } // If the JSON object does not yet have a stringify method, give it one.
    if (typeof JSON.stringify !== 'function') {
        JSON.stringify = function (value, replacer, space) { // The stringify method takes a value and an optional replacer, and an optional
            // space parameter, and returns a JSON text. The replacer can be a function
            // that can replace values, or an array of strings that will select the keys.
            // A default replacer method can be provided. Use of the space parameter can
            // produce text that is more easily readable.
            var i;
            gap = '';
            indent = ''; // If the space parameter is a number, make an indent string containing that
            // many spaces.
            if (typeof space === 'number') {
                for (i = 0; i < space; i += 1) {
                    indent += ' ';
                } // If the space parameter is a string, it will be used as the indent string.
            } else if (typeof space === 'string') {
                indent = space;
            } // If there is a replacer, it must be a function or an array.
            // Otherwise, throw an error.
            rep = replacer;
            if (replacer && typeof replacer !== 'function' && (typeof replacer !== 'object' || typeof replacer.length !== 'number')) {
                throw new Error('JSON.stringify');
            } // Make a fake root object containing our value under the key of ''.
            // Return the result of stringifying the value.
            return str('', {
                '': value
            });
        };
    } // If the JSON object does not yet have a parse method, give it one.
    if (typeof JSON.parse !== 'function') {
        JSON.parse = function (text, reviver) { // The parse method takes a text and an optional reviver function, and returns
            // a JavaScript value if the text is a valid JSON text.
            var j;
            function walk(holder, key) { // The walk method is used to recursively walk the resulting structure so
                // that modifications can be made.
                var k, v, value = holder[key];
                if (value && typeof value === 'object') {
                    for (k in value) {
                        if (Object.hasOwnProperty.call(value, k)) {
                            v = walk(value, k);
                            if (v !== undefined) {
                                value[k] = v;
                            } else {
                                delete value[k];
                            }
                        }
                    }
                }
                return reviver.call(holder, key, value);
            } // Parsing happens in four stages. In the first stage, we replace certain
            // Unicode characters with escape sequences. JavaScript handles many characters
            // incorrectly, either silently deleting them, or treating them as line endings.
            cx.lastIndex = 0;
            if (cx.test(text)) {
                text = text.replace(cx, function (a) {
                    return '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                });
            } // In the second stage, we run the text against regular expressions that look
            // for non-JSON patterns. We are especially concerned with '()' and 'new'
            // because they can cause invocation, and '=' because it can cause mutation.
            // But just to be safe, we want to reject all unexpected forms.
            // We split the second stage into 4 regexp operations in order to work around
            // crippling inefficiencies in IE's and Safari's regexp engines. First we
            // replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
            // replace all simple value tokens with ']' characters. Third, we delete all
            // open brackets that follow a colon or comma or that begin the text. Finally,
            // we look to see that the remaining characters are only whitespace or ']' or
            // ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.
            if (/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) { // In the third stage we use the eval function to compile the text into a
                // JavaScript structure. The '{' operator is subject to a syntactic ambiguity
                // in JavaScript: it can begin a block or an object literal. We wrap the text
                // in parens to eliminate the ambiguity.
                j = eval('(' + text + ')'); // In the optional fourth stage, we recursively walk the new structure, passing
                // each name/value pair to a reviver function for possible transformation.
                return typeof reviver === 'function' ? walk({
                    '': j
                },
                '') : j;
            } // If the text is not JSON parseable, then a SyntaxError is thrown.
            throw new SyntaxError('JSON.parse');
        };
    }
} ());

// JavaScript Document
function detectBrowser() { //Detect browser version  
    var userAgent = navigator.userAgent.toLowerCase(); // Figure out what browser is being used  
    var browser = {
        version: (userAgent.match(/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/) || [])[1],
        safari: /webkit/.test(userAgent),
        opera: /opera/.test(userAgent),
        msie: /msie/.test(userAgent) && !/opera/.test(userAgent),
        mozilla: /mozilla/.test(userAgent) && !/(compatible|webkit)/.test(userAgent),
        chrome: /chrome/.test(userAgent)
    };
    return browser; //end browser detection 
}

//To send session from Javascript
function reportAjaxFunction(url) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else if (window.ActiveXObject) { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Your browser does not support XMLHTTP!");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) { //document.myForm.time.value=xmlhttp.responseText;
            //alert(xmlhttp.responseText);
            if (xmlhttp.responseText == "Success") {
                window.location = "TermsAndCondtion.aspx";
            } else {
                window.location = "TermsAndCondtion.aspx"; //document.getElementById('submit_result').innerHTML= "Login Failed";
            }
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);
}

function WireUpEventHandler(Target, Event, Listener, argumentArray) { //Register event.
    if (Target.addEventListener) {
        Target.addEventListener(Event, Listener(argumentArray), false);
    } else if (Target.attachEvent) {
        Target.attachEvent('on' + Event, Listener(argumentArray));
    } else {
        Event = 'on' + Event;
        Target.Event = Listener;
    }
}

///*******************************end of common methods**************************************************///
var testId = 0;
var tenantId = 0; //Method called to fetch all questions for online test
function GetQuestionsForOnlineTest() {
    if (document.all) {
        document.onkeydown = function () {
            var key_f5 = 116; // 116 = F5
            if (key_f5 == event.keyCode) {
                alert("Refreshing the page is not allowed");
                event.keyCode = 0;
                return false;
            }
        }
    }
   
    var hdnTestId = document.getElementById("ctl00_DefaultContent_hdnTestId");
    var hdnTenantId = document.getElementById("ctl00_DefaultContent_hdnTenantId");
    testId = hdnTestId.value;
    tenantId = hdnTenantId.value;
    
    var userId = "0";
    var jsonQuestionRequest = {
        "TestId": testId,
        "TenantId": tenantId,
        "UserId": userId
    }; 
    disableSelection(document.getElementById('masterBody'));
    callAjaxService("GetQuestionsForOnlineAssessment", jsonQuestionRequest, callBackGetQuestionsForOnlineAssessment, callBackGetQuestionsForOnlineAssessmentFail);
}

// Global variables
var responseArray = new Array();
var groupName = "";
var sectionName = "";
var queId = 0;
var questionString = "";
var answerCollection = new Array();
var instructionString = "";
var questionCount = 0;
var testDuration = 0.0;
var optionalGroups = 0;
var nonMandatoryCollection = null;
var optionalFlag = 0;
var questionType = 0;
var alias = null;

//If service call succeeded in fetching questions for online test
function callBackGetQuestionsForOnlineAssessment(response) {

    if (response != null && (response.IsException == true || response.IsFailure == true))
    {
        alert(response.Message);
    } 
    else
    { 
        if (response.JsonGroupWithQuestioncollectionForMandatorySection != null) {
            for (var count = 0; count < response.JsonGroupWithQuestioncollectionForMandatorySection.length; count++) {
                groupName = response.JsonGroupWithQuestioncollectionForMandatorySection[count].Name;
                var qIdArray = new Array();
                for (var i = 0; i < response.JsonGroupWithQuestioncollectionForMandatorySection[count].SectionTypeCollection.length; i++) {
                    sectionName = response.JsonGroupWithQuestioncollectionForMandatorySection[count].SectionTypeCollection[i].Name;
                    instructionString = response.JsonGroupWithQuestioncollectionForMandatorySection[count].SectionTypeCollection[i].Instruction; //var noOfQues = response.JsonGroupWithQuestioncollectionForMandatorySection[count].SectionTypeCollection[i].QuestionType.length;
                    for (var j = 0; j < response.JsonGroupWithQuestioncollectionForMandatorySection[count].SectionTypeCollection[i].QuestionType.length; j++) {
                        TypeOfQuestionEnum = response.JsonGroupWithQuestioncollectionForMandatorySection[count].SectionTypeCollection[i].QuestionType[j].QuestionGeneralType.TypeOfQuestionEnum;
                        if(TypeOfQuestionEnum==10)
                        {
                            questionString = "<b>Passage:</b><br/>" + response.JsonGroupWithQuestioncollectionForMandatorySection[count].SectionTypeCollection[i].QuestionType[j].QuestionGeneralType.HtmlString + "<br/><b>Question:</b><br/>";
                            var CQGTC = response.JsonGroupWithQuestioncollectionForMandatorySection[count].SectionTypeCollection[i].QuestionType[j].ChildQuestionGeneralTypeCollection;
                            for (var k = 0; k < CQGTC.length; k++)
                            {
                                queId = CQGTC[k].QuestionId;
                                questionString += CQGTC[k].QuestionGeneralType.HtmlString;
                                answerCollection = CQGTC[k].AnswerChoiceCollection;
                                TypeOfQuestionEnum = CQGTC[k].QuestionGeneralType.TypeOfQuestionEnum;
                                QuestionType = CQGTC;
                                createResponseObject(groupName, sectionName, queId, questionString, instructionString, answerCollection, TypeOfQuestionEnum, QuestionType);
                                qIdArray.push(queId);
                            }
                        }
                        else
                        {
                            queId = response.JsonGroupWithQuestioncollectionForMandatorySection[count].SectionTypeCollection[i].QuestionType[j].QuestionId;
                            questionString = response.JsonGroupWithQuestioncollectionForMandatorySection[count].SectionTypeCollection[i].QuestionType[j].QuestionGeneralType.HtmlString;
                            answerCollection = response.JsonGroupWithQuestioncollectionForMandatorySection[count].SectionTypeCollection[i].QuestionType[j].AnswerChoiceCollection;
                            QuestionType = response.JsonGroupWithQuestioncollectionForMandatorySection[count].SectionTypeCollection[i].QuestionType[j];
                            createResponseObject(groupName, sectionName, queId, questionString, instructionString, answerCollection, TypeOfQuestionEnum, QuestionType);
                            qIdArray.push(queId);
                        }
                        
                    }
                }
                arrayForChart(groupName, sectionName, qIdArray);
            }

        }
        nonMandatoryCollection = response.JsonGroupWithQuestioncollectionForNonMandatorySection;
    }
    createFlagButtons();
 
    viewTestQuestions(responseArray[0],0);
    timerStart();
}

var chartArray = new Array();
function arrayForChart(group, section, qIdArray) {
    var chartObj = {
        "Group": group,
        "Section": section,
        "NoOfQuestions": qIdArray.length,
        "Questions": qIdArray
    }
    chartArray.push(chartObj);
}

// To create response object and stores in array and displaying on request
function createResponseObject(groupName, sectionName, questionId, questionString, instructionString, answercollection, TypeOfQuestionEnum, QuestionType) {
    var responseObject = {
        "group": groupName,
        "section": sectionName,
        "questionId": questionId,
        "question": questionString,
        "instruction": instructionString,
        "answers": answercollection,
        "typeOfQuestion": TypeOfQuestionEnum,
        "questionType": QuestionType,
        "isQuestionAnswered": false
    }
    responseArray.push(responseObject);
}

function generatePreviewQuestion(optionalGroupPreview, counter)
{ 

    var queStr="";
    var ansHtmlString="";
    var htmlString="";
    switch(optionalGroupPreview.QuestionGeneralType.TypeOfQuestionEnum)
    {
        case 1: //Boolean
                queStr = "Boolean Question:<br/>";
                queStr = queStr + optionalGroupPreview.QuestionGeneralType.HtmlString; 
                queStr = queStr + "<br/>True/False";           
                break;
        case 4: //Matchchoice
                queStr = "Match the choices Question:<br/>"; 
                var AnswerChoiceCollection = optionalGroupPreview.AnswerChoiceCollection;
                var QuestionGeneralType = optionalGroupPreview.QuestionGeneralType;
                var ChildQuestionGeneralTypeCollection = optionalGroupPreview.ChildQuestionGeneralTypeCollection;
                var ACCLength1 = AnswerChoiceCollection.length;
                var MCtable = document.createElement("table");
                MCtable.width = "100%";
                var tdC1 = getTd("Column A");
                var tdC2 = getTd("Column B");                
                tdC1.width = "50%";
                tdC2.width = "50%";
                var trC = document.createElement("tr");
                trC.style.backgroundColor = "#cccccc";
                trC.appendChild(tdC1);
                trC.appendChild(tdC2);
                MCtable.appendChild(trC);              
                for (var ACCIndex1 = 0; ACCIndex1 < ACCLength1; ACCIndex1++) {
                    var columnA = ChildQuestionGeneralTypeCollection[ACCIndex1].QuestionGeneralType.HtmlString;
                    var columnB = AnswerChoiceCollection[ACCIndex1].HtmlString;
                    var tr = document.createElement("tr");
                    var td1 = getTd(choiceArray[ACCIndex1] + ") " + columnA);
                    var td2 = getTd(ACCIndex1 + 1 + ") " + columnB);                    
                    tr.appendChild(td1);
                    tr.appendChild(td2);
                    MCtable.appendChild(tr);                    
                }
                var tempQDiv = document.createElement("div");
                tempQDiv.appendChild(MCtable);          
                queStr = queStr + tempQDiv.innerHTML;
                
                break;
        case 7: //MCQ
                queStr = "Multiple choices Question:<br/>"
                queStr = queStr + optionalGroupPreview.QuestionGeneralType.HtmlString;
                var ansCol = optionalGroupPreview.AnswerChoiceCollection;                
                for (var a = 0; a < ansCol.length; a++) {
                    var anscnt = a + 1;
                    ansHtmlString = ansHtmlString + anscnt + ". " + ansCol[a].HtmlString + "<br/>";
                }                
                break;
        case 8: //Subjective
                queStr = "Subjective Question:<br/>"
                queStr = queStr + optionalGroupPreview.QuestionGeneralType.HtmlString;
                break;
    }
    
    htmlString = "<b>Question :" + counter + "</b><br/>" + queStr + "<br/>";   
    htmlString = htmlString + "<ul>" + ansHtmlString + "</ul>";
    
    return htmlString;
}


function getOptionalGroups(optionalGroups) { 

    var htmlString = "";
    var counter = 1;
    var grpName = optionalGroups.Name;
    htmlString = "<b>Group name :</b>" + grpName + "<br/><br/>";
    for (var i = 0; i < optionalGroups.SectionTypeCollection.length; i++) {
        var secName = optionalGroups.SectionTypeCollection[i].Name;
        htmlString = htmlString + "<b>Section Name :</b>" + secName + "<br/><br/>";
        var instStr = optionalGroups.SectionTypeCollection[i].Instruction;
        htmlString = htmlString + "<b>Instruction :</b>" + instStr + "<br/><br/><hr><br/>";
        
        for (var j = 0; j < optionalGroups.SectionTypeCollection[i].QuestionType.length; j++) {
            var flag =0;
            var typeofQuestion = optionalGroups.SectionTypeCollection[i].QuestionType[j].QuestionGeneralType.TypeOfQuestionEnum;
            if(typeofQuestion==10)
            {
                var passage = "Reference to context Question:<br/>Passage:<br/>" + optionalGroups.SectionTypeCollection[i].QuestionType[j].QuestionGeneralType.HtmlString + "<br/>"; 
                var cqgtc = optionalGroups.SectionTypeCollection[i].QuestionType[j].ChildQuestionGeneralTypeCollection;
                for (var k = 0; k < cqgtc.length; k++) 
                {
                    if(flag != 0) passage ="";
                    htmlString = htmlString + passage + "RTC-Q" + (k+1) + "<br/>"+ generatePreviewQuestion(cqgtc[k],counter);
                    counter = counter + 1;
                    flag=1;
                }
            }
            else
            {            
                htmlString = htmlString + generatePreviewQuestion(optionalGroups.SectionTypeCollection[i].QuestionType[j],counter);
                counter = counter + 1;
            }
        }
    }
    PreviewWindow(htmlString, grpName);
}

//For viewing the optional group questions
function PreviewWindow(htmlString, grpName) {
    if (document.getElementById("modalWindowDivControl") != null) modalWindowDiv.innerHTML = "";
    modalWindowDiv = document.createElement("div");
    modalWindowDiv.setAttribute("className", "demo");
    modalWindowDiv.id = "modalWindowDivControl";
    modalWindowDiv.title = "Group : " + grpName;
    var browser = detectBrowser();
    if (browser.msie == true && browser.version.toString() == "6.0") {
        var divTemp = document.createElement("div");
        divTemp.className = "ui-dialog-content-height";
        divTemp.innerHTML = htmlString; //divTemp.style.height = "480px";
        modalWindowDiv.appendChild(divTemp);
    } else {
        modalWindowDiv.innerHTML = htmlString;
    }
    document.getElementById('divJQueryModlWindow').appendChild(modalWindowDiv);
    jQuery("#modalWindowDivControl").dialog({
        bgiframe: true,
        height: 550,
        width: 650,
        modal: true,
        overlay: {
            backgroundColor: '#000',
            opacity: 0.5
        }
    });
}

//To get chosen optional group questions
function GetAdditionalQuestions(OptionalGroup) { 
    var qIdArray = new Array();
    
    groupName = OptionalGroup.Name;
    var optionalCount = 0;
    for (var i = 0; i < OptionalGroup.SectionTypeCollection.length; i++) {
        sectionName = OptionalGroup.SectionTypeCollection[i].Name;
        instructionString = OptionalGroup.SectionTypeCollection[i].Instruction; //var noOfQues = OptionalGroup.SectionTypeCollection[i].QuestionType.length;
        for (var j = 0; j < OptionalGroup.SectionTypeCollection[i].QuestionType.length; j++) {               
            TypeOfQuestionEnum = OptionalGroup.SectionTypeCollection[i].QuestionType[j].QuestionGeneralType.TypeOfQuestionEnum;
            if(TypeOfQuestionEnum==10)
            {
                questionString = "<b>Passage:</b><br/>" + OptionalGroup.SectionTypeCollection[i].QuestionType[j].QuestionGeneralType.HtmlString + "<br/><b>Question:</b><br/>";
                var CQGTC = OptionalGroup.SectionTypeCollection[i].QuestionType[j].ChildQuestionGeneralTypeCollection;
                for (var k = 0; k < CQGTC.length; k++)
                {
                    queId = CQGTC[k].QuestionId;
                    questionString += CQGTC[k].QuestionGeneralType.HtmlString;
                    answerCollection = CQGTC[k].AnswerChoiceCollection;
                    TypeOfQuestionEnum = CQGTC[k].QuestionGeneralType.TypeOfQuestionEnum;
                    QuestionType = CQGTC;
                    createResponseObject(groupName, sectionName, queId, questionString, instructionString, answerCollection, TypeOfQuestionEnum, QuestionType);
                    qIdArray.push(queId);
                    optionalCount = optionalCount + 1;
                }
            }
            else
            {
                queId = OptionalGroup.SectionTypeCollection[i].QuestionType[j].QuestionId;
                questionString = OptionalGroup.SectionTypeCollection[i].QuestionType[j].QuestionGeneralType.HtmlString;
                answerCollection = OptionalGroup.SectionTypeCollection[i].QuestionType[j].AnswerChoiceCollection;
                QuestionType = OptionalGroup.SectionTypeCollection[i].QuestionType[j];
                createResponseObject(groupName, sectionName, queId, questionString, instructionString, answerCollection, TypeOfQuestionEnum, QuestionType);
                qIdArray.push(queId);
                optionalCount = optionalCount + 1;
            }
            
        }
    } 
    arrayForChart(groupName, sectionName, qIdArray);
    createFlagButtonsForOptionalQuestions(optionalCount);
    HandlingQuestions();
    questionCount = questionCount+1;
    updateFlagbuttons();
    viewTestQuestions(responseArray[questionCount],questionCount);
}

//Generate optional groups dynamically
function OptionalGroups(optionGroups) {
    var options = optionGroups;
    var optionRadio = null;
    var browser = detectBrowser();
    var modalOptionGroup = document.createElement("div");
    modalOptionGroup.id = "modalChooseGroup";
    modalOptionGroup.title = "Choose Group";
    var divOptionGrps = document.createElement("div");
    divOptionGrps.id = "divChooseGroup";
    var checkedvalue = new Array();
    var i;
    for (i = 0; i < options; i++) {
        if (browser.msie && browser.version == "6.0") {
            optionRadio = document.createElement("<input name='preview'>");
        } else {
            optionRadio = document.createElement("input");
            optionRadio.name = "preview";
        }
        optionRadio.id = 'opt' + i;
        optionRadio.type = 'radio';
        optionRadio.value = i;
        var value = nonMandatoryCollection[0].JsonGroupcollection[i]; //WireUpEventHandler(optionRadio, "click", getOptionalGroups, [value]);
        var optionSpan = document.createElement('span');
        optionSpan.id = 'span' + i;
        optionSpan.innerHTML = nonMandatoryCollection[0].JsonGroupcollection[i].Name + "<br/>";
        divOptionGrps.appendChild(optionRadio);
        divOptionGrps.appendChild(optionSpan);
        checkedvalue.push(optionRadio);
    }
    modalOptionGroup.appendChild(divOptionGrps);
    var btnAdd = null;
    btnAdd = document.createElement("a");
    btnAdd.id = 'btnAdd';
    btnAdd.innerHTML = "Add";
    btnAdd.className = "positive_a";
    btnAdd.style.textDecoration = "none";
    btnAdd.style.marginRight = "5px";
    btnAdd.setAttribute('href', 'javascript:addGroup(' + options + ')');
    var btnPreview = document.createElement("a");
    btnPreview.id = 'btnPreview';
    btnPreview.innerHTML = "Preview";
    btnPreview.className = "positive_a";
    btnPreview.style.textDecoration = "none";
    btnPreview.setAttribute('href', 'javascript:preview(' + options + ')');
    var divButton = document.createElement("div");
    divButton.id = "modalButton";
    divButton.className = "buttons f_right";
    divButton.appendChild(btnAdd);
    divButton.appendChild(btnPreview);
    modalOptionGroup.appendChild(divButton);
    document.getElementById('divJQueryModlWindow').appendChild(modalOptionGroup);
    jQuery("#modalChooseGroup").dialog({
        bgiframe: true,
        height: 210,
        width: 280,
        modal: true,
        overlay: {
            height: '100%',
            backgroundColor: '#000',
            opacity: 0.5
        }
    });
}

//adding up optional group as per choice
function addGroup(optionCount) { 
    for (var i = 0; i < optionCount; i++) {
        var optId = 'opt' + i;
        var optObj = document.getElementById(optId);
        if (optObj.checked == true) {
            GetAdditionalQuestions(nonMandatoryCollection[0].JsonGroupcollection[optObj.value]);
            optionalFlag = 1;
            btnNext.className = "";
        }
    }
    jQuery("#modalChooseGroup").remove(); //jQuery("#modalChooseGroup").close();
}

function preview(optionCount) {
    for (var i = 0; i < optionCount; i++) {
        var optId = 'opt' + i;
        var optObj = document.getElementById(optId);
        if (optObj.checked == true) {
            getOptionalGroups(nonMandatoryCollection[0].JsonGroupcollection[optObj.value]);
        }
    }
}

function createFlagButtons() {
    var htmlString;
    htmlString = "<ul id='FlagAnchorsList'>";
    for (var count = 0; count < responseArray.length; count++) {
        var quesNumber = count + 1;
        htmlString = htmlString + "<li>";
        htmlString = htmlString + "<a id =\"flag_" + quesNumber + "\" class='red' href='#'onclick='FlagClick(" + quesNumber + ")'>";
        htmlString = htmlString + quesNumber;
        htmlString = htmlString + "</a>";
        htmlString = htmlString + "</li>";
    }
    htmlString = htmlString + "</ul>";
    document.getElementById('FlagButtonDiv').innerHTML = htmlString
}

function createFlagButtonsForOptionalQuestions(optionQuestionCount) {
    var htmlString;
    var quesNumber = parseInt(document.getElementById('questionSpan').innerHTML);
    htmlString = document.getElementById('FlagAnchorsList').innerHTML;
    for (var count = 0; count < optionQuestionCount; count++) {
        quesNumber = quesNumber + 1;
        htmlString = htmlString + "<li>";
        htmlString = htmlString + "<a id =\"flag_" + quesNumber + "\" class='red' href='#'onclick='FlagClick(" + quesNumber + ")'>";
        htmlString = htmlString + quesNumber;
        htmlString = htmlString + "</a>";
        htmlString = htmlString + "</li>";
    }
    document.getElementById('FlagAnchorsList').innerHTML = htmlString
}

function FlagClick(quesNumber) {
    HandlingQuestions();
    var optionDiv = document.getElementById("optionsDiv");
    optionDiv.innerHTML = "";
    viewTestQuestions(responseArray[(quesNumber - 1)],(quesNumber - 1));
    questionCount = (quesNumber - 1);
    document.getElementById('questionSpan').innerHTML = quesNumber;
    updateFlagbuttons();
}

function FlagQuestion() {
    var questionToFlag = document.getElementById('questionSpan').innerHTML;
    var obj = document.getElementById("flag_" + questionToFlag);
    if (obj != null) {
        if (obj.className == "red" || obj.className == "green") {
            obj.className = "orange";
        } else { if (obj.className == "orange") {
                //                var choiceArray = new Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J");
                //                if (questionCount < responseArray.length) {
                //                    var isAnswered = "false";
                //                    for (var c = 0; c < totalAnswerCount; c++) {
                //                        if (document.getElementById('option' + c).checked == true) {
                //                            answerText = choiceArray[c]; //document.getElementById('option'+c).value;
                //                            if (obj != null) {
                //                                obj.className = "green";
                //                                isAnswered = "true";
                //                            }
                //                        }
                //                    }
                //                    if (isAnswered == "false") {
                //                        obj.className = "red";
                //                    }
                //                    var answerObject = {
                //                        "QuestionId": responseArray[questionCount].questionId,
                //                        "AnswerString": answerText
                //                    }
                //                    answerArray.push(answerObject);
                //                }
                HandlingQuestions();
                if (responseArray[questionCount].isQuestionAnswered == true) obj.className = "green";
                else obj.className = "red";
            }
        }
    }
}

//For viewing one question per page
var totalAnswerCount = 0;
var btnBack = null;
var btnNext = null;
var secForTooltip = "";
var instrForTooltip = "";
var typeOfQuestion = 0;

//Method to view the Question as per the Question Type
function viewTestQuestions(responseObject,count) {
    if(responseObject != null)
    {
    
        btnBack = document.getElementById("btnBack");
        btnNext = document.getElementById("btnNext");
        if(responseObject.section)
            secForTooltip = responseObject.section;
        if(responseObject.instruction)
            instrForTooltip = responseObject.instruction;
//        if (responseObject != 'undefined' && responseObject != null) {
            //Number of questions to be displayed
            var spanQuestionNo = document.getElementById("questionSpan");
            var questionnos = 0;
            questionnos = questionCount + 1;
            spanQuestionNo.innerHTML = questionnos; //Test duration
            var spanDuration = document.getElementById("spanDuration");
            spanDuration.innerHTML = document.getElementById("ctl00_DefaultContent_hdnTestDuration").value + " minutes"; // for answer choice collection options
            var optionDiv = document.getElementById("optionsDiv");
            optionDiv.innerHTML = "";
            var sectionLabel = document.getElementById("lblSection");
            var subsectionLabel = document.getElementById("lblSubsection");
            var questionLabel = document.getElementById("questionLabel");
            typeOfQuestion = responseObject.typeOfQuestion;
            sectionLabel.innerHTML = responseObject.group;
            subsectionLabel.innerHTML = responseObject.section;
    
            var resultArray = new Array();
            resultArray=readCookie('CandidateAnswers')
            var jsObject=new Array();
            if(resultArray!=null)
            {
                var questionArray=new Array();
               
                questionArray=resultArray.split(':');
                if(questionArray[count]!=null)
                {
                    jsObject=questionArray[count].split(',');
                    if(jsObject[0]!=responseObject.questionId)
                        { 
                            count=questionArray.length;
                           
                            do
                            {
                                count=count-1
                                if(questionArray[count]!=null)
                                {
                                    jsObject=questionArray[count].split(',');
                                }
                            }while(jsObject[0]!=responseObject.questionId && count!=0)
                    }
                    
                }
                else
                {
                            count=questionArray.length;
                            do
                            {
                                
                                if(questionArray[count]!=null)
                                {
                                    jsObject=questionArray[count].split(',');
                                }
                                count=count-1
                            }while(jsObject[0]!=responseObject.questionId && count!=0)
                
                }
            
            }
            
            switch (typeOfQuestion) {
            
            case 0:
                //"None";
                break;
            case 1:
                //"Boolean";
                questionLabel.innerHTML = responseObject.question;
                var answerTrue = {
                    "HtmlString": "True"
                };
                var answerFalse = {
                    "HtmlString": "False"
                };
                addElements(answerTrue, 0,0);
                addElements(answerFalse, 1,0);
                totalAnswerCount = 2;
            case 2:
                //"FillInTheBlank";
                break;
            case 3:
                //"Formula"; 
                break;
            case 4:
                //"MatchChoice";
                var questionArray = createMatchChoice(responseObject.questionType);
                questionLabel.innerHTML = questionArray[0];
                optionDiv.innerHTML = questionArray[1];
                break;
            case 5:
                //"Audio"; 
                break;
            case 6:
                //"Image"; 
                break;
            case 7:
                //"MCQ";
             
                questionLabel.innerHTML = responseObject.question;
                if(responseObject.questionId!=jsObject[0])
                {
                    jsObject[1]=0;
                }
                if (responseObject.answers != null) {
                    for (var i = 0; i < responseObject.answers.length; i++) {
                        addElements(responseObject.answers[i], i,jsObject[1]);
                    }
                }
                totalAnswerCount = i;
                break;
            case 8:
                //"QA";
                questionLabel.innerHTML = responseObject.question;
                var tempSpan = document.createElement("span");
                var txtArea = document.createElement("Textarea");
                txtArea.id = responseObject.questionId;
                tempSpan.appendChild(txtArea);
                optionDiv.innerHTML = tempSpan.innerHTML;
                totalAnswerCount = i;
                break;
            case 9:
                //"MultipleCorrectAnswer"; 
                break;
            case 10:
                //"ReferenceToContext";
                break;
            case 11:
                //"Template"; 
                break;
            case 12:
                //"Grouped"; 
                break;
            case 13:
                //"GenericSingleton"; 
                break;
            case 14:
                //"GenerisGrouped"; 
                break;
            }
       // }
    }
}

var mcAnswerArray = new Array();

//Dynamically generating Match Choice Question
function createMatchChoice(QuestionType) {
    var AnswerChoiceCollection = QuestionType.AnswerChoiceCollection;
    var QuestionGeneralType = QuestionType.QuestionGeneralType;
    var ChildQuestionGeneralTypeCollection = QuestionType.ChildQuestionGeneralTypeCollection;
    var ACCLength1 = AnswerChoiceCollection.length;
    var MCtable = document.createElement("table");
    MCtable.width = "100%";
    var tdC1 = getTd("Column A");
    var tdC2 = getTd("Column B");
    tdC1.className = "qustion_textd";
    tdC2.className = "qustion_textd";
    tdC1.width = "50%";
    tdC2.width = "50%";
    var trC = document.createElement("tr");
    trC.style.backgroundColor = "#cccccc";
    trC.appendChild(tdC1);
    trC.appendChild(tdC2);
    MCtable.appendChild(trC);
    var MCAtable = document.createElement("table");
    MCAtable.width = "60px";
    for (var ACCIndex1 = 0; ACCIndex1 < ACCLength1; ACCIndex1++) {
        var mcQuestionId = ChildQuestionGeneralTypeCollection[ACCIndex1].QuestionId;
        mcAnswerArray[ACCIndex1] = {
            "QuestionId": mcQuestionId,
            "AnswerString": ''
        }
        var columnA = ChildQuestionGeneralTypeCollection[ACCIndex1].QuestionGeneralType.HtmlString;
        var columnB = AnswerChoiceCollection[ACCIndex1].HtmlString;
        var tr = document.createElement("tr");
        var td1 = getTd(choiceArray[ACCIndex1] + ") " + columnA);
        var td2 = getTd(ACCIndex1 + 1 + ") " + columnB);
        td1.className = "qustion_textc";
        td2.className = "qustion_textc";
        tr.appendChild(td1);
        tr.appendChild(td2);
        MCtable.appendChild(tr);
        var trA = document.createElement("tr");
        var td1A = getTd(choiceArray[ACCIndex1] + ") ");
        td1A.style.width = "30px";
        var tempSpan = document.createElement("span");
        var txtInpt = document.createElement("input");
        txtInpt.type = "text";
        txtInpt.maxLength = 1;
        txtInpt.id = mcQuestionId;
        txtInpt.style.width = "20px";
        //txtInpt.onkeypress = "return numbersonly(this, event, "+ACCLength1+")"; 
        txtInpt.setAttribute("onkeypress","return numbersonly(this, event, "+ACCLength1+")");
        tempSpan.appendChild(txtInpt);
        var td2A = getTd(tempSpan.innerHTML);
        td2A.style.width = "30px";
        td1A.className = "qustion_textc";
        td2A.className = "qustion_textc";
        trA.appendChild(td1A);
        trA.appendChild(td2A);
        MCAtable.appendChild(trA);
    }
    totalAnswerCount = ACCIndex1;
    var tempQDiv = document.createElement("div");
    tempQDiv.appendChild(MCtable);
    var tempADiv = document.createElement("div");
    tempADiv.appendChild(MCAtable);
    var questionArray = new Array();
    questionArray[0] = tempQDiv.innerHTML;
    questionArray[1] = tempADiv.innerHTML;
    return questionArray;
}

String.prototype.trimSpace = function () {
    return this.replace(/(^\s*)|(\s*$)/g, "");
}

function numbersonly(myfield, e, ansLength,dec) { 
    var key;     
    var keychar;
    var answerString = "123456789";
    answerString = answerString.substr(0,Number(ansLength));
    if (window.event) key = window.event.keyCode;
    else if (e) key = e.which;
    else return true;
    keychar = String.fromCharCode(key);

    // control keys
    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27)) return true;

    // numbers
    else if ((answerString.indexOf(keychar) > -1)) return true;

    // decimal point jump
    else if (dec && (keychar == ".")) {
        myfield.form.elements[dec].focus();
        return false;
    }
    else return false;
}

//Construct a table cell
function getTd(innerData) {
    var td = document.createElement("td");
    if (innerData != null) {
        td.innerHTML = innerData;
    }
    return td;
}

//Dynamically generating options for answers (mcq/boolean)
var answerText = "";
var newtype = null;
function addElements(answer, id,position) {
    var ni = document.getElementById('optionsDiv');
    var browser = detectBrowser();
    if (browser.msie) {
        newtype = document.createElement("<input name='question' onclick='optionClick()'>");
    } else {
        newtype = document.createElement("input");
        newtype.name = "question";
    }
    newtype.type = 'radio';
    newtype.id = 'option' + id;
    newtype.value = answer.HtmlString;
      
    var newspan = document.createElement('span');
    newspan.id = 'span' + answer.Choice + 'Text';
    newspan.innerHTML = answer.HtmlString;
    answerText = newspan.innerHTML;
    var tempDiv = document.createElement("div");
    tempDiv.style.padding = "5px";
    tempDiv.appendChild(newtype);
    tempDiv.appendChild(newspan);
    ni.appendChild(tempDiv);
    var enableLastSession=readCookie("EnableLastSession");
    if(enableLastSession=="true")
    {
        if(position!=0)
        {
            if(position==answer.Choice)
            {
               document.getElementById('option' + id).checked=true;
                
            }
        }
    }
}
function optionClick()
{
    HandlingQuestions()
    var JsonObjectArray = new Array();
    var cookieString="";
    if (answerArray != null && answerArray.length != 0) {
        for (var i = 0; i < answerArray.length; i++) {
            var jsonObject = {
                "QuestionId": answerArray[i].QuestionId,
                "Answer": answerArray[i].AnswerString
            }
            JsonObjectArray.push(jsonObject);
            cookieString = cookieString + answerArray[i].QuestionId + ',' + answerArray[i].AnswerString + ':';

        }
         createCookie('CandidateAnswers',cookieString,100);
     }
}
//Go to next question
function viewNextQuestion() {

var btnSubmit = document.getElementById("btnSubmit");
    if (btnNext.className == "") {
        if (questionCount < responseArray.length - 1) {
            HandlingQuestions();
            questionCount = questionCount + 1;
            var optionDiv = document.getElementById("optionsDiv");
            optionDiv.innerHTML = "";
            btnBack.className = "";
            viewTestQuestions(responseArray[questionCount],questionCount);
            updateFlagbuttons();
        }
        else if (questionCount == responseArray.length - 1) {
            btnNext.className = "disabled";            
            var submitEnable = document.getElementById("submitBtnEnable");
            if (nonMandatoryCollection != null && nonMandatoryCollection.length != 0) {
                for (var i = 0; i < nonMandatoryCollection.length; i++) {
                    optionalGroups = nonMandatoryCollection[i].JsonGroupcollection.length;
                } //getOptionalGroups(nonMandatoryCollection.JsonGroupcollection);
            }
            else
                submitEnable.value = "yes";
                //btnSubmit.className = "";
            if (optionalFlag == 0) {
                if (nonMandatoryCollection != null && nonMandatoryCollection.length != 0) {
                    OptionalGroups(optionalGroups);
                }
            }
            else
                submitEnable.value = "yes";
                //btnSubmit.className = "";
            
        }
    }
    
//    var JsonObjectArray = new Array();
//    var cookieString="";
//    if (answerArray != null && answerArray.length != 0) {
//        for (var i = 0; i < answerArray.length; i++) {
//            var jsonObject = {
//                "QuestionId": answerArray[i].QuestionId,
//                "Answer": answerArray[i].AnswerString
//            }
//            JsonObjectArray.push(jsonObject);
//            cookieString = cookieString + answerArray[i].QuestionId + ',' + answerArray[i].AnswerString + ':';

//        }
//         createCookie('CandidateAnswers',cookieString,100);
   // }


            
}

// Get the array index for answer choice for persistence
function getArrayIndex(array, item) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] == item) {
            return i;
        }
    }
    return -1;
}

//Match the choice Update answers on prev/next
function matchChoiceCheck() {
    var tst = answerArray;
    var flag = 0;
    var flag1 = 0;
    for (var a = 0; a < answerArray.length; a++) {
        if (document.getElementById(answerArray[a].QuestionId)) {
            flag1++;
            if (answerArray[a].AnswerString != '') {
                var index = getArrayIndex(choiceArray, answerArray[a].AnswerString);
                document.getElementById(answerArray[a].QuestionId).value = index + 1;
                flag++;
            } else {
                document.getElementById(answerArray[a].QuestionId).value = '';
            }
            if (flag == flag1) responseArray[questionCount].isQuestionAnswered = true;
            else responseArray[questionCount].isQuestionAnswered = false;
        }
    }
}

//Subjective Update answers on prev/next
function subjectiveCheck() {
    var tst = answerArray;
    for (var a = 0; a < answerArray.length; a++) {
        if (document.getElementById(answerArray[a].QuestionId)) {
            document.getElementById(answerArray[a].QuestionId).value = answerArray[a].AnswerString;
            if (document.getElementById(answerArray[a].QuestionId).value != '') responseArray[questionCount].isQuestionAnswered = true;
            else responseArray[questionCount].isQuestionAnswered = false;
        }
    }
}

// Checking the radio button while switching between views
function radioButtonCheck() {

    for (var aqtest in answerArray) {
        if (answerArray[aqtest].QuestionId == responseArray[questionCount].questionId) {
            if (answerArray[aqtest].AnswerString != "") {
                var index = getArrayIndex(choiceArray, answerArray[aqtest].AnswerString);
                if (index != -1) {
                    document.getElementById('option' + index).checked = true;
                    responseArray[questionCount].isQuestionAnswered = true;
                }
            }
        }
    }
//    var JsonObjectArray = new Array();
//    var cookieString="";
//    if (answerArray != null && answerArray.length != 0) {
//        for (var i = 0; i < answerArray.length; i++) {
//            var jsonObject = {
//                "QuestionId": answerArray[i].QuestionId,
//                "Answer": answerArray[i].AnswerString
//            }
//            JsonObjectArray.push(jsonObject);
//            cookieString = cookieString + answerArray[i].QuestionId + ',' + answerArray[i].AnswerString + ':';

//        }
//         createCookie('CandidateAnswers',cookieString,100);
//     }
}

// Checking the boolean radio button while switching between views
function booleanCheck() {
    for (var aqtest in answerArray) {
        if (answerArray[aqtest].QuestionId == responseArray[questionCount].questionId) {
            if (answerArray[aqtest].AnswerString != "") {

                if (answerArray[aqtest].AnswerString == "True") document.getElementById('option' + 0).checked = true;
                else document.getElementById('option' + 1).checked = true;
                responseArray[questionCount].isQuestionAnswered = true;

            }
        }
    }
}

//Global variables
var choiceArray = new Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J");
var booleanArray = new Array("True", "False");
var answerArray = new Array();

//Filtering out questions and displying one by one
function HandlingQuestions() {
    if (typeOfQuestion == 1 || typeOfQuestion == 7) {
        answerText = "";
        for (var c = 0; c < totalAnswerCount; c++) {
            if (document.getElementById('option' + c).checked == true) {
                if (typeOfQuestion == 1) answerText = booleanArray[c]
                else answerText = choiceArray[c];
                responseArray[questionCount].isQuestionAnswered = true;
            }
        }
        if (answerText != "") {
            var answerObject = {
                "QuestionId": responseArray[questionCount].questionId,
                "AnswerString": answerText
            }
            if (answerArray.length > 0) {
                var flag = 0;
                for (var aq in answerArray) {
                    if (answerArray[aq].QuestionId == responseArray[questionCount].questionId) {
                        answerArray[aq].AnswerString = answerText;
                        flag = 1;
                        break;
                    }
                }
                if (flag == 0) answerArray.push(answerObject);
            } else answerArray.push(answerObject);
        }
    }
    if (typeOfQuestion == 4) { //matchchoice
        var flag = 0;
        for (var c = 0; c < totalAnswerCount; c++) {
            if (document.getElementById(mcAnswerArray[c].QuestionId)) {
                var mcAns = document.getElementById(mcAnswerArray[c].QuestionId).value;
                mcAns = mcAns.trimSpace()
            }
            if (mcAns != "" && mcAns != 0) {
                mcAnswerArray[c].AnswerString = choiceArray[mcAns - 1];
                flag++;
            }
        }
        if (flag == totalAnswerCount) responseArray[questionCount].isQuestionAnswered = true;
        else responseArray[questionCount].isQuestionAnswered = false;
        for (var c = 0; c < totalAnswerCount; c++) {
            var answerObject = {
                "QuestionId": mcAnswerArray[c].QuestionId,
                "AnswerString": mcAnswerArray[c].AnswerString
            }
            if (answerArray.length > 0) {
                var flag = 0;
                for (var aq in answerArray) {
                    if (answerArray[aq].QuestionId == mcAnswerArray[c].QuestionId) {
                        answerArray[aq].AnswerString = mcAnswerArray[c].AnswerString;
                        flag = 1;
                        break;
                    }
                }
                if (flag == 0) answerArray.push(answerObject);
            } else answerArray.push(answerObject);
        }
    }
    if (typeOfQuestion == 8) { //subjective
        answerText = "";
        var answerText = document.getElementById(responseArray[questionCount].questionId).value;
        answerText = answerText.trimSpace();
        if (answerText != "") responseArray[questionCount].isQuestionAnswered = true;
        else responseArray[questionCount].isQuestionAnswered = false;
        var answerObject = {
            "QuestionId": responseArray[questionCount].questionId,
            "AnswerString": answerText
        }
        if (answerArray.length > 0) {
            var flag = 0;
            for (var aq in answerArray) {
                if (answerArray[aq].QuestionId == responseArray[questionCount].questionId) {
                    answerArray[aq].AnswerString = answerText;
                    flag = 1;
                    break;
                }
            }
            if (flag == 0) answerArray.push(answerObject);
        } else answerArray.push(answerObject);
    }
}

function updateFlagbuttons() {
    if (typeOfQuestion == 1) booleanCheck();
    else if (typeOfQuestion == 4) matchChoiceCheck();
    else if (typeOfQuestion == 8) subjectiveCheck();
    else if (typeOfQuestion == 7) radioButtonCheck();
    for (var ra in responseArray) {
        var questionToFlag = document.getElementById('questionSpan').innerHTML;
        var obj = document.getElementById("flag_" + (parseInt(ra) + 1));
        if (obj != null && obj.className != "orange") {
            if (responseArray[ra].isQuestionAnswered == true) obj.className = "green";
            else obj.className = "red";
        }
    }
}

//Go to previous question
function viewPreviousQuestion() {
    if (btnBack.className == "") {
        if (questionCount != 0) {
            HandlingQuestions();
            questionCount = questionCount - 1;
            var optionDiv = document.getElementById("optionsDiv");
            optionDiv.innerHTML = "";
            btnNext.className = "";
            viewTestQuestions(responseArray[questionCount],questionCount);
            if (typeOfQuestion == 4) matchChoiceCheck();
            else if (typeOfQuestion == 8) subjectiveCheck();
            else radioButtonCheck();
            updateFlagbuttons();
        } else {
            btnBack.className = "disabled";
        }
    }
}

//If service call failed in fetching questions for online test
function callBackGetQuestionsForOnlineAssessmentFail(response, fail) {
    ErrorMessageHandler("Not able to fetch Questions");
}

//Method to get Query String Value.
function querySt(ji) {
    hu = window.location.search.substring(1);
    gy = hu.split("&");
    for (i = 0; i < gy.length; i++) {
        ft = gy[i].split("=");
        if (ft[0] == ji) {
            return ft[1];
        }
    }
}

//Method called for login to htmlTest
function LoginValidate() {
aa
    var obj = document.getElementById('btnLogin');    
    if (obj.className != "disabled")
    {
        document.getElementById('submit_result').innerHTML = "";
        //var alias = querySt("alias");
        if (alias != '') {
            loginShowLoading();
            var GetTenantByIdOrAliasRequest = {
                "TenantAlias": alias
            }; //callAjaxService("GetTenantByIdOrAlias",GetTenantByIdOrAliasRequest,callBackGetTenantByIdOrAlias,callBackGetTenantByIdOrAliasFail);
            var objTenant = GetTenantByIdOrAlias(GetTenantByIdOrAliasRequest);
        } else {
            ErrorMessageHandler("Enter Tenant Alias in url");
        }
    }
}

//If service call succeeded forLogin
function callBackLoginValidate(response) {
    if (response.IsFailure == false && response.Message == "") {
        
        var QueryString = "Login=True&TestId=" + response.TestId + "&TenantId=" + response.TenantId + "&TestDuration=" + response.TestDuration + "&TestName=" + response.TestName + "&PresentationURL=" + response.PresentationURL + "&CandidateName=" + response.CandidateName + "&CandidateId=" + response.CandidateId + "&TestInstructions=" + response.TestInstructions;
        reportAjaxFunction('TestSessionSet.aspx?function=LoginTest&' + QueryString);
        
    } else {
        document.getElementById('txtUserName').value = "";
        document.getElementById('txtPassword').value = "";
        document.getElementById('submit_result').innerHTML = response.Message;
        loginHideLoading();
    }
}

//If service call failed in Login
function callBackLoginValidateFail(response, fail) {
    ErrorMessageHandler("Login Failed");
    loginHideLoading();
}

function callBackGetTenantByIdOrAliasFail(response, fail) {}

//On click of Checkbox on Agree for Terms and conditions
function chkAgreeClick() {
    if (document.getElementById('chkAgree').checked == true) { //document.getElementById('btnAgree').value="Agree";
        document.getElementById('agreeDiv').className = "btn5";
        document.getElementById('agreeSpan').className = "btn_5";
    } else {
        document.getElementById('agreeDiv').className = "btn6";
        document.getElementById('agreeSpan').className = "btn_6"; //document.getElementById('btnAgree').value="Disagree";
    }
}

function callBackGetTenantByIdOrAlias(response) {
    tenantId = response.Tenant.Id;
}

//On clicking of agree button
function btnAgreeClick() { 

    if (document.getElementById('agreeDiv').className == "btn5") { 
        if(document.getElementById('pptUrlHiddenInput').value==null || document.getElementById('pptUrlHiddenInput').value=="")
        window.location = "Instructions.aspx";
        else
        window.location = "AboutCompany.aspx"; 
    } else { if (confirm("If you press diagree you will be log out")) {
            window.location = "HTMLTestLogin.aspx";
        } else {
            window.location = "TermsAndCondtion.aspx";
        }
    }
}

//On clicking of Start test
function StartTestClick() {
    window.location = "HTMLTestPage.aspx";
}

function SkipDemoClick() {
    window.location = "instructions.aspx";
}

function loginShowLoading() {
    document.getElementById('loadingDiv').style.display = 'block';
}

function loginHideLoading() {
    document.getElementById('loadingDiv').style.display = 'none';
}

function SubmitResultOnclick() {
    var btnSubmit = document.getElementById("btnSubmit");
    //if (btnSubmit.className == "") 
    var submitEnable = document.getElementById("submitBtnEnable");
    if(submitEnable && submitEnable.value == "yes")
    {
        var submitConfirm = confirm("Are you sure, you want to Confirm Submission of test.");
        if (submitConfirm) 
        {
            document.getElementById("submitConfirm").value="submitted";
            if(btnSubmit)
            {
                btnSubmit.setAttribute("onclick","javascript:void(0)");
                btnSubmit.className="disabled";
            }
            SubmitResult();
        }
    }
}


// For submitting test and call "Submit test method"
function SubmitResult() {
  
    HandlingQuestions();
    var JsonObjectArray = new Array();
    if (answerArray != null && answerArray.length != 0) {
        for (var i = 0; i < answerArray.length; i++) {
            var jsonObject = {
                "QuestionId": answerArray[i].QuestionId,
                "Answer": answerArray[i].AnswerString
            }
            JsonObjectArray.push(jsonObject);
        }
    }
    var hdnUserId = document.getElementById('ctl00_DefaultContent_hdnUserId');
    var TestUserId = hdnUserId.value;
    var JsonSubmitResult = {
        "TestId": testId,
        "TenantId": tenantId,
        "UserId": "0",
        "TestUserId": TestUserId,
        "TestResultCollection": JsonObjectArray
    };
    callAjaxService("SubmitTestResult", JsonSubmitResult, callBackJsonSubmitResult, callBackJsonSubmitResultFail);
}

function callBackJsonSubmitResult(response) {
    var chartObjectArray = GetChartObjectArray(chartArray); //confirm("Test submitted successfully..");  
    var jsonGridRequestString = {
        "ObjectList": chartObjectArray
    };
    ConstructSession(JSON.stringify(jsonGridRequestString), 'true');
    createCookie("EnableLastSession","",-1);
    createCookie("ALoginId","",-1);
    createCookie("CandidateAnswers","",-1);
}

function GetChartObjectArray(chartArray) {
    var answeredQues = 0;
    var group = "";
    var section = "";
    var noOfQuestions = 0;
    var chartDimensionArray = new Array();
    var chartDimensions = null;
    var leftQues = 0;
    for (var que in chartArray) {
        var answeredQues = 0;
        var qAnswered = 0;
        group = chartArray[que].Group;
        section = chartArray[que].Section;
        for (var ans in chartArray[que].Questions) {
            noOfQuestions = chartArray[que].Questions.length;
            for (var key in responseArray) {
                if (chartArray[que].Questions[ans] == responseArray[key].questionId) {
                    if (responseArray[key].isQuestionAnswered == true) qAnswered++;
                }
            }
            answeredQues = qAnswered;
            //answeredQues = getAnsweredQues(answerArray, chartArray[que].Questions[ans], answeredQues);
        }
        leftQues = noOfQuestions - answeredQues;
        chartDimensions = {
            "Group": group,
            "Section": section,
            "NoOfQuestions": noOfQuestions,
            "AnsweredQues": answeredQues,
            "LeftQues": leftQues
        }
        chartDimensionArray.push(chartDimensions);
    }
    return chartDimensionArray;
}

function getAnsweredQues(array, item, answeredQues) {
    for (var a = 0; a < array.length; a++) {
        if (item == array[a].QuestionId && answerArray[a].AnswerString != null && answerArray[a].AnswerString != "") {
            answeredQues = answeredQues + 1;
        }
    }
    return answeredQues;
}

function CallBackSession(arg, context) {
    window.location = "../CompletionChart.aspx";
}

function callBackJsonSubmitResultFail(response, fail) {
    alert("Submit Result Failed");
}

function right(e) {
    if (navigator.appName == 'Netscape' && (e.which == 3 || e.which == 2)) return false;
    else if (navigator.appName == 'Microsoft Internet Explorer' && (event.button == 2 || event.button == 3)) {
        alert("Sorry, you do not have permission to right click.");
        return false;
    }
    return true;
}

document.onmousedown = right;
document.onmouseup = right;
if (document.layers) window.captureEvents(Event.MOUSEDOWN);
if (document.layers) window.captureEvents(Event.MOUSEUP);
window.onmousedown = right;
window.onmouseup = right;
var message = "";

function clickIE() {
    if (document.all) {
        (message);
        return false;
    }
}

function clickNS(e) {
    if (document.layers || (document.getElementById && !document.all)) {
        if (e.which == 2 || e.which == 3) {
            (message);
            return false;
        }
    }
}

if (document.layers) {
    document.captureEvents(Event.MOUSEDOWN);
    document.onmousedown = clickNS;
} else {
    document.onmouseup = clickNS;
    document.oncontextmenu = clickIE;
}
document.oncontextmenu = new Function("return false");

function disableSelection(target) {
    if (typeof target.onselectstart != "undefined") //IE route
    target.onselectstart = function () {
        return false
    }
    else if (typeof target.style.MozUserSelect != "undefined") //Firefox route
    target.style.MozUserSelect = "none"
    else //All other route (ie: Opera)
    target.onmousedown = function () {
        return false
    }
    target.style.cursor = "default"
}
function checkForAlias()
{
    var obj = document.getElementById('btnLogin');    
    if (obj != null) obj.className = "disabled";    
    var tenantAlias = location.search.substring(location.search.indexOf('=') + 1);    
    if(tenantAlias=="" || tenantAlias==null)
        {
            var promptAlias = prompt("Alias Required.Please Input here",tenantAlias);
            if(promptAlias!=null)
            window.location = location.href + "?alias=" + promptAlias;
        } 
        else
        {
            //document.getElementById('logoDiv').innerHTML = "<img src=\"../assets/"+tenantAlias+"/companylogo.jpg\" />";
            if (obj != null) obj.className = "";
        }
}
 
function masterBodyOnload() {
    disableSelection(document.getElementById('masterBody'));
    if (document.all) {
        document.onkeydown = function () {
            var key_f5 = 116; // 116 = F5
            if (key_f5 == event.keyCode) {
                alert("Refreshing the page is not allowed");
                event.keyCode = 0;
                return false;
            }
        }
    }
    preloadImages();
}
function preloadImages() { 
    var virtaulDir = document.getElementById("virtualPath").value;
    if (virtaulDir == null || virtaulDir == "") {
        virtaulDir = "/";
    }  
    //alert(virtaulDir);
    var imagePath = location.protocol + "//" + location.host + virtaulDir +'App_Themes/HiRePro/Images/';    
    MM_preloadImages(imagePath+'btn.gif', imagePath+'colored_btns.gif', imagePath+'fl_btn.gif', imagePath+'nxt_btn.gif', imagePath+'prev_btn.gif');
}

function MM_preloadImages() 
{
  var d=document; 
  if(d.images)
  { 
    if(!d.MM_p) 
        d.MM_p=new Array();
    var i;
    var j=d.MM_p.length
    var a=MM_preloadImages.arguments; 
    for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0)
        { 
            d.MM_p[j] = new Image; 
            var img = a[i].substring(0, a[i].length);
            
            d.MM_p[j++].src=img; 
            
        }
  }
}

function checkRefresh() {
    if (confirm("Are you sure you want to navigate away from this page? Press OK to continue, or Cancel to stay on the current page.")) {
        window.location = "HTMLTestLogin.aspx";
    } else {}
}

function getInstruction() {
    var divMsg = document.getElementById('divTooltip');
    divMsg.innerHTML = secForTooltip + "<br/>" + instrForTooltip;
    divMsg.style.display = "block";
}

function clearDiv() {
    var divMsg = document.getElementById('divTooltip');
    divMsg.innerHTML = "";
    divMsg.style.display = "none";
}

function createCookie(name,value,days) {
var cookie=readCookie(name);

//    if(cookie!=null)
//    {
//        var date = new Date();
//        date.setTime(date.getTime()+(-1*24*60*60*1000));
//	    var expires = "; expires="+date.toGMTString();
//        document.cookie = name+"="+""+expires+"; path=/";
//    }

	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}
