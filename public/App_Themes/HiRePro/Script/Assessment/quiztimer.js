﻿// JavaScript Document
var bPaused = false;
var timerID = 0;

var total_seconds_left = 0;
var total_seconds_gone = 0;
var m;
var s;
function UpdateTimer() {
    if (!bPaused) {

        if (document.getElementById('spanElapsed') != null)
            document.getElementById('spanElapsed').innerHTML = FormatTimeFromSeconds(total_seconds_gone);
        if (document.getElementById('spanRemaining') != null && total_seconds_left >= 0)
            document.getElementById('spanRemaining').innerHTML = FormatTimeFromSeconds(total_seconds_left);
        if (isSectionWiseTimingTempVar == true) {
            createCookie(sectionWiseTiming[startIndex].SectionName, total_seconds_left, 100);
        }
        else {
            createCookie("TimeRemaining", total_seconds_left, 100);
        }
        //	   if(document.getElementById('timeRemaining').innerHTML=='00:00:00')
        //		{
        //			alert ("Time Up total time spend is : "+m+"-minutes, "+s+"-seconds");
        //		}
        total_seconds_left--;
        total_seconds_gone++;

    }

    if (total_seconds_left >= 0)
        timerID = setTimeout("UpdateTimer()", 1000);
    else {
        if (isSectionWiseTiming == true) {
            sectionWiseTimerStop();
        }
        else
            timerStop();
    }

}

function FormatTimeFromSeconds(secs) {

    var h = parseInt(secs / 3600); h = h.toFixed(0);

    secs = secs - (3600 * h);

    m = parseInt(secs / 60); m = m.toFixed(0);

    secs = secs - (60 * m);

    s = secs;

    if (h < 10) h = "0" + h;
    if (m < 10) m = "0" + m;
    if (s < 10) s = "0" + s;

    return h + ":" + m + ":" + s;
}

function timerStart() {
    var enableLastSession = readCookie("EnableLastSession");
    var h = 0;
    var timeremain = 0;
    if (isSectionWiseTimingTempVar == true && enableLastSession == "true") {
        timeremain = readCookie(sectionWiseTiming[startIndex].SectionName);
    }
    else
        timeremain = readCookie("TimeRemaining");
    var m;
    var s = 0;
    total_seconds_gone = 0;

    if (enableLastSession == "true") {
        if (timeremain != null) {
            document.getElementById('spanRemaining').innerHTML = FormatTimeFromSeconds(timeremain);
            m = parseInt(timeremain / 60);
            s = parseInt(timeremain % 60);
            total_seconds_gone = parseInt(document.getElementById('testTotalTime').value) * 60 - timeremain;
            if (timeremain <= 0) {
                if (isSectionWiseTimingTempVar == true) {
                    showNextSetOfQuestions(true);
                }
                else
                    timerStop();
            }
        }
        else {
            m = parseInt(document.getElementById('testTotalTime').value);
        }
    }
    else {
        m = parseInt(document.getElementById('testTotalTime').value);
    }
    if (isNaN(m)) {
        m = 0;
    }

    total_seconds_left += h * 3600;
    total_seconds_left += m * 60;
    total_seconds_left += s;

    if (total_seconds_left > 0) {
        timerID = setTimeout("UpdateTimer()", 1000);
    }
}

function timerStop() {
    if (timerID) {
        clearTimeout(timerID);
        timerID = 0;
        setInnerHtml("flag_list_td", "<img src=\"App_Themes/HiRePro/Images/ajax-start-loader.gif\" alt=\"\" />");
        document.getElementById("flag_list_td").align = "right";
        setValue("submitConfirm", "Submitted");
        hideDiv("optionsDiv");
        hideDiv("questionLabel");
        var browserHW = getBrowserWindowSize();
        var bheight = browserHW.height;
        bheight = bheight - 200;
        setStyleHeight("question_td", bheight, 250);
        setInnerHtml("question_header_div", "");
        var btnSubmit = document.getElementById("submit_btn");
        if (btnSubmit) {
            btnSubmit.setAttribute("onclick", "javascript:void(0)");
            btnSubmit.className = "but3-disabled";
            disableButton("submit_btn", true);
        }
        SubmitResult();
    }
}

function timerStopSectionWise() {
    if (timerID) {
        clearTimeout(timerID);
        timerID = 0;
    }
}

function Pause() {
    bPaused = !bPaused;
}

function Reset() {
    bPaused = false;
    timerID = 0;
    total_seconds_left = 0;
    total_seconds_gone = 0;

    //document.timeDisplay.timeRemaining.value = FormatTimeFromSeconds( total_seconds_left );
    document.getElementById('time_windowb').innerHTML = FormatTimeFromSeconds(total_seconds_left);
    document.timeDisplay.timeSpent.value = FormatTimeFromSeconds(total_seconds_gone);

    document.title = "";
}

//function createCookie(name, value, days) {
//    var cookie = readCookie(name);

//    //    if(cookie!=null)
//    //    {
//    //        var date = new Date();
//    //        date.setTime(date.getTime()+(-1*24*60*60*1000));
//    //	    var expires = "; expires="+date.toGMTString();
//    //        document.cookie = name+"="+""+expires+"; path=/";
//    //    }

//    if (days) {
//        var date = new Date();
//        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
//        var expires = "; expires=" + date.toGMTString();
//    }
//    else var expires = "";
//    document.cookie = name + "=" + value + expires + "; path=/";
//}

//function readCookie(name) {
//    var nameEQ = name + "=";
//    var ca = document.cookie.split(';');
//    for (var i = 0; i < ca.length; i++) {
//        var c = ca[i];
//        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
//        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
//    }
//    return null;
//}