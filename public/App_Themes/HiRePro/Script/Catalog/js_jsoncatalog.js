﻿////This file is used in order to make Catalog binding with controls such as
/// dropdown/select , multiselect, multicheck combo, Auto suggest , etc. in centralised manner.
var ControlAndTypeCollection = null;
var CatalogMasterCollection = null;
var requiredChooseOption;
function PopulateControl(controlAndCatalogMaster,chooseOptionRequired)
{
    requiredChooseOption = chooseOptionRequired;
    if(controlAndCatalogMaster!=null && controlAndCatalogMaster.length >0)
    {
        ControlAndTypeCollection = controlAndCatalogMaster;
        var TenantId = getTenantId();
        var UserId = getUserId();
        var tenantInfo = { "TenantId": TenantId, "UserId": UserId };
        var catalogMasterLength =  controlAndCatalogMaster.length;
        var catalogMasterNames = new Array();
        for(var i = 0;i<catalogMasterLength;i++)
        {
           if($.inArray(controlAndCatalogMaster[i].CatalogMasterName,catalogMasterNames) == -1)
           {
              catalogMasterNames[i] = controlAndCatalogMaster[i].CatalogMasterName;
           }
        }
        var getCatalogValuesRequest={"CatalogName":catalogMasterNames,"TenantId":tenantInfo.TenantId,"UserId":tenantInfo.UserId};
        callAjaxService("GetCatalogValues",getCatalogValuesRequest,callBackGetCatalogValuesForAll,callBackCatalogFail);
    }
}

function callBackCatalogFail(response) {
    ErrorMessageHandler(response._message);
}

function callBackGetCatalogValuesForAll(result)
{
    if(result!=null && result.CatalogMasterList.length >0)
    {
        CatalogMasterCollection = result.CatalogMasterList;
        if(null != ControlAndTypeCollection)
        {
            var controlCount = ControlAndTypeCollection.length;
            for(var j = 0;j < controlCount;j++)
            {
                var valueList =  GetCatalogValuesForAll(ControlAndTypeCollection[j].CatalogMasterName);
                switch(ControlAndTypeCollection[j].ControlType)
                {
                
                    case 'Dropdown':
                    case 'select':
                    case 'Multiselect':
                            fillCatalogDropDown(ControlAndTypeCollection[j].ControlId,valueList);
                            break;
                    case 'Multicheck':
                            break; 
                    case 'Autocomplete':
                            setDataSource(ControlAndTypeCollection[j].DataSource,valueList,"Master");              
                }
            }
        }
    }
    disableLoading();
    ControlAndTypeCollection = null;
    CatalogMasterCollection = null;    
}


//This Function is used to set datasource in a global variable through referenc.
//This Global variable is used by auto complete controls and variables are declared in respective pages
function setDataSource(dataSource,valueList,type)
{
    if(!valueList)
        return;
    if(!dataSource)
        return;
    if(type=="Master")
    {
       for(var i =0; i < valueList.length; i++)
       {
           dataSource[i] = { name: valueList[i].Name, id: valueList[i].CatalogValueId }   
       }
   }
   else if(type == "Entity")
   {
        for(var i =0; i < valueList.length; i++)
       {
           dataSource[i] = { name: valueList[i].CommonName, id: valueList[i].CommonId }   
       }
   }
}


//This function is used to set datasource for auto complete control
function setDataToControl(controlId,dataSource)
{
   if(controlId !=null)
   {
    $(controlId).autocomplete(dataSource, {
    
            minChars: 0,
            width: 210,
            matchContains: false,
            autoFill: true,
            multiple: true,
            cacheLength: 50,
            formatItem: function(row, i, max) {
                return row.name;
            },
            formatMatch: function(row, i, max) {
                return row.name;
            },
            formatResult: function(row) {
                return row.name;
            }
        });
   }     
}

function GetCatalogValuesForAll(catalogMasterName)
{
    var catalogMasterCount = CatalogMasterCollection.length;
    for(var i = 0;i < catalogMasterCount;i++)
    {
        if(CatalogMasterCollection[i].Name == catalogMasterName)
        {
            return CatalogMasterCollection[i].CatalogValues;
            break;
        }
    }
}


//This Function is used to bind data to a dropdown/select or multiselect
function fillCatalogDropDown(dropDownId, valueArray) {
    var dropDown = document.getElementById(dropDownId);
    if (dropDown != null) {
    dropDown.options.length = 0;
    if(valueArray)
    {
        var c=valueArray.length;
            for (var i = 0; i < c; i++) 
            {
                var name=valueArray[i].Name;
                var id=valueArray[i].CatalogValueId;            
                var option = document.createElement("option");
                option.value = id;
                option.innerHTML = name;
                option.title = name;
                dropDown.appendChild(option);
            }
       
           if(requiredChooseOption)
           {     
            var opChoose = document.createElement("option");
            opChoose.value = "9999";
            opChoose.innerHTML = "Choose";
            opChoose.title = "Choose";
            opChoose.selected = "true";       
           // var c1=ddlApproveBy.options.length;
            dropDown.appendChild(opChoose);    
          }       
        }
    }
}






//---------------------------------------------------//
///Implementation of GetList of Entity
var getListOfServiceCallindex = 0;
var ControlAndEntityTypeList = null;
var len = 0;

function CommonGetListOfEntity(ControlAndEntity)
{
    if(ControlAndEntity !=null && ControlAndEntity.length >0)
    {
        ControlAndEntityTypeList = ControlAndEntity;
        len = ControlAndEntity.length;        
        GetListOfEntityServiceCall();     
    }
}

function GetListOfEntityServiceCall()
{
    var TenantId = getTenantId();
    var UserId = getUserId();
    var tenantInfo = { "TenantId": TenantId, "UserId": UserId };
    
    var request = { "EntityType": ControlAndEntityTypeList[getListOfServiceCallindex].EntityType };
    var getEntitytyListRequest={"CommonNameRequest":request,"TenantId":tenantInfo.TenantId,"UserId":tenantInfo.UserId};
    callAjaxService("GetListOfEntity",getEntitytyListRequest,callbackCommonGetListOfEntity,callBackCatalogFail);  
}

function callbackCommonGetListOfEntity(response)
{
    if(response!=null)
    {
       if(ControlAndEntityTypeList != null)
           switch(ControlAndEntityTypeList[getListOfServiceCallindex].ControlType)
            {
                case 'Dropdown':
                case 'select':
                case 'Multiselect':
                        fillEntityToDropdown(ControlAndEntityTypeList[getListOfServiceCallindex].ControlId,response.GetNamesOutputList);
                        break;
                case 'Multicheck':
                        break;    
                case 'Autocomplete':  
                        setDataSource(ControlAndEntityTypeList[getListOfServiceCallindex].DataSource,response.GetNamesOutputList,"Entity");  
                        break;                               
            } 
        getListOfServiceCallindex++;
        if(getListOfServiceCallindex<len)
        {
            GetListOfEntityServiceCall();
        }    
        else
        {
            getListOfServiceCallindex = 0;
            ControlAndEntityTypeList = null;
            len = 0;
        }  
    }
    disableLoading();
}


//This function is used to fill dropdown/select or multiselect control with entitylist values
function fillEntityToDropdown(controlId,valueList)
{
    var control = document.getElementById(controlId);
    if(control)
    {
        control.options.length = 0;
        option = new Option("Choose", '9999');
        control.options[0] = option;
        var len = valueList.length;
        var index = 0;
         for (var i = 1; i <= len; i++)
         {
            option = new Option(valueList[index].CommonName, valueList[index].CommonId);
            control.options[i] = option;
            index++;
         }
    }
}




//////////////////////////////////////////////////////////////////////
// Functions for autocomplete multicheckbox
function getSelectedIdsInMultiCheckedAutoFillCombo(controlId, datasource)
 { 
    var selectedText = $(controlId).val();
    selectedText = $.trim(selectedText);
    var selectedTextArray = selectedText.split(',');       
    var selectedIdsArray = new Array();
    var selectedIndexArray = new Array();
    if (selectedTextArray != null && selectedTextArray.length != 0)
    {
    
        for (var i = 0; i < selectedTextArray.length; i++) 
        {  
           if(selectedTextArray[i] != "")
           {         
               for (var j = 0; j < datasource.length; j++)
               {
                if (selectedTextArray[i].replace(/\s/g, "") == datasource[j].name.replace(/\s/g, "")) {
                    selectedIdsArray.push(datasource[j].id);
                    break;
                }
            }
         }
       }
    }
    return selectedIdsArray;
}

//Sets value i.e text against ids in text box with comma separated values
function setTextInTheMultiCheckedAutoFillCombo(controlId, ids, dataSource) 
{
    var val = "";
    if(ids!=null){
    for (var i = 0; i < ids.length; i++) {
        if (dataSource != null) {
            for (var j = 0; j < dataSource.length; j++) {
                if (ids[i] == dataSource[j].id) {
                    val += dataSource[j].name + ", ";
                    break;
                }
            }
        }
     }
    $(controlId).get(0).innerHTML = val;
    }
}


function addedIdsInMultiCheckedAutoFillCombo(oldIds, controlid, datasource) {
    var currentSelectedIds = getSelectedIdsInMultiCheckedAutoFillCombo(controlid, datasource);
    var newAddedIds = new Array();
    if (oldIds != undefined) {
        for (var i = 0; i < currentSelectedIds.length; i++) {
            var isMatch = false;
            for (var j = 0; j < oldIds.length; j++) {
                if (oldIds[j] == currentSelectedIds[i]) {
                    isMatch = true;
                    break;
                }
            }
            if (!isMatch)
                newAddedIds.push(currentSelectedIds[i]);
        }
    }
    return newAddedIds;
}

function removedIdsInMultiCheckAutoFillCombo(oldIds, controlid , datasource) 
{
    var removeIds = new Array();
    if (oldIds != undefined) {
        var currentSelectedIds = getSelectedIdsInMultiCheckedAutoFillCombo(controlid , datasource);

        for (var i = 0; i < oldIds.length; i++) {
            var isMatch = false;
            for (var j = 0; j < currentSelectedIds.length; j++) {
                if (oldIds[i] == currentSelectedIds[j]) {
                    isMatch = true;
                    break;
                }
            }
            if (!isMatch)
                removeIds.push(oldIds[i]);
        }
    }
    return removeIds;
}

