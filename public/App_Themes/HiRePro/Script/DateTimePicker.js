//Global variables
var winCal;
var dtToday = new Date();
var Cal;
var docCal;
var MonthName = ["January", "February", "March", "April", "May", "June", "July",
	"August", "September", "October", "November", "December"];
var WeekDayName = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
var exDateTime; //Existing Date and Time

//Configurable parameters
var cnTop = "200"; //top coordinate of calendar window.
var cnLeft = "500"; //left coordinate of calendar window
var WindowTitle = "DateTime Picker"; //Date Time Picker title.
var WeekChar = 2; //number of character for week day. if 2 then Mo,Tu,We. if 3 then Mon,Tue,Wed.
var CellWidth = 20; //Width of day cell.
var DateSeparator = "-"; //Date Separator, you can change it to "/" if you want.
var TimeMode = 24; //default TimeMode value. 12 or 24

var ShowLongMonth = true; //Show long month name in Calendar header. example: "January".
var ShowMonthYear = true; //Show Month and Year in Calendar header.
var MonthYearColor = "#cc0033"; //Font Color of Month and Year in Calendar header.
var WeekHeadColor = "#0099CC"; //Background Color in Week header.
var SundayColor = "#6699FF"; //Background color of Sunday.
var SaturdayColor = "#CCCCFF"; //Background color of Saturday.
var WeekDayColor = "white"; //Background color of weekdays.
var FontColor = "blue"; //color of font in Calendar day cell.
var TodayColor = "#FFFF33"; //Background color of today.
var SelDateColor = "#069"; //Backgrond color of selected date in textbox.
var YrSelColor = "#cc0033"; //color of font of Year selector.
var ThemeBg = ""; //Background image of Calendar window.
var display = false;
var calDiv = null;
//end Configurable parameters
//end Global variable

function NewCal(pCtrl, pFormat, pShowTime, pTimeMode, wereToDisplay) {
    if ( document.getElementById('toDisp') == null )
    {
        var ctl = document.createElement('input');
        ctl.id = "toDisp";
        ctl.value = "false";
        ctl.type = "hidden";
        document.body.appendChild(ctl);
    }
    
    calDiv = wereToDisplay;
    
    Cal = new Calendar1(dtToday);
    if ((pShowTime != null) && (pShowTime)) 
    {
        Cal.ShowTime = true;
        if ((pTimeMode != null) && ((pTimeMode == '12') || (pTimeMode == '24'))) {
            TimeMode = pTimeMode;
        }
    }
    
    if (pCtrl != null)
        Cal.Ctrl = pCtrl;
    if (pFormat != null)
        Cal.Format = pFormat.toUpperCase();

    exDateTime = document.getElementById(pCtrl).value;
    if (exDateTime != "")//Parse Date String
    {
        var Sp1; //Index of Date Separator 1
        var Sp2; //Index of Date Separator 2 
        var tSp1; //Index of Time Separator 1
        var tSp1; //Index of Time Separator 2
        var strMonth;
        var strDate;
        var strYear;
        var intMonth;
        var YearPattern;
        var strHour;
        var strMinute;
        var strSecond;
        //parse month
        Sp1 = exDateTime.indexOf(DateSeparator, 0);
        Sp2 = exDateTime.indexOf(DateSeparator, (parseInt(Sp1) + 1));

        if ((Cal.Format.toUpperCase() == "DDMMYYYY") || (Cal.Format.toUpperCase() == "DDMMMYYYY")) {
            strMonth = exDateTime.substring(Sp1 + 1, Sp2);
            strDate = exDateTime.substring(0, Sp1);
        }
        else if ((Cal.Format.toUpperCase() == "MMDDYYYY") || (Cal.Format.toUpperCase() == "MMMDDYYYY")) {
            strMonth = exDateTime.substring(0, Sp1);
            strDate = exDateTime.substring(Sp1 + 1, Sp2);
        }
        if (isNaN(strMonth))
            intMonth = Cal.GetMonthIndex(strMonth);
        else
            intMonth = parseInt(strMonth, 10) - 1;
        if ((parseInt(intMonth, 10) >= 0) && (parseInt(intMonth, 10) < 12))
            Cal.Month = intMonth;
        //end parse month
        //parse Date
        if ((parseInt(strDate, 10) <= Cal.GetMonDays()) && (parseInt(strDate, 10) >= 1))
            Cal.Date = strDate;
        //end parse Date
        //parse year
        strYear = exDateTime.substring(Sp2 + 1, Sp2 + 5);
        YearPattern = /^\d{4}$/;
        if (YearPattern.test(strYear))
            Cal.Year = parseInt(strYear, 10);
        //end parse year
        //parse time
        if (Cal.ShowTime == true) {
            tSp1 = exDateTime.indexOf(":", 0);
            tSp2 = exDateTime.indexOf(":", (parseInt(tSp1) + 1));
            strHour = exDateTime.substring(tSp1, (tSp1) - 2);
            Cal.SetHour(strHour);
            strMinute = exDateTime.substring(tSp1 + 1, tSp2);
            Cal.SetMinute(strMinute);
            strSecond = exDateTime.substring(tSp2 + 1, tSp2 + 3);
            Cal.SetSecond(strSecond);
        }
    }
    RenderCal(wereToDisplay);
}

function RenderCal(wereToDisplay) {
    var vCalHeader = "";
    var vCalData;
    var vCalTime;
    var i;
    var j;
    var SelectStr;
    var vDayCount = 0;
    var vFirstDay;
    docCal = "";
  
    
    //vCalHeader = "<div id=\"Calendar\"><table border=0 cellpadding=1 cellspacing=0 width='100%' align=\"center\" valign=\"top\" style=\"border:1px solid #1f6d8c; padding:1px; background:#ffffff;\">\n";
    //Month Selector
    //vCalHeader += "<tr>\n<td colspan='7'><table border=0 width='100%' cellpadding=0 cellspacing=0><tr><td align='left'>\n";
    vCalHeader += "<div class=\"calendar\" ><ul><li class=\"top click\"><select name=\"MonthSelector\" style=\"width:70px\" onChange=\"javascript:Cal.SwitchMth(this.selectedIndex);RenderCal(" + wereToDisplay.id + ");\">\n";
    for (i = 0; i < 12; i++) {
        if (i == Cal.Month)
            SelectStr = "Selected";
        else
            SelectStr = "";
        vCalHeader += "<option " + SelectStr + " value >" + MonthName[i] + "\n";
    }
    vCalHeader += "</select></li>";//</td>";
    //Year selector
    vCalHeader += "\n<li class=\"right\"><a href=\"javascript:Cal.IncYear();RenderCal(" + wereToDisplay.id + ")\"><img src=\"App_Themes/HirePro_Json/images/arrowlft.gif\" /></a></li><li class=\"right\">" + Cal.Year + "</li><li class=\"right\"><a href=\"javascript:Cal.DecYear();RenderCal(" + wereToDisplay.id + ")\"><img src=\"App_Themes/HirePro_Json/images/arrowright.gif\" /></a></li></ul>";//</tr></table></td>\n";
    //vCalHeader += "</tr>";
    //Calendar header shows Month and Year
    vCalHeader += "<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\" valign=\"top\" align=\"center\">";
    if (ShowMonthYear)
        vCalHeader += "<caption>" + Cal.GetMonthName(ShowLongMonth) + " " + Cal.Year + "</caption>\n";
    //Week day header
    vCalHeader += "<thead><tr>";
    for (i = 0; i < 7; i++) {
        vCalHeader += "<th align='center'>" + WeekDayName[i].substr(0, WeekChar) + "</th>";
    }
    vCalHeader += "</tr></thead><tbody>";
    //docCal.write(vCalHeader);
    docCal += vCalHeader;
    //Calendar detail
    CalDate = new Date(Cal.Year, Cal.Month);
    CalDate.setDate(1);
    vFirstDay = CalDate.getDay();
    vCalData = "<tr>";
    for (i = 0; i < vFirstDay; i++) {
        vCalData = vCalData + GenCell();
        vDayCount = vDayCount + 1;
    }
    for (j = 1; j <= Cal.GetMonDays(); j++) {
        var strCell;
        vDayCount = vDayCount + 1;
        if ((j == dtToday.getDate()) && (Cal.Month == dtToday.getMonth()) && (Cal.Year == dtToday.getFullYear()))
            strCell = GenCell(j, true, TodayColor); //Highlight today's date
        else {
            if (j == Cal.Date) {
                strCell = GenCell(j, true, SelDateColor);
            }
            else {
                if (vDayCount % 7 == 0)
                    strCell = GenCell(j, false, SaturdayColor);
                else if ((vDayCount + 6) % 7 == 0)
                    strCell = GenCell(j, false, SundayColor);
                else
                    strCell = GenCell(j, null, WeekDayColor);
            }
        }
        vCalData = vCalData + strCell;

        if ((vDayCount % 7 != 0) && (j > Cal.GetMonDays())) {
            vCalData = vCalData + "</tr>\n";
            }
        if ((vDayCount % 7 == 0) && (j < Cal.GetMonDays())) {
            vCalData = vCalData + "</tr>\n<tr>";
        }
    }
    docCal += vCalData;
    //Time picker
    if (Cal.ShowTime) {
        var showHour;
        showHour = Cal.getShowHour();
        vCalTime = "<tfoot><tr>\n<td class=\"tdCal\" colspan='7' align='center'>";
        vCalTime += "<input type='text' name='hour' maxlength=2 size=1 style=\"WIDTH: 22px\" value=" + showHour + " onchange=\"javascript:Cal.SetHour(this.value)\">";
        vCalTime += " : ";
        vCalTime += "<input type='text' name='minute' maxlength=2 size=1 style=\"WIDTH: 22px\" value=" + Cal.Minutes + " onchange=\"javascript:Cal.SetMinute(this.value)\">";
        vCalTime += " : ";
        vCalTime += "<input type='text' name='second' maxlength=2 size=1 style=\"WIDTH: 22px\" value=" + Cal.Seconds + " onchange=\"javascript:Cal.SetSecond(this.value)\">";
        if (TimeMode == 12) {
            var SelectAm = (parseInt(Cal.Hours, 10) < 12) ? "Selected" : "";
            var SelectPm = (parseInt(Cal.Hours, 10) >= 12) ? "Selected" : "";

            vCalTime += "<select name=\"ampm\" onchange=\"javascript:Cal.SetAmPm(this.options[this.selectedIndex].value);\">";
            vCalTime += "<option " + SelectAm + " value=\"AM\">AM</option>";
            vCalTime += "<option " + SelectPm + " value=\"PM\">PM<option>";
            vCalTime += "</select>";
        }
        vCalTime += "\n</td>\n</tr></tfoot>";
         docCal += vCalTime;
    }
    //end time picker
    docCal += "\n</table></div>";
        
    if (typeof (wereToDisplay) != "undefined")
            wereToDisplay.innerHTML = docCal;
            
    var toDisplay = document.getElementById('toDisp');
    
    if ( toDisplay.value == "false" )
        toDisplay.value = "true";
    else
        toDisplay.value = "false";

    if (wereToDisplay.length != undefined && wereToDisplay.length > 0)
        wereToDisplay = wereToDisplay[0]; 
   
    if ( toDisplay.value == "true" )
    {
        if (typeof (wereToDisplay) != "undefined")
        {
            wereToDisplay.innerHTML = docCal;
            wereToDisplay.style.display = "inline"; 
            wereToDisplay.style.width = "125px";
            wereToDisplay.style.height = "125px";
        }
    }
    else
    {
        if (typeof (wereToDisplay) != "undefined")
            wereToDisplay.innerHTML = "";
        wereToDisplay.style.display = "none";
    }
}

function GenCell(pValue, pHighLight, pColor)//Generate table cell with value
{
    var PValue;
    var PCellStr;
    var vColor;
    var vHLstr1; //HighLight string
    var vHlstr2;
    var vTimeStr;

    if (pValue == null)
        PValue = "";
    else
        PValue = pValue;

    if (pColor != null && pColor == "#069")
        pColor = "tab_row_backcolor_Selected";
    else
        pColor = "tab_row_backcolor";
        
    if ((pHighLight != null) && (pHighLight))
    { vHLstr1 = "color='red'><b>"; vHLstr2 = "</b>"; }
    else
    { vHLstr1 = ">"; vHLstr2 = ""; }

    if (Cal.ShowTime) {
        vTimeStr = "document.getElementById('" + Cal.Ctrl + "').value+=' '+" + "Cal.getShowHour()" + "+':'+" + "Cal.Minutes" + "+':'+" + "Cal.Seconds";
        if (TimeMode == 12)
            vTimeStr += "+' '+Cal.AMorPM";
    }
    else
        vTimeStr = "";
    if (PValue == "")
        PCellStr = "<td>&nbsp;</td>";
    else {
        PCellStr = "<td><a href=\"Javascript:;\" onclick=\"javascript:closeCal();document.getElementById('" + Cal.Ctrl + "').value='" + Cal.FormatDate(PValue) + "';" + vTimeStr + ";\">" + PValue + "</a>" + vHLstr2 + "</td>";
    }
    return PCellStr;
}

function closeCal()
{
    document.getElementById('toDisp').value = "false";
    if (calDiv.length != undefined && calDiv.length > 0)
        calDiv = calDiv[0];
    calDiv.style.display = "none";
}

function Calendar1(pDate, pCtrl) {
    //Properties
    this.Date = pDate.getDate(); //selected date
    this.Month = pDate.getMonth(); //selected month number
    this.Year = pDate.getFullYear(); //selected year in 4 digits
    this.Hours = pDate.getHours();

    if (pDate.getMinutes() < 10)
        this.Minutes = "0" + pDate.getMinutes();
    else
        this.Minutes = pDate.getMinutes();

    if (pDate.getSeconds() < 10)
        this.Seconds = "0" + pDate.getSeconds();
    else
        this.Seconds = pDate.getSeconds();

    this.MyWindow = winCal;
    this.Ctrl = pCtrl;
    this.Format = "ddMMyyyy";
    this.Separator = DateSeparator;
    this.ShowTime = false;
    if (pDate.getHours() < 12)
        this.AMorPM = "AM";
    else
        this.AMorPM = "PM";
}

function GetMonthIndex(shortMonthName) {
    for (i = 0; i < 12; i++) {
        if (MonthName[i].substring(0, 3).toUpperCase() == shortMonthName.toUpperCase())
        { return i; }
    }
}
Calendar.prototype.GetMonthIndex = GetMonthIndex;

function IncYear()
{ Cal.Year++; document.getElementById('toDisp').value = "false";}
Calendar.prototype.IncYear = IncYear;

function DecYear()
{ Cal.Year--; document.getElementById('toDisp').value = "false"; }
Calendar.prototype.DecYear = DecYear;

function SwitchMth(intMth)
{ Cal.Month = intMth; document.getElementById('toDisp').value = "false";}
Calendar.prototype.SwitchMth = SwitchMth;

function SetHour(intHour) {
    var MaxHour;
    var MinHour;
    if (TimeMode == 24)
    { MaxHour = 23; MinHour = 0 }
    else if (TimeMode == 12)
    { MaxHour = 12; MinHour = 1 }
    else
        alert("TimeMode can only be 12 or 24");
    var HourExp = new RegExp("^\\d\\d$");
    if (HourExp.test(intHour) && (parseInt(intHour, 10) <= MaxHour) && (parseInt(intHour, 10) >= MinHour)) {
        if ((TimeMode == 12) && (Cal.AMorPM == "PM")) {
            if (parseInt(intHour, 10) == 12)
                Cal.Hours = 12;
            else
                Cal.Hours = parseInt(intHour, 10) + 12;
        }
        else if ((TimeMode == 12) && (Cal.AMorPM == "AM")) {
            if (intHour == 12)
                intHour -= 12;
            Cal.Hours = parseInt(intHour, 10);
        }
        else if (TimeMode == 24)
            Cal.Hours = parseInt(intHour, 10);
    }
}
Calendar.prototype.SetHour = SetHour;

function SetMinute(intMin) {
    var MinExp = new RegExp("^\\d\\d$");
    if (MinExp.test(intMin) && (intMin < 60))
        Cal.Minutes = intMin;
}
Calendar.prototype.SetMinute = SetMinute;

function SetSecond(intSec) {
    var SecExp = new RegExp("^\\d\\d$");
    if (SecExp.test(intSec) && (intSec < 60))
        Cal.Seconds = intSec;
}
Calendar.prototype.SetSecond = SetSecond;

function SetAmPm(pvalue) {
    this.AMorPM = pvalue;
    if (pvalue == "PM") {
        this.Hours = (parseInt(this.Hours, 10)) + 12;
        if (this.Hours == 24)
            this.Hours = 12;
    }
    else if (pvalue == "AM")
        this.Hours -= 12;
}
Calendar.prototype.SetAmPm = SetAmPm;

function getShowHour() {
    var finalHour;
    if (TimeMode == 12) {
        if (parseInt(this.Hours, 10) == 0) {
            this.AMorPM = "AM";
            finalHour = parseInt(this.Hours, 10) + 12;
        }
        else if (parseInt(this.Hours, 10) == 12) {
            this.AMorPM = "PM";
            finalHour = 12;
        }
        else if (this.Hours > 12) {
            this.AMorPM = "PM";
            if ((this.Hours - 12) < 10)
                finalHour = "0" + ((parseInt(this.Hours, 10)) - 12);
            else
                finalHour = parseInt(this.Hours, 10) - 12;
        }
        else {
            this.AMorPM = "AM";
            if (this.Hours < 10)
                finalHour = "0" + parseInt(this.Hours, 10);
            else
                finalHour = this.Hours;
        }
    }
    else if (TimeMode == 24) {
        if (this.Hours < 10)
            finalHour = "0" + parseInt(this.Hours, 10);
        else
            finalHour = this.Hours;
    }
    return finalHour;
}
Calendar.prototype.getShowHour = getShowHour;

function GetMonthName(IsLong) {
    var Month = MonthName[this.Month];
    if (IsLong)
        return Month;
    else
        return Month.substr(0, 3);
}
Calendar.prototype.GetMonthName = GetMonthName;

function GetMonDays()//Get number of days in a month
{
    var DaysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    if (this.IsLeapYear()) {
        DaysInMonth[1] = 29;
    }
    return DaysInMonth[this.Month];
}
Calendar.prototype.GetMonDays = GetMonDays;

function IsLeapYear() {
    if ((this.Year % 4) == 0) {
        if ((this.Year % 100 == 0) && (this.Year % 400) != 0) {
            return false;
        }
        else {
            return true;
        }
    }
    else {
        return false;
    }
}
Calendar.prototype.IsLeapYear = IsLeapYear;

function FormatDate(pDate) {
    if (this.Format.toUpperCase() == "DDMMYYYY")
        return (pDate + DateSeparator + (this.Month + 1) + DateSeparator + this.Year);
    else if (this.Format.toUpperCase() == "DDMMMYYYY")
        return (pDate + DateSeparator + this.GetMonthName(false) + DateSeparator + this.Year);
    else if (this.Format.toUpperCase() == "MMDDYYYY")
        return ((this.Month + 1) + DateSeparator + pDate + DateSeparator + this.Year);
    else if (this.Format.toUpperCase() == "MMMDDYYYY")
        return (this.GetMonthName(false) + DateSeparator + pDate + DateSeparator + this.Year);
}
Calendar.prototype.FormatDate = FormatDate;	