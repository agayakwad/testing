//jQuery.HiReProCountdownTimerPlugin.js
(function ($) {
    $.fn.extend({
        //plugin name - countdownProgressbar
        countdownProgressbar: function (arg) {
            //Settings list and the default values
            var defaults = {
                totalTime: 120, //Total Time(in secs)
                remainingTime: 120, //keeps remaining time(in secs)(function use)
                setValue: null, //Takes remaining time to set progressbar to specifiy time(in secs)
                progressbarDivision: 0, //Division of progress to be reduce in 1 sec
                intervalFunction: null, //function to be called for calculating and reducing progressbar(function use)
                progressBarCount: 99.6, //Total no of units in progressbar(function use)
                alarmPercentage: [20], //Percent of time at which progressbar will give alarm(percent in integer format)
                alarmPercent: [0], //Calculated time for alarm(function use)
                alarmTimeOut: 20, //No of time for which alarm will be active(function use)
                displayTotalTime: true, //Display total time on top of progressbar
                displayRemainingTime: true, //Display remaining time on bottom left of progressbar, if displayElapsedTime is "false" then remaining time is displayed in middle of progressbar
                displayElapsedTime: true, //Display elapsed time on bottom right of progressbar
                stopTimer: false
            };

            var options = defaults;
            var Options = $(this).data('Option');
            if (Options)
                $.extend(options, Options);

            if (arg) {
                if (arg.totalTime)
                    options.totalTime = arg.totalTime;
                if (arg.alarmPercentage)
                    options.alarmPercentage = arg.alarmPercentage;
                if (arg.setValue)
                    options.setValue = arg.setValue;
            }
            return this.each(function () {
                var o = options;
                var obj = $(this);
                //Resetting Interval used to set counter
                if (o.intervalFunction != null || o.stopTimer == true) {
                    clearInterval(o.intervalFunction);
                }
                else if (!o.stopTimer) {
                    //Calculate alarm percent to sec
                    for (var alarmIndex = 0; alarmIndex < o.alarmPercentage.length; alarmIndex++)
                        o.alarmPercent[alarmIndex] = o.totalTime * (o.alarmPercentage[alarmIndex] / 100);
                    //Calculate progress division for given total time
                    o.progressbarDivision = (99.6 / o.totalTime);
                    //Setting value for all variables if setValue is specified
                    var elapsedTime = 0;
                    if (o.setValue) {
                        o.remainingTime = o.setValue;
                        elapsedTime = o.totalTime - o.remainingTime;
                        o.progressBarCount = 99.6 - (elapsedTime * o.progressbarDivision);
                    }
                    else {
                        o.progressBarCount = 99.6;
                        o.remainingTime = o.totalTime;
                    }
                    //create div for Total Time display
                    if (o.displayTotalTime && $('#progressBarTotalTimeDiv' + obj.attr('id')).length == 0) {
                        var totalTimeDiv = $('<div />').attr({ 'id': 'progressBarTotalTimeDiv' + obj.attr('id') }).css({ 'width': '400px', 'height': '20px', 'text-align': 'center' }).html('Total Time: ' + getTimeFormate(o.totalTime)).appendTo(obj);
                    }
                    //create div for progressbar
                    if ($('#progressBarOutterDiv' + obj.attr('id')).length == 0) {
                        var outterDiv = $('<div />').attr({ 'id': 'progressBarOutterDiv' + obj.attr('id') }).css({ 'border': '1px solid #BADAF2', 'width': '400px', 'height': '10px' }).appendTo(obj);
                        var innerDiv = $('<div />').attr({ 'id': 'progressBarInnerDiv' + obj.attr('id') }).css({ 'background-repeat': 'no-repeat', 'background-image': 'url(App_Themes/HiRePro/theme1/images/gradient.php.png)', 'border': '1px solid #BADAF2', 'width': '99.6%', 'height': '92%' }).appendTo(outterDiv);
                    }
                    //create div for elapsed Time display
                    if (o.displayElapsedTime && $('#progressBarFooterTimeDiv' + obj.attr('id')).length == 0) {
                        var totalTimeDiv = $('<div />').attr({ 'id': 'progressBarFooterTimeDiv' + obj.attr('id') }).css({ 'width': '400px', 'height': '20px' }).appendTo(obj);
                        if (o.displayElapsedTime && $('#progressBarElapsedTimelbl' + obj.attr('id')).length == 0)
                            totalTimeDiv.append($('<span />').css('float', 'right').html('Elapsed: ').append($('<span />').attr('id', 'progressBarElapsedTimelbl' + obj.attr('id')).html(getTimeFormate(elapsedTime))));
                    }
                    //create div for remaining Time display
                    if (o.displayRemainingTime) {
                        if (o.displayElapsedTime && $('#progressBarRemainingTimelbl' + obj.attr('id')).length == 0) {
                            totalTimeDiv.append($('<span />').css('float', 'left').html('Remaining: ').append($('<span />').attr('id', 'progressBarRemainingTimelbl' + obj.attr('id')).html(getTimeFormate(o.remainingTime))));
                        }
                        else if (!o.displayElapsedTime && $('#progressBarRemainingTimelbl' + obj.attr('id')).length == 0) {
                            outterDiv.prepend($('<span />').attr('id', 'progressBarRemainingTimelbl' + obj.attr('id')).css({ 'position': 'absolute' }));
                        }
                    }
                    $(this).data('Option', o);
                    //Setting interval for progress control function
                    o.intervalFunction = setInterval(function () { incrementProgressBar(o, obj.attr('id')) }, 1000);
                }
            });
        }
    });
    //Private function
    //Calculate remaining time and position of progress bar
    var incrementProgressBar = function (o, id) {
        o.progressBarCount = (o.progressBarCount - o.progressbarDivision).toFixed(4);
        $('#progressBarInnerDiv' + id).css('width', o.progressBarCount + '%');
        o.remainingTime = o.remainingTime - 1;
        var remainingTimer = getTimeFormate(o.remainingTime);
        $('#progressBarRemainingTimelbl' + id).html(remainingTimer);
        var left = $('#progressBarOutterDiv' + id).offset().left + (($('#progressBarOutterDiv' + id).width() / 2) - ($('#progressBarRemainingTimelbl' + id).width() / 2));
        $('#progressBarRemainingTimelbl' + id).css('left', left);

        var elapsedTime = o.totalTime - o.remainingTime;
        var elapsedTimer = getTimeFormate(elapsedTime);
        $('#progressBarElapsedTimelbl' + id).html(elapsedTimer);
        if (jQuery.inArray(o.remainingTime, o.alarmPercent) != -1) {
            glowProgress('progressBarInnerDiv' + id, o.alarmTimeOut);
        }
        if (o.remainingTime <= 0)
            clearInterval(o.intervalFunction);
    };
    //Create glow effect at time of alarm
    var glowProgress = function (id, alarmTimeOut) {
        $('#' + id).toggle();
        alarmTimeOut--;
        if (alarmTimeOut > 0) {
            setTimeout(function () { glowProgress(id, alarmTimeOut) }, 100);
        }
    };
    //Return time in [hh:mm:ss] format
    var getTimeFormate = function (secs) {
        var h = parseInt(secs / 3600).toFixed(0);
        var m = secs - (3600 * h);
        m = parseInt(m / 60).toFixed(0);
        var s = secs - (60 * m) - (3600 * h);
        var time = "";
        if (h && h > 0)
            time = (h > 9) ? h + ":" : "0" + h + ":";
        else
            time = "00:";
        if (m && m > 0)
            time += (m > 9) ? m + ":" : "0" + m + ":";
        else
            time += "00:";
        if (s && s > 0)
            time += (s > 9) ? s : "0" + s;
        else
            time += "00";
        return time;
    };
})(jQuery);