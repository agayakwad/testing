﻿var htmlTest = new HtmlTest();
var TenantInfo = {};
getTenantInfo();
var mandatoryCollection;

function HtmlTest() {
    /*Variables*/
    this.isLoginEnabled = false

    var serviceCallMethodNames = [];
    var collegeArray = [];
    var collgeObjArray = [];
    var degreeArray = [];
    var degreeObjArray = [];
    var departmentArray = [];
    var departmentObjArray = [];
    var answerArray = [];
    var responseArray = [];
    var sectionWiseTiming = [];
    var intermediateAnswerCollection = {};

    var intermediateAnswerObjCollection = [];
    var intermediateIndexer = 0;
    var IsintermediateSaveEnabled = true;
    var intermediateIntervalInSec = 300;
    var intermediateTimerId = 0;
    var intermediateGroupCollection = {};

    var nonMandatoryCollection;


    var currentQuestionNo = 0;
    var loginValidateCount = 0;
    var candidateId = 0;
    var testId = 0;
    var typeOfQuestion = 0;
    var sectionIndex = 0;
    var optionalSectionIndex = 0;
    var totalTime = 0;
    var totalRemainingSeconds = 0;
    var totalElapsedSeconds = 0;
    var timerID = 0;
    var questionIndexer = 0;
    var testSubmissionFailure = 0;
    var testSubmissionIntervalInSec = 60;
    var testSubmissionTimerId = 0;
    var loginValidateMsgTimer = 0;
    var questionStartTime = 0;
    var questionEndTime = 0;
    var fixedDivHeight = 320;
    var versionNo = 0;

    var ipAddress;
    var candidateName = "";
    var loginName = "";
    var testInstructionForHP = "";
    var presentationURL = "";
    var encString = "";

    var instantResult = false;
    var isStaticTest = false;
    var isSubmit = false
    var isPresentation = false;
    var optionRandomize = false;
    var IsLocalStorageAvailable = false;
    var isSectionWiseTiming = false;
    var cookiesModified = true;
    var enableLastSession = true;
    var IsOptionalGroup = false;
    var IsReactivated = false;
    var IsPsychometricTestType = false;
    var IsQuestionPaperPreviewEnabled = false;

    var lostSessionJsonDataObj = {};
    var lostSessionGroupObj = {};
    var QuestionTypeObj = {
        "Boolean": 1,
        "MCQ": 7,
        "Subjective": 8,
        "RTC": 10,
        "Psychometric": 15
    };
    var Ids = {
        "PagesDiv": {
            "mainLogin": "candidate_login_div",
            "candidateRegistration": "reg_span",
            "aboutUsId": "ctl00_DefaultContent_aboutCompanyInfo1",
            "testInstruction": "instructions_right_div",
            "testPage": "optionsDiv",
            "submitSuccessPage": "submission_right_div"
        },
        "Image": {
            "loadingImgSmall": "loadingicon",
            "submitCandidateLoading": "registerLoading",
            "loadTestPaper": "imgLoadTest",
            "instructionHelp": "toolTipIMg",
            "testNavigationBtnInstruction": "imgTestBtnInstruction",
            "testFlagBtnInstruction": "imgTestFlagBtnInstruction"
        },
        "Label": {
            "loginErrorMsg": "error_msg",
            "loginValidateMsg": "success_msg",
            "rightHeader": "headerTextRight",
            "leftHeader": "ctl00_DefaultContent_headerTextLeft",
            "leftContent": "ctl00_DefaultContent_contentTextLeft",
            "testName": "test_name",
            "candidateName": "candidate_name_span",
            "termsAndConditions": "terms_conditions",
            "sectionAndSubsectionName": "secSubSecDiv",
            "testTiming": "timerDiv",
            "flagButton": "FlagButtonDiv",
            "currentQuestionNo": "questionSpan",
            "totalTime": "spanDuration",
            "answerOptions": "optionsDiv",
            "sectionName": "lblSection",
            "subSectionName": "lblSubsection",
            "elapsedTime": "spanElapsed",
            "remainingTime": "spanRemaining"
        },
        "TextBox": {
            "username": "txtUserName",
            "password": "txtPassword",
            "department": "txtDepartment",
            "degree": "txtDegree",
            "college": "txtCollege",
            "candidateName": "txtCandidateName",
            "phoneNo": "txtPhoneNumber",
            "emailId": "txtEmailId",
            "percentage": "txtPercentage"
        },
        "Button": {
            "saveCandidate": "saveCandidate",
            "skipDemo": "skipDemo",
            "startTest": "startTest_btn",
            "flagUnflag": "flagUnflag",
            "previous": "btnBack",
            "next": "btnNext",
            "clearAnswer": "clearAnswer",
            "submitTest": "submit_btn",
            "login": "login_btn",
            "previewQuestions": "btnQuestionPreview",
            "viewResult": "btnViewResult"
        },
        "CheckBox": {
            "termsAndConditions": "terms_chk"
        },
        "Select": {
            "cgpaOrPercentage": "cgpaPer"
        }
    };

    /*Variables
    Ajax Helper*/
    var setServiceCallMethods = function () {
        var virtualPath = $('#virtualPath').val();
        versionNo = $('#versionNo').val();
        serviceCallMethodNames["LoginToTest"] = "/login"; //virtualPath + "JSONServices/JSONAssessmentManagementService.svc/LoginToTest";
        serviceCallMethodNames["GetQuestionsForOnlineAssessment"] = "/test" //virtualPath + "JSONServices/JSONAssessmentManagementService.svc/GetQuestionsForOnlineAssessment";
        serviceCallMethodNames["SubmitTestResult"] = "/submit"; //virtualPath + "JSONServices/JSONAssessmentManagementService.svc/SubmitTestResult";
        serviceCallMethodNames["GetTestResult"] = virtualPath + "JSONServices/JSONAssessmentManagementService.svc/GetTestResult";
        serviceCallMethodNames["SaveTestCandidate"] = virtualPath + "JSONServices/JSONCandidateManagementService.svc/SaveTestCandidate";
        serviceCallMethodNames["RemoveCache"] = virtualPath + "JSONServices/JSONHireProUtilityService.svc/RemoveCache";
        serviceCallMethodNames["GetHtmlOfControl"] = virtualPath + "JSONServices/JSONHireProUtilityService.svc/GetHtmlOfControl";
        serviceCallMethodNames["GetCandidateResults"] = virtualPath + "JSONServices/JSONAssessmentManagementService.svc/ViewCandidateScoreByCandidateId";
    };

    var callAjaxService = function (serviceCallMethodName, request, successHandler, errorHandler, dataType) {
        var returnDataType = "json";
        var htmlPath;
        if (dataType) {
            returnDataType = dataType;
        }
        if (serviceCallMethodName == "GetHtmlOfControl") {
            htmlPath = request.Path + "_" + +TenantInfo.tenantId + "_" + versionNo;
            var htmlStringResponse = readCookie(htmlPath);
            if (htmlStringResponse && htmlStringResponse.IsException == false && htmlStringResponse.IsFailure == false) {
                var response = { text: htmlStringResponse };
                successHandler(response);
            }
        }
        var ajaxValue
        if (serviceCallMethodName != "GetQuestionsForOnlineAssessment" && serviceCallMethodName != "LoginToTest") {
            ajaxValue = {
                "type": "POST",
                "url": serviceCallMethodNames[serviceCallMethodName],
                "data": $.stringify(request),
                "contentType": "application/json; charset=utf-8",
                "dataType": returnDataType,
                "success": function (response) {
                    if (serviceCallMethodName == "GetHtmlOfControl") {
                        var responseTemp;
                        if (response.text) {
                            responseTemp = response.text;
                        }
                        else {
                            responseTemp = response.childNodes[0].textContent;
                        }
                        createCookie(htmlPath, responseTemp, true);
                    }
                    successHandler(response);
                },
                "error": errorHandler,
                "complete": function (XMLHttpRequest, textStatus) {
                    if (textStatus == "error" && textStatus == "parseerror") { this; }
                }
            }
            if ($.ajax) {
                $.ajax(ajaxValue);
            } else {
                jQuery.ajax(ajaxValue);
            }
        }
        else {
            /*$.get(serviceCallMethodNames[serviceCallMethodName], function (data) {
                alert(data);
            });*/

            ajaxValue = {
                "type": "GET",
                "url": serviceCallMethodNames[serviceCallMethodName],
                //"data": $.stringify(request),
                "contentType": "application/json; charset=utf-8",
                "dataType": returnDataType,
                "success": function (response) {
                    if (serviceCallMethodName == "GetHtmlOfControl") {
                        var responseTemp;
                        if (response.text) {
                            responseTemp = response.text;
                        }
                        else {
                            responseTemp = response.childNodes[0].textContent;
                        }
                        createCookie(htmlPath, responseTemp, true);
                    }
                    successHandler(response);
                },
                "error": errorHandler,
                "complete": function (XMLHttpRequest, textStatus) {
                    if (textStatus == "error" && textStatus == "parseerror") { this; }
                }
            }
			if (serviceCallMethodName == "GetQuestionsForOnlineAssessment")
			{
				ajaxValue.url += "/" + request.TestId;
			}
			else if(serviceCallMethodName == "LoginToTest")
			{
				ajaxValue.url += "/" + request.LoginName + "/" + request.Password;
			}
			if ($.ajax) {
                $.ajax(ajaxValue);
            } else {
                jQuery.ajax(ajaxValue);
            }
        }
        
    };

    var reportAjaxFunction = function (url) {
        $.get(url, function (response) {
            if (response == "UpdateCandidate") {
                return;
            }
            else if (response == "LoginTest") {
                if (isPresentation == true) {
                    var idArray = ["ctl00_DefaultContent_aboutCompanyInfo1", "candidate_login_div"];
                }
            }
        });
    };
    /*Ajax Helper
    Methods Public*/
    this.Load = function () {
        ipAddress = $("#ctl00_DefaultContent_ip_address").val();

        getTenantInfo();
        masterPageSetting();
        htmlTest.setPageHeightAndWidth();
        setServiceCallMethods();
        createAllButtons();
        //LocalStorage
        try {
            if (localStorage)
                IsLocalStorageAvailable = true;
            else
                IsLocalStorageAvailable = false;
        }
        catch (ex) {
            IsLocalStorageAvailable = false;
        }
        if ($.getQueryString("a") != null) {
            $("#txtUserName").val(atob($.getQueryString("a")))
        }
        if ($.getQueryString("b") != null) {
            $("#txtPassword").val(atob($.getQueryString("b")))
        }
    };

    this.askConfirm = function () {
        addAnswerArrayToCookie();
        if (htmlTest.isLoginEnabled) {
            if (IsintermediateSaveEnabled)
                saveIntimidate();
            return "You have attempted to leave this page. By doing this you will get disqualified from the test. Are you sure you want to exit this page?";
        }
    };

    this.loginToTest = function () {
        loginName = jQuery.trim($('#' + Ids.TextBox.username).val());
        var Password = jQuery.trim($('#' + Ids.TextBox.password).val());
        if (loginName == "") {
            $('#' + Ids.Label.loginErrorMsg).html('Enter UserName');
        }
        else if (Password == "") {
            $('#' + Ids.Label.loginErrorMsg).html('Enter Password');
        }
        else {
            $('#' + Ids.Label.loginErrorMsg).html('');
            candidateId = 0;
            var os = getOS();
            var clientInfo = "Browser :" + getBrowser() + ", Version :" + $.browser.version + ", Color Depth:" + window.screen.colorDepth + "bit, Screen Resolution :" + window.screen.width + "x" + window.screen.height + ", OS :" + os[0] + ", Version :" + os[1] + ", IP Address :" + ipAddress;
            if (TenantInfo.tenantId) {
                var loginToTestRequest = {
                    "LoginName": loginName,
                    "Password": Password,
                    "TenantId": TenantInfo.tenantId,
                    "ClientSystemInfo": clientInfo.toString()
                };
                loginValidateCount = 0;
                showLoginValid();
                $('#' + Ids.Button.login).button({ disabled: true });
                callAjaxService("LoginToTest", loginToTestRequest, callBackLoginToTest, callBackLoginValidateFail);
            }
            else {
                jAlert("Invalid URL", 'Information');
            }
        }

        function callBackLoginToTest(response) {
			console.log("Login Success.");
            if (response.IsFailure == false && (!response.Message || response.Message == "")) {
                if (response.IsInstantResult != null)
                    instantResult = response.IsInstantResult;
                isStaticTest = response.IsStaticTest;
                $('#' + Ids.CheckBox.termsAndConditions).removeAttr('checked');
                isSubmit = false;
                candidateId = response.CandidateId;
                var Alogin = readCookie("ALoginId");
                if (Alogin != loginName) {
                    enableLastSession = false;
                    createCookie("CandidateAnswers", "");
                    createCookie("TimeRemaining", "");
                    createCookie("CandidateAnswersTimeStamp", "");
                    createCookie("TimeSpent", "");
                }
                createCookie("ALoginId", loginName);
                IsReactivated = response.ReactivatedCount > 0 ? true : false;
                if (response.CandidateName != null && response.CandidateName != "null" && response.CandidateName != "") {
                    candidateName = response.CandidateName;
                }
                if (response.TestInstructions != null && jQuery.trim(response.TestInstructions) != "") {
                    testInstructionForHP = response.TestInstructions;
                }
                if (response.TypeOfTestType && response.TypeOfTestType == 1) {
                    IsPsychometricTestType = true;
                }
                /*if (TenantInfo.alias.toLowerCase() == "hp") {
                    optionRandomize = true;
                }*/
                testId = response.TestId;
                totalTime = response.TestDuration;
                totalElapsedSeconds = response.TimeSpent != null ? response.TimeSpent : 0;
                presentationURL = response.PresentationURL;
                if (presentationURL != null && presentationURL != "")
                    isPresentation = true;
                /*if (!TenantInfo.alias.toLowerCase() == "amsdemo") {
                    $('#' + Ids.Label.testName).html(response.TestName);
                }
                else {
                    $('#' + Ids.Label.testName).css('display', 'none');
                }*/ 
                var dummyCandidate = false;
                if (response.AssessmentCatalogMaster != null) {
                    dummyCandidate = true;
                    toggleCandidateRegistration(response.AssessmentCatalogMaster);
                }
                $('#' + Ids.PagesDiv.mainLogin).remove();
                var QueryString = "Login=True&TestId=" + response.TestId + "&LoginName=" + loginName + "&TestDuration=" + response.TestDuration + "&TestName=" + response.TestName + "&CandidateName=" + candidateName + "&CandidateId=" + response.CandidateId;
                if (dummyCandidate == false) {
                    reportAjaxFunction('HTML_Test/TestSessionSet.aspx?function=LoginTest&' + QueryString);
                    if (isPresentation == true) {
                        $('#' + Ids.Label.leftHeader).html("Company Info");
                        $('#' + Ids.Label.rightHeader).html("Online Assessment");
                        $('#' + Ids.Button.skipDemo).css('display', '');
                        setFlashPresentation(Ids.Label.leftContent);
                        $('#' + Ids.PagesDiv.aboutUsId).css('display', '');
                    }
                    else {
                        toggleTestInstruction();
                    }
                }
                else {
                    reportAjaxFunction('HTML_Test/TestSessionSet.aspx?function=UpdateCandidate&' + QueryString);
                    $('#' + Ids.Label.leftHeader).html("Candidate Registration");
                    $('#' + Ids.Label.rightHeader).html("Information");
                    $('#' + Ids.Label.leftContent).html("Dear Candidate,<br><br>You are not yet registered with us.<br>Kindly fill in the adjacent Candidate Registration Form,<br>to help us capture your details for any further communication.");
                    $('#' + Ids.PagesDiv.candidateRegistration).css('display', '');
                    var request = { "Path": "~/HtmlTestControls/CandidateRegistration.ascx" };
                    callAjaxService("GetHtmlOfControl", request, callBackGetCandidateRegistrationPageHtmlSuccess, callBackGetCandidateRegistrationPageHtmlFail, "xml");
                }
                htmlTest.isLoginEnabled = true;
                if (IsReactivated == true && IsintermediateSaveEnabled) {
                    var jsonGetResultRequest = {
                        "TestId": testId,
                        "CandidateId": candidateId,
                        "TenantId": TenantInfo.tenantId,
                        "UserId": "0"
                    };
                    callAjaxService("GetTestResult", jsonGetResultRequest, callBackGetResult, callBackGetResultFail);
                }
            } else {
                loginValidateCount = 0;
                $('#' + Ids.Image.loadingImgSmall + ', #' + Ids.Label.loginValidateMsg).css('display', 'none');
                if (loginValidateMsgTimer) {
                    clearTimeout(loginValidateMsgTimer);
                    loginValidateMsgTimer = 0;
                }
                $('#' + Ids.TextBox.username + ', #' + Ids.TextBox.password).val('');
                $('#' + Ids.Label.loginErrorMsg).html(response.Message);
                $('#' + Ids.Button.login).button({ disabled: false });
            }
        }

        function callBackLoginValidateFail(response) {
            loginValidateCount = 0;
            $('#' + Ids.Image.loadingImgSmall + ', #' + Ids.Label.loginValidateMsg).css('display', 'none');
            if (loginValidateMsgTimer) {
                clearTimeout(loginValidateMsgTimer);
                loginValidateMsgTimer = 0;
            }
            $('#' + Ids.Label.loginErrorMsg).html('<p>Unable to connect server<br /> Check your Internet connection.</p>');
            $('#' + Ids.Button.login).button({ disabled: false });
        }

        function callBackGetResult(response) {
            if (response != null && response.Answers != null && response.Answers.length > 0) {
                if (response.TimeStamp != null && response.TimeStamp != "" && readCookie("CandidateAnswersTimeStamp") && readCookie("CandidateAnswersTimeStamp") != null && readCookie("CandidateAnswersTimeStamp") != "") {
                    var lostSessionTimeStamp = new Date(response.TimeStamp);

                    var candidateAnswersCookieTimeStamp = new Date(readCookie("CandidateAnswersTimeStamp"));
                    var datediff = DateDiff(lostSessionTimeStamp, candidateAnswersCookieTimeStamp)
                    if (datediff < 0) {
                        lostSessionJsonDataObj = response.Answers;
                        lostSessionGroupObj = response.TestUserGroupInfoCollection;
                        createCookie('CandidateAnswers', $.stringify(lostSessionJsonDataObj));
                    } else {
                        callBackGetResultFail();
                    }
                } else {
                    if (response.TimeStamp != null && response.TimeStamp != "") {
                        lostSessionJsonDataObj = response.Answers;
                        lostSessionGroupObj = response.TestUserGroupInfoCollection;
                    } else {
                        callBackGetResultFail();
                    }
                }
            }
            else {
                callBackGetResultFail();
            }
        }

        function callBackGetResultFail(response) {
            if ($.isEmptyObject(lostSessionJsonDataObj) || cookiesModified == true) {
                var canidateAnswers = readCookie("CandidateAnswers");
                if (canidateAnswers != null && canidateAnswers != "") {
                    lostSessionJsonDataObj = $.parseJSON(canidateAnswers);
                    for (var i = 0; i < lostSessionJsonDataObj.length; i++) {
                        if (lostSessionJsonDataObj[i].A != null && lostSessionJsonDataObj[i].A != undefined && lostSessionJsonDataObj[i].TimeSpent > 0)
                            intermediateAnswerCollection[lostSessionJsonDataObj[i].Q] = lostSessionJsonDataObj[i];
                    }
                }
                cookiesModified = false;
            }
        }

        function callBackGetCandidateRegistrationPageHtmlSuccess(response) {
            if (response && (response.IsException == true || response.IsFailure == true)) {
                ErrorMessageHandler(response.Message);
            }
            else {
                if (response.text) {
                    $('#' + Ids.PagesDiv.candidateRegistration).html(response.text);
                }
                else {
                    $('#' + Ids.PagesDiv.candidateRegistration).html(response.childNodes[0].textContent);
                }
                $('#' + Ids.Button.saveCandidate).button();
            }
        }

        function callBackGetCandidateRegistrationPageHtmlFail(response) {
            console.log(response);
        }
    };

    this.setPageHeightAndWidth = function () {
        var idArray = ["ctl00_DefaultContent_contentTextLeft", "candidate_login_div", "reg_span", "ctl00_DefaultContent_aboutCompanyInfo1", "instructions_right_div", "optionsDiv", "submission_right_div"];
        setPageHeight(idArray, 150, fixedDivHeight);
    };

    this.checkCgpaPercentage = function (value) {
        if (value == "percentage") {
            validateInput('Number', '#txtPercentage', '#percentage_err', true, null, null, 35, 100, '#cgpaPer');
        }
        else {
            validateInput('Number', '#txtPercentage', '#percentage_err', true, null, null, 1, 10, '#cgpaPer');
        }
    };

    this.showInstructions = function () {
        $('#' + Ids.PagesDiv.aboutUsId).css('display', 'none');
        toggleTestInstruction();
    };

    this.changeStartBtn = function (obj) {
        if (obj.checked == true) {
            $('#' + Ids.Button.startTest).button({ disabled: false });
        }
        else {
            $('#' + Ids.Button.startTest).button({ disabled: true });
        }
    };
    this.updateTestCandidate = function () {
        validateUpdateTestCandidate();
        if (IsValidate == true) {
            candidateName = $('#' + Ids.TextBox.candidateName).val();
            var candidatePhone = $('#' + Ids.TextBox.phoneNo).val();
            if (candidatePhone == null || candidatePhone == 0) {
                candidatePhone = "";
            }
            var candidateEmail = $('#' + Ids.TextBox.emailId).val(); ;
            var collegeId = catalogLookUp($('#' + Ids.TextBox.college).val(), "Colleges");
            var degreeId = catalogLookUp($('#' + Ids.TextBox.degree).val(), "Degrees");
            var departmentId = catalogLookUp($('#' + Ids.TextBox.department).val(), "Departments");
            var ispercentage = false;
            if ($('#' + Ids.Select.cgpaOrPercentage).find('option:selected').attr('value') == "percentage")
                ispercentage = true;
            var cgpa = $('#' + Ids.TextBox.percentage).val();
            if (cgpa == null || cgpa == "") {
                cgpa = 0.0;
            }
            var requestObj = {
                "Id": candidateId,
                "CandidateName": candidateName,
                "Mobile": candidatePhone,
                "CandidateEmail": candidateEmail,
                "CollegeId": collegeId,
                "DegreeId": degreeId,
                "BranchId": departmentId,
                "IsPercentage": ispercentage,
                "Percentage": cgpa,
                "Cgp": cgpa,
                "TenantId": TenantInfo.tenantId,
                "UserId": 0,
                "IsRegistered": true,
                "TestId": testId
            };
            $('#' + Ids.Image.submitCandidateLoading).attr('src', 'App_Themes/HiRePro/theme1/images/ajax-loader1.gif').css('display', '');
            $('#' + Ids.Button.saveCandidate).button({ disabled: true });
            callAjaxService("SaveTestCandidate", requestObj, callBackupdateTestCandidate, generalCallBackFail);
        }

        function callBackupdateTestCandidate(response) {
            if (response != null && (response.IsException == true || response.IsFailure == true)) {
                jAlert(response.Message);
                $('#' + Ids.Button.saveCandidate).button({ disabled: false });
                $('#' + Ids.Image.submitCandidateLoading).css('display', 'none');
            }
            else {
                var QueryString = "CandidateName=" + candidateName;
                reportAjaxFunction('HTML_Test/TestSessionSet.aspx?function=LoginTest&' + QueryString);
                $('#' + Ids.PagesDiv.candidateRegistration).css('display', 'none');
                resetFields();
                if (isPresentation == true)
                    $('#' + Ids.PagesDiv.aboutUsId).css('display', '');
                else
                    toggleTestInstruction();
            }
        }
    };

    this.startTest = function () {
        var isChecked = $('#' + Ids.CheckBox.termsAndConditions).is(':checked');
        if (isChecked == true) {
            $('#' + Ids.Button.startTest).button({ disabled: true });
            $('#' + Ids.Image.loadTestPaper).attr('src', 'App_Themes/HiRePro/theme1/images/ajax-start-loader.gif').css('display', '');
            $('#' + Ids.Label.termsAndConditions).css('display', 'none');
            var jsonQuestionRequest = {
                "TestId": testId,
                "IsStaticTest": isStaticTest,
                "CandidateId": candidateId,
                "TenantId": TenantInfo.tenantId,
                "UserId": "0"
            };
            disableSelection($('#' + Ids.Label.leftContent));
            callAjaxService("GetQuestionsForOnlineAssessment", jsonQuestionRequest, callBackStartTest, callBackStartTestFail);
        }

        function callBackStartTestFail(response) {
            var message = "";
            if (response != null && response.Message != null)
                message = response.Message;
            else
                message = "Unable to Connect Server, Check your Network Connections<br />";
            jAlert(message + " Click Ok to retry Again", "Network ERROR", function (r) { htmlTest.startTest(); });
        }

        function callBackStartTest(response) {
            if (response != null && (response.IsException == true || response.IsFailure == true)) {
                jAlert(response.Message + ", Click OK to retry Again", "Network ERROR", function (r) { htmlTest.startTest(); });
            }
            else if (response != null) {
                $('#' + Ids.Label.candidateName).html(candidateName);
                if (response.GroupWiseTiming == "undefined" || response.GroupWiseTiming == undefined) {
                    isSectionWiseTiming = false;
                }
                else {
                    isSectionWiseTiming = response.GroupWiseTiming;
                }
                if (response.IsOptionalGroup == "undefined" || response.IsOptionalGroup == undefined) {
                    IsOptionalGroup = false;
                }
                else {
                    IsOptionalGroup = response.IsOptionalGroup;
                }
                if (response.IsOptionRandomize == "undefined" || response.IsOptionRandomize == undefined) {
                    optionRandomize = false;
                }
                else {
                    optionRandomize = response.IsOptionRandomize;
                }
                toggleOnlineTest();
                var totalSectionIndex = 0;
                var questionRange = 0;
                if (response.JsonGroupWithQuestioncollectionForMandatorySection != null) {
                    mandatoryCollection = response.JsonGroupWithQuestioncollectionForMandatorySection;
                    for (var count = 0; count < response.JsonGroupWithQuestioncollectionForMandatorySection.length; count++) {
                        var groupName = response.JsonGroupWithQuestioncollectionForMandatorySection[count].Name;
                        var qIdArray = [];
                        var sectionQuestionCount = 0;
                        var sectionCollection = response.JsonGroupWithQuestioncollectionForMandatorySection[count].SectionTypeCollection;
                        for (var i = 0; i < sectionCollection.length; i++) {
                            sectionName = sectionCollection[i].Name;
                            instructionString = sectionCollection[i].Instruction;
                            sectionQuestionCount += sectionCollection[i].QuestionType.length;
                            for (var j = 0; j < sectionCollection[i].QuestionType.length; j++) {
                                var TypeOfQuestion = sectionCollection[i].QuestionType[j].TypeOfQuestion;
                                if (TypeOfQuestion == QuestionTypeObj.RTC) {
                                    sectionQuestionCount--;
                                    questionString = "<b>Passage:</b><br/>" + sectionCollection[i].QuestionType[j].HTMLString + "<br/><b>Question:</b><br/>";
                                    var CQGTC = sectionCollection[i].QuestionType[j].ChildQuestionGeneralTypeCollection;
                                    for (var k = 0; k < CQGTC.length; k++) {
                                        sectionQuestionCount++;
                                        queId = CQGTC[k].QuestionId;
                                        var queStr = questionString + CQGTC[k].HTMLString;
                                        answerCollection = CQGTC[k].AnswerChoiceCollection;
                                        TypeOfQuestion = CQGTC[k].TypeOfQuestion;
                                        //QuestionType = CQGTC;
                                        var answerTimeObj = getObjFromCookie(queId);
                                        var sectionId = sectionCollection[i].SectionId;
                                        var questionTenantId = CQGTC[k].QuestionTenantId;
                                        createResponseObject(groupName, sectionName, queId, queStr, instructionString, answerCollection, TypeOfQuestion, answerTimeObj.answer);
                                        qIdArray.push(queId);
                                        var answerObject = {
                                            "Q": queId,
                                            "A": answerTimeObj.answer,
                                            "TimeSpent": (answerTimeObj.timeSpend == null) ? 0 : answerTimeObj.timeSpend,
                                            "SecId": sectionId
                                        }
                                        if (TenantInfo.tenantId != questionTenantId)
                                            answerObject.QTenantId = questionTenantId;
                                        answerArray.push(answerObject);
                                    }
                                }
                                else if (TypeOfQuestion == QuestionTypeObj.Boolean || TypeOfQuestion == QuestionTypeObj.MCQ || TypeOfQuestion == QuestionTypeObj.Subjective || QuestionTypeObj.Psychometric) {
                                    queId = sectionCollection[i].QuestionType[j].QuestionId;
                                    questionString = sectionCollection[i].QuestionType[j].HTMLString;
                                    answerCollection = sectionCollection[i].QuestionType[j].AnswerChoiceCollection;
                                    //QuestionType = sectionCollection[i].QuestionType[j];
                                    var answerTimeObj = getObjFromCookie(queId);
                                    var sectionId = sectionCollection[i].SectionId;
                                    var questionTenantId = sectionCollection[i].QuestionType[j].QuestionTenantId;
                                    createResponseObject(groupName, sectionName, queId, questionString, instructionString, answerCollection, TypeOfQuestion, answerTimeObj.answer);
                                    qIdArray.push(queId);
                                    var answerObject = {
                                        "Q": queId,
                                        "A": answerTimeObj.answer,
                                        "TimeSpent": (answerTimeObj.timeSpend == null) ? 0 : answerTimeObj.timeSpend,
                                        "SecId": sectionId
                                    }
                                    if (TypeOfQuestion == QuestionTypeObj.Subjective)
                                        answerObject.IsSubjective = true;
                                    if (TenantInfo.tenantId != questionTenantId)
                                        answerObject.QTenantId = questionTenantId;
                                    answerArray.push(answerObject);
                                }
                            }
                        }
                        if (isSectionWiseTiming == true) {
                            var sectionWiseTimingObj = {
                                "SectionId": response.JsonGroupWithQuestioncollectionForMandatorySection[count].Id,
                                "SectionName": sectionName,
                                "QuestionCount": sectionQuestionCount,
                                "SectionTotalTime": response.JsonGroupWithQuestioncollectionForMandatorySection[count].TotalTime
                            };
                            sectionWiseTiming.push(sectionWiseTimingObj);
                        }
                        if (!IsPsychometricTestType) {
                            setSectionInfo(groupName, sectionQuestionCount, questionRange, totalSectionIndex, null);
                            questionRange = questionRange + sectionQuestionCount;
                            totalSectionIndex++;
                        }
                    }
                }
                var optionalGroupName = "";
                nonMandatoryCollection = response.JsonGroupWithQuestioncollectionForNonMandatorySection;
                for (var i = 0; i < nonMandatoryCollection.length; i++) {
                    sectionQuestionCount = 0;
                    if (nonMandatoryCollection[i].Name)
                        optionalGroupName = nonMandatoryCollection[i].Name;
                    else
                        optionalGroupName = "Optional Group";
                    var groupName = "";
                    for (var j = 0; j < nonMandatoryCollection[i].JsonGroupcollection.length; j++) {
                        if (j != 0)
                            groupName += " or ";
                        groupName += nonMandatoryCollection[i].JsonGroupcollection[j].Name;
                    }
                    for (var l = 0; l < nonMandatoryCollection[i].JsonGroupcollection[0].SectionTypeCollection.length; l++) {
                        var TypeOfQuestion = nonMandatoryCollection[i].JsonGroupcollection[0].SectionTypeCollection[l].QuestionType[0].TypeOfQuestion;
                        if (TypeOfQuestion == QuestionTypeObj.RTC) {
                            for (var k = 0; k < nonMandatoryCollection[i].JsonGroupcollection[0].SectionTypeCollection[0].QuestionType.length; k++)
                                sectionQuestionCount += nonMandatoryCollection[i].JsonGroupcollection[0].SectionTypeCollection[l].QuestionType[k].ChildQuestionGeneralTypeCollection.length;
                        }
                        else {
                            sectionQuestionCount += nonMandatoryCollection[i].JsonGroupcollection[0].SectionTypeCollection[l].QuestionType.length;
                        }
                    }
                    if (!IsPsychometricTestType) {
                        setSectionInfo(groupName, sectionQuestionCount, questionRange, totalSectionIndex, optionalGroupName);
                        questionRange = questionRange + sectionQuestionCount;
                        totalSectionIndex++;
                    }
                }
                if (isSectionWiseTiming == true)
                    totalTime = sectionWiseTiming[0].SectionTotalTime;
                var totalQuestionToView = (isSectionWiseTiming == true) ? sectionWiseTiming[sectionIndex].QuestionCount - 1 : responseArray.length - 1;
                if ((IsOptionalGroup && questionIndexer == totalQuestionToView && optionalSectionIndex == nonMandatoryCollection.length) || (!IsOptionalGroup && isSectionWiseTiming == true && questionIndexer == totalQuestionToView && sectionIndex == sectionWiseTiming.length - 1) || (!IsOptionalGroup && isSectionWiseTiming == false && questionIndexer == totalQuestionToView)) {
                    $('#' + Ids.Button.next).button({ disabled: true });
                    $('#' + Ids.Button.submitTest).button({ disabled: false });
                }
                createFlagButtons();
                viewTestQuestions(responseArray[0]);
                timerStart();
            }
            else {
                callBackStartTestFail(response, instructionsDivId, tenantInputId);
            }
        }
    };

    this.FlagQuestion = function () {
        var obj = $('#flag_' + currentQuestionNo);
        if (obj.attr('class') == "button_two") {
            if (responseArray[currentQuestionNo].isQuestionAnswered == true)
                obj.attr('class', 'button_one');
            else
                obj.attr('class', 'button_three');
        }
        else {
            obj.attr('class', 'button_two');
        }
    };

    this.viewNextQuestion = function () {
        var totalQuestionToView = (isSectionWiseTiming == true) ? sectionWiseTiming[sectionIndex].QuestionCount - 1 : responseArray.length - 1;

        if (questionIndexer < totalQuestionToView) {
            currentQuestionNo++;
            questionIndexer++;
            viewTestQuestions(responseArray[currentQuestionNo], currentQuestionNo - 1);
            if ((IsOptionalGroup && questionIndexer == totalQuestionToView && optionalSectionIndex == nonMandatoryCollection.length) || (!IsOptionalGroup && isSectionWiseTiming == true && questionIndexer == totalQuestionToView && sectionIndex == sectionWiseTiming.length - 1) || (!IsOptionalGroup && isSectionWiseTiming == false && questionIndexer == totalQuestionToView)) {
                $('#' + Ids.Button.next).button({ disabled: true });
                $('#' + Ids.Button.submitTest).button({ disabled: false });
            }
        }
        else if (isSectionWiseTiming == true && questionIndexer == totalQuestionToView && sectionIndex < sectionWiseTiming.length - 1) {
            jConfirm('Are you sure you want to move to the next section?  You will not be able to revisit this section once you start the next section.<br /> Press OK to continue or Cancel to stay on the current page.', 'Information', function (r) {
                if (r) {
                    //if (IsintermediateSaveEnabled)
                    //  saveIntimidate();
                    sectionWiseTimerStop();
                }
            });
        }
        else if (IsOptionalGroup && questionIndexer == totalQuestionToView && optionalSectionIndex < nonMandatoryCollection.length) {
            //if (IsintermediateSaveEnabled)
            //saveIntimidate();
            OptionalGroups(nonMandatoryCollection[optionalSectionIndex].JsonGroupcollection.length);
        }

        if (questionIndexer == 1)
            $('#' + Ids.Button.previous).button({ disabled: false });
    };

    this.viewPreviousQuestion = function () {
        if (questionIndexer > 0) {
            currentQuestionNo--;
            questionIndexer--;
            viewTestQuestions(responseArray[currentQuestionNo], currentQuestionNo + 1);
            if (questionIndexer == 0)
                $('#' + Ids.Button.previous).button({ disabled: true });
            if (questionIndexer <= responseArray.length - 2)
                $('#' + Ids.Button.next).button({ disabled: false });
        }
    };

    this.optionClick = function () {
        HandlingAnswer();
        updateFlagBtn();
    };

    this.clearAnswers = function () {
        if (typeOfQuestion == QuestionTypeObj.Boolean || typeOfQuestion == QuestionTypeObj.MCQ || QuestionTypeObj.Psychometric)
            $('input[name="question"]').removeAttr('checked');
        if (typeOfQuestion == QuestionTypeObj.Subjective)
            $('#' + responseArray[currentQuestionNo].questionId).val('');
        answerArray[currentQuestionNo].A = null;
        addAnswerArrayToCookie();
        responseArray[currentQuestionNo].isQuestionAnswered = false;
        updateFlagBtn();
    };

    this.FlagClick = function (questionNo) {
        var totalQuestionToView = (isSectionWiseTiming == true) ? sectionWiseTiming[sectionIndex].QuestionCount - 1 : responseArray.length - 1;

        if (questionNo <= responseArray.length - 1 && questionNo >= 0) {
            var questionToSave = currentQuestionNo;
            var diff = questionNo - currentQuestionNo;
            questionIndexer += diff;
            currentQuestionNo = questionNo;
            viewTestQuestions(responseArray[currentQuestionNo], questionToSave);
            if (questionIndexer == 0)
                $('#' + Ids.Button.previous).button({ disabled: true });
            if (questionIndexer <= totalQuestionToView - 1)
                $('#' + Ids.Button.next).button({ disabled: false });
            if ((IsOptionalGroup && questionIndexer == totalQuestionToView && optionalSectionIndex == nonMandatoryCollection.length) || (!IsOptionalGroup && isSectionWiseTiming == true && questionIndexer == totalQuestionToView && sectionIndex == sectionWiseTiming.length - 1) || (!IsOptionalGroup && isSectionWiseTiming == false && questionIndexer == totalQuestionToView)) {
                $('#' + Ids.Button.next).button({ disabled: true });
                $('#' + Ids.Button.submitTest).button({ disabled: false });
            }
            if (questionIndexer >= 1)
                $('#' + Ids.Button.previous).button({ disabled: false });
        }
    };

    this.showTermsConditions = function (id) {
        jQuery('#' + id).dialog({
            height: 500,
            width: 650,
            modal: true
        });
    };

    this.SubmitResultOnclick = function () {
        jConfirm('Are you sure you want to submit. You will not be able to make any changes once you submit the test.<br /> Press OK to continue or Cancel to stay on the current page.', 'Test Submission', function (r) {
            if (r) timerStop(1);
        });
    };

    this.showCandidateAnswer = function () {
        $('#masterBody').attr('oncopy', 'return true;');
        jAlert("<textarea id=\"candidate_ans_txtarea\" onClick=\"$(this).focus();\" rows=\"4\" cols=\"30\" readonly=\"readonly\">" + encString + "</textarea><br />", "Candidate Answers", function (r) { SubmitResult(); });
    };

    this.flushStaticTestCache = function () {
        callAjaxService("RemoveCache", { FlushAll: true })
    };

    /*Methods Public
    Methods Private*/
    var masterPageSetting = function () {
        document.aspnetForm.reset();
        testSubmissionFailure = 0;

        //        $(document).keypress(function (event) {
        //            alert(event.keyCode);
        //            if ('116' == event.keyCode) {
        //                jAlert("Refreshing the page is not allowed", "Information");
        //                return false;
        //            }
        //        });
        $(window).resize(function (arg) { htmlTest.setPageHeightAndWidth(); });
        
        $('#masterBody').attr({ 'oncopy': 'return false;', 'oncut': 'return false;' });
        $('#txtPassword').keypress(function (event) {
            if (event.keyCode == '13') {
                htmlTest.loginToTest();
            }
        });
    };

    var setPageHeight = function (idArray, minHeight, fixedSubHeight) {
        var pageHeight = $(window).height() - fixedSubHeight;
        var height = (pageHeight < minHeight) ? minHeight : pageHeight;
        if (idArray != null && idArray.length != 0) {
            for (var i = 0; i < idArray.length; i++) {
                $('#' + idArray[i]).css({ 'overflow': 'auto', 'height': height + 'px' });
            }
        }
        $('textarea').css({ 'height': '95%', 'width': '96%' });
    };

    var createAllButtons = function () {
        if (IsQuestionPaperPreviewEnabled)
            $('#' + Ids.Button.previewQuestions).css('display', '').click(function () { preview(true); }).button();
        if (instantResult)
            $('#' + Ids.Button.viewResult).css('display', '').click(function () { getCandidateResult(); }).button();
        $('#' + Ids.Button.skipDemo + ', #' + Ids.Button.startTest).button();
        $('#' + Ids.Button.startTest).button({ disabled: true });
    };

    var toggleCandidateRegistration = function (CatalogMaster) {
        if (TenantInfo.alias.toLowerCase() == "hp") {
            $("#txtPhoneNumber").hide();
            $("#phonenumberlabel").hide();
            $("#txtCollege").hide();
            $("#collegelabel").hide();
            $("#txtDegree").hide();
            $("#degreelabel").hide();
            $("#txtDepartment").hide();
            $("#departmentlabel").hide();
            $("#txtPercentage").hide();
            $("#percentagelabel").hide();
            $("#cgpaPer").hide();
        }
        else {
            bindCatalogValues(CatalogMaster);
            $('#' + Ids.TextBox.college).autocomplete({ source: collegeArray });
            $('#' + Ids.TextBox.degree).autocomplete({ source: degreeArray });
            $('#' + Ids.TextBox.department).autocomplete({ source: departmentArray });
        }
    };

    var toggleTestInstruction = function () {
        $('#' + Ids.Button.skipDemo).css('display', 'none');
        $('#' + Ids.Button.startTest).css('display','').button({ disabled: true });
        $('#' + Ids.PagesDiv.testInstruction).css('display', '');
        $('#' + Ids.Label.leftHeader).html("Welcome " + candidateName);
        $('#' + Ids.Label.rightHeader).html("Instruction");
        $('#' + Ids.Label.leftContent).html(testInstructionForHP);
        $('#' + Ids.Label.termsAndConditions).css('display', '');
        $('#' + Ids.Image.testNavigationBtnInstruction).attr('src', 'App_Themes/HiRePro/theme1/images/buttonimg.jpg');
        $('#' + Ids.Image.testFlagBtnInstruction).attr('src', 'App_Themes/HiRePro/theme1/images/flagimg.jpg');
    };

    var toggleOnlineTest = function () {
        $('#' + Ids.PagesDiv.testInstruction).css('display', 'none');
        $('#' + Ids.Image.loadTestPaper).css('display', 'none');
        $('#' + Ids.Button.startTest).css('display', 'none');
        fixedDivHeight = 374;
        htmlTest.setPageHeightAndWidth();
        $('#' + Ids.Label.leftHeader).html("Question No :").append($('<span />').attr('id', 'questionSpan'));
        $('#' + Ids.Label.rightHeader).html("Answer");
        $('#' + Ids.PagesDiv.testPage).css('display', '');
        $('#' + Ids.Button.flagUnflag).css('display', '').button({ icons: { primary: "ui-icon-flag"} });
        $('#' + Ids.Button.previous).css('display', '').button({ disabled: true, icons: { primary: "ui-icon-seek-prev"} });
        $('#' + Ids.Button.next).css('display', '').button({ icons: { secondary: "ui-icon-seek-next"} });
        $('#' + Ids.Button.clearAnswer).css('display', '').button();
        $('#' + Ids.Button.submitTest).css('display', '').button({ disabled: true });
        $('#' + Ids.Image.instructionHelp).attr('src', 'App_Themes/HiRePro/theme1/images/help.gif');

        if (!IsPsychometricTestType) {
            var sectionDiv = $('#' + Ids.Label.sectionAndSubsectionName).html('');
            var sectionSpan = $('<span />').addClass('font_a').html('Group: ').appendTo(sectionDiv);
            var lblSection = $('<span />').html('&nbsp;').attr('id', 'lblSection').css('color', 'black').appendTo(sectionDiv);
            var subSectionSpan = $('<span />').addClass('font_a').html(' | Section: ').appendTo(sectionDiv);
            var lblSubsection = $('<span />').attr('id', 'lblSubsection').html('&nbsp').css('color', 'black').appendTo(sectionDiv);

            var timerString = " [Total Time: <span id=\"spanDuration\">" + $("#test_duration").val() + "</span>  |  Time Elapsed: <span id=\"spanElapsed\"></span>  |  Time Remaining: <span id=\"spanRemaining\"></span>]";
            $('#' + Ids.Label.testTiming).html('&nbsp;&nbsp;').append($('<img />').attr('src', 'App_Themes/HiRePro/theme1/images/Timmer.png').css('vertical-align', 'middle')).append(timerString);
        }
        else {
            $('#label_strik').css('height', '108px');
        }
    };

    var showLoginValid = function () {
        var TimeinSeconds = (120 / 4) * 1000; // (expectedTime/noOfParts) * secs
        var validationmessage = ["Validating", "Authenticating", "Logging In"];
        $('#' + Ids.Label.loginValidateMsg).html(validationmessage[loginValidateCount]).css('display', '');
        if (loginValidateCount == 0) {
            $('#' + Ids.Image.loadingImgSmall).attr('src', 'App_Themes/HiRePro/theme1/images/ajax-loader1.gif').css('display', '');
            candidateId
        }
        if (loginValidateCount < 2)
            loginValidateMsgTimer = setTimeout(showLoginValid, TimeinSeconds);
        loginValidateCount++;
    };

    var createCookie = function (name, value, isHtmlControl) {
        //        if (days != -1)
        //            $.cookie(name, value, { expires: days });
        //        else
        //            $.cookie(name, null);
        if (!isHtmlControl) {
            var date = new Date();
            date.setTime(date.getTime() + (6 * 60 * 60 * 1000));
            $.cookie(name, value, { expires: date });
        }
        if (IsLocalStorageAvailable && localStorage) {
            try {
                localStorage.setItem(name, value);
                localStorage.setItem("__CallTimeIn__" + name, Date());
            }
            catch (ex) {
                if (ex.code == 22) {
                    if (localStorage.clear) {
                        localStorage.clear();
                    }
                    else {
                        for (var i = 0; i < localStorage.length; i++) {
                            localStorage.removeItem(localStorage.name(i));
                        }
                    }
                }
            }
        }
    };

    var readCookie = function (name) {
        var item = $.cookie(name);
        if (IsLocalStorageAvailable && localStorage) {
            try {
                var itemTemp = localStorage.getItem(name);
                if (itemTemp != null && itemTemp != undefined) {
                    item = itemTemp;
                }
            }
            catch (ex) {
                item = $.cookie(name);
            }
        }
        return item;
    };

    var setFlashPresentation = function (divId) {
        var data = "<object width=\"100%\" height=\"100%\" codebase=\"http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab\">";
        data += "<param name=\"movie\" value=\"" + presentationURL + "\" />";
        data += "<param name=\"quality\" value=\"high\" />";
        data += "<param name=\"bgcolor\" value=\"#ffffff\" />";
        data += "<param name=\"allowScriptAccess\" value=\"sameDomain\" />";
        data += "<embed src=\"" + presentationURL + "\" quality=\"high\" bgcolor=\"#ffffff\" ";
        data += "width=\"500\" height=\"400\" align=\"middle\" play=\"true\" loop=\"false\" allowscriptaccess=\"sameDomain\" ";
        data += "type=\"application/x-shockwave-flash\" pluginspage=\"http://www.adobe.com/go/getflashplayer\">";
        data += "</embed></object>";
        $('#' + divId).html(data);
    };

    var bindCatalogValues = function (catalogCollection) {
        if (catalogCollection != null && catalogCollection.length != 0) {
            for (var i = 0; i < catalogCollection.length; i++) {
                switch (catalogCollection[i].Name) {
                    case "Colleges": addCatalogToArray(catalogCollection[i].JsonAssessmentCatalogValueList, collegeArray, collgeObjArray);
                        break;
                    case "Degrees": addCatalogToArray(catalogCollection[i].JsonAssessmentCatalogValueList, degreeArray, degreeObjArray);
                        break;
                    case "Departments": addCatalogToArray(catalogCollection[i].JsonAssessmentCatalogValueList, departmentArray, departmentObjArray);
                        break;
                }
            }
        }
    };

    var addCatalogToArray = function (catalogValues, array, arrayObject) {
        if (catalogValues != null && catalogValues.length > 0) {
            for (var i = 0; i < catalogValues.length; i++) {
                var jsonObj = { "Id": catalogValues[i].CatalogValueId, "Name": jQuery.trim(catalogValues[i].Name.replace(/\,/g, ' -')) }
                arrayObject.push(jsonObj);
                array.push(jQuery.trim(catalogValues[i].Name.replace(/\,/g, ' -')));
            }
        }
    };

    var catalogLookUp = function (catalog, type) {
        var catalogId = 0;
        switch (type) {
            case "Colleges": catalogId = getCatalogId(catalog, collgeObjArray);
                break;
            case "Degrees": catalogId = getCatalogId(catalog, degreeObjArray);
                break;
            case "Departments": catalogId = getCatalogId(catalog, departmentObjArray);
                break;
        }
        return catalogId;
    };

    var getCatalogId = function (catalog, ObjectArray) {
        var id = 0;
        for (var i = 0; i < ObjectArray.length; i++) {
            if ((ObjectArray[i].Name).toLowerCase() == (jQuery.trim(catalog)).toLowerCase()) {
                id = ObjectArray[i].Id;
                break;
            }
        }
        return id;
    };

    var validateUpdateTestCandidate = function () {
        IsValidate = true;
        if (TenantInfo.alias.toLowerCase() != "hp") {
            validateInput('String', '#txtCandidateName', '#cand_name_err', true);
            validateInput('Number', '#txtPhoneNumber', '#phone_err', true);
            validateInput('Email', '#txtEmailId', '#email_err', true);
            validateInput('AlphaNumerical', '#txtCollege', '#college_err', true);
            validateInput('AlphaNumerical', '#txtDegree', '#degree_err', true);
            validateInput('AlphaNumerical', '#txtDepartment', '#department_err', true);
            validateInput('PercentageCGPA', '#txtPercentage', '#percentage_err', true, null, null, 35, 100, '#cgpaPer')
        }
        else {
            validateInput('String', '#txtCandidateName', '#cand_name_err', true);
            validateInput('Email', '#txtEmailId', '#email_err', true);
        }

    }

    var resetFields = function () {
        $('#' + Ids.TextBox.candidateName + ', #' + Ids.TextBox.phoneNo + ', #' + Ids.TextBox.emailId + ', #' + Ids.TextBox.college + ', #' + Ids.TextBox.degree + ', #' + Ids.TextBox.department + ', #' + Ids.TextBox.percentage).val('');
    };


    var generalCallBackFail = function (response) {
        jAlert(response.Message);
    };

    var disableSelection = function (target) {
        if ($.browser.mozilla) {//Firefox
            $(target).css('MozUserSelect', 'none');
        } else if ($.browser.msie) {//IE
            $(target).bind('selectstart', function () { return false; });
        } else {//Opera, etc.
            $(target).mousedown(function () { return false; });
        }
    };

    var getObjFromCookie = function (questionId) {
        var obj = { answer: null, timeSpend: 0 };
        if (enableLastSession == true || IsReactivated == true) {
            if ($.isEmptyObject(lostSessionJsonDataObj) || cookiesModified == true) {
                var canidateAnswers = readCookie("CandidateAnswers");
                if (canidateAnswers != null && canidateAnswers != "")
                    lostSessionJsonDataObj = $.parseJSON(canidateAnswers);
                cookiesModified = false;
            }
            if (!$.isEmptyObject(lostSessionJsonDataObj)) {
                for (var i = 0; i < lostSessionJsonDataObj.length; i++) {
                    if (lostSessionJsonDataObj[i].Q == questionId) {
                        obj.answer = lostSessionJsonDataObj[i].A;
                        obj.timeSpend = lostSessionJsonDataObj[i].TimeSpent;
                        return obj;
                    }
                }
            }
        }
        return obj;
    };

    var createResponseObject = function (groupName, sectionName, questionId, questionString, instructionString, answercollection, TypeOfQuestion, ansString) {
        var isAnswered = (ansString != null && ansString != undefined && ansString.length > 0) ? true : false;
        answercollection = (optionRandomize && answercollection && answercollection.length > 2) ? jumble(answercollection) : answercollection;
        var responseObject = {
            "group": groupName,
            "section": sectionName,
            "questionId": questionId,
            "question": questionString,
            "instruction": instructionString,
            "answers": answercollection,
            "typeOfQuestion": TypeOfQuestion,
            "isQuestionAnswered": isAnswered
        }
        responseArray.push(responseObject);
    };

    var jumble = function (ansCollection) {
        var ansCollectionJumbled = [];
        var count = ansCollection.length;
        var consumedIndex = [];
        consumedIndex.push(count - 1);
        for (var i = 0; i < count - 1; i++) {
            var newIndex = Math.floor(Math.random() * count);
            while ($.inArray(newIndex, consumedIndex) != -1) {
                newIndex = Math.floor(Math.random() * count);
            }
            consumedIndex.push(newIndex);
            ansCollectionJumbled[newIndex] = ansCollection[i];
        }
        ansCollectionJumbled.push(ansCollection[i]);
        return ansCollectionJumbled;
    };

    var setSectionInfo = function (groupName, groupQuesCount, questionRange, totalSectionIndex, optionalGroupName) {
        var divCount = $('#divTotalCountForEachSection');
        var mainDiv = $('#label_strik');
        if (divCount != null) {
            if (totalSectionIndex == 0) {
                mainDiv.height(mainDiv.height() + 7);
                divCount.html("Groups:");
            }
            if (totalSectionIndex % 5 == 0) {
                mainDiv.height(mainDiv.height() + 10);
                innerDivCount = $('<div />').css({ 'width': '100%', 'float': 'left' }).appendTo(divCount);
            }
            if (totalSectionIndex != 0 && totalSectionIndex % 5 != 0) {
                innerDivCount.append(' |');
            }
            var start = questionRange + 1;
            var end = questionRange + groupQuesCount;
            var spanGroupSectionQuestionCount = "";
            if (optionalGroupName && optionalGroupName.length > 0)
                spanGroupSectionQuestionCount = $('<span />').addClass('color_a').css({ 'padding-left': '5px' }).html(optionalGroupName + "[" + groupName + "]" + ": ").appendTo(innerDivCount);
            else
                spanGroupSectionQuestionCount = $('<span />').addClass('color_a').css({ 'padding-left': '5px' }).html(groupName + ": ").appendTo(innerDivCount);
            if (start == end) {
                spanGroupSectionQuestionCount.append($('<label />').html(start).css({ 'color': 'black', 'font-size': '12px' }));
            }
            else {
                spanGroupSectionQuestionCount.append($('<label />').html(start + " to " + end).css({ 'color': 'black', 'font-size': '12px' }));
            }
        }
    };

    var createFlagButtons = function () {
        var totalQuestions = 0;
        var questionInPreviousSection = 0;
        $('#' + Ids.Label.flagButton).css('display', '').html(' ');
        if (isSectionWiseTiming == true) {
            totalQuestions = sectionWiseTiming[sectionIndex].QuestionCount;
            for (var i = 0; i < sectionIndex; i++)
                questionInPreviousSection += sectionWiseTiming[i].QuestionCount;
            currentQuestionNo = questionInPreviousSection;
        }
        else {
            totalQuestions = responseArray.length;
        }
        for (var count = 0; count < totalQuestions; count++) {
            var questionValue = count + questionInPreviousSection;
            var btnTitle = (questionValue + 1);
            if (!IsPsychometricTestType)
                btnTitle = responseArray[questionValue].group + ' - ' + responseArray[questionValue].section;
            var flagBtn = $('<input />').click(function () { htmlTest.FlagClick($(this).attr('value') - 1); }).css({ 'width': '27px', 'margin-right': '3px' }).attr({ 'id': 'flag_' + questionValue, 'type': 'button', 'title': btnTitle, 'value': +(questionValue + 1) }).appendTo('#' + Ids.Label.flagButton);
            if (responseArray[questionValue].isQuestionAnswered == true)
                $(flagBtn).addClass('button_one');
            else
                $(flagBtn).addClass('button_three');
        }
    };

    var viewTestQuestions = function (questionObj, questionNoToSaveTime) {
        if (questionNoToSaveTime)
            questionTimeUpdate(questionNoToSaveTime);
        if (questionObj != null) {
            var toolTip = "";
            if (testInstructionForHP && (TenantInfo.alias.toLowerCase() == "hp" || IsPsychometricTestType)) {
                toolTip = testInstructionForHP.stripTags();
            }
            else if (questionObj.instruction && jQuery.trim(questionObj.instruction).length > 1) {
                toolTip = questionObj.section + " : " + questionObj.instruction;
            }
            else {
                toolTip = questionObj.section;
            }
            $('#' + Ids.Image.instructionHelp).attr('title', toolTip);
            $('#' + Ids.Label.currentQuestionNo).html(currentQuestionNo + 1);
            $('#' + Ids.Label.answerOptions).html($('<div />').attr('id', 'optionsTable').css({ 'width': '95%' }));
            if (!IsPsychometricTestType) {
                var groupName = (questionObj.group && questionObj.group.length > 30) ? questionObj.group.substr(0, 30) + "..." : questionObj.group;
                $('#' + Ids.Label.sectionName).html(groupName).attr('title', questionObj.group);
                var sectionName = (questionObj.section && questionObj.section.length > 30) ? questionObj.section.substr(0, 30) + "..." : questionObj.section;
                $('#' + Ids.Label.subSectionName).html(sectionName).attr('title', questionObj.section);
            }

            typeOfQuestion = questionObj.typeOfQuestion;
			console.log(questionObj.question);
            $('#' + Ids.Label.leftContent).html(questionObj.question).find('p:first').css('display', 'inline');

            var position = answerArray[currentQuestionNo].A;

            switch (typeOfQuestion) {
                case QuestionTypeObj.Boolean:
                    var boolAnswerArray = [];
                    var answerTrue = {
                        "HTMLString": "True",
                        "Choice": "True"
                    };
                    boolAnswerArray.push(answerTrue);
                    var answerFalse = {
                        "HTMLString": "False",
                        "Choice": "False"
                    };
                    boolAnswerArray.push(answerFalse);
                    addAnswerOption(boolAnswerArray, position);
                    break;
                case QuestionTypeObj.MCQ:
                case QuestionTypeObj.Psychometric:
                    if (questionObj.answers != null) {
                        addAnswerOption(questionObj.answers, position);
                    }
                    break;
                case QuestionTypeObj.Subjective:
                    var txtArea = $('<Textarea />').attr({ 'id': questionObj.questionId, 'onpaste': 'return false', 'onblur': 'htmlTest.optionClick()' }).css({ 'height': '95%', 'width': '96%' });
                    if (position != 0 && position != null && position != "null") {
                        $(txtArea).val(position);
                    }
                    $('#' + Ids.Label.answerOptions).append(txtArea);
                    break;
            }
        }
    };

    var addAnswerOption = function (answer, position) {
        if (answer && answer.length > 0) {
            var divAnsOption = $('#optionsTable');
            for (var i = 0; i < answer.length; i++) {
                var lblAnsOption = $('<label />').appendTo(divAnsOption);
                var ckbAnsOption = $('<input name = "question" type = "radio" />').attr({ 'id': 'option' + i, 'value': answer[i].HTMLString, 'choice': answer[i].Choice }).click(function () { htmlTest.optionClick(); }).css({ 'border': '0px', 'margin-right': '5px' }).appendTo(lblAnsOption);
                var spanAnsOption = $('<span />').attr({ 'id': 'span' + answer[i].Choice + 'Text' }).html(answer[i].HTMLString).appendTo(lblAnsOption);
                $(spanAnsOption).find('p:first').css('display', 'inline');
                var trSeprator = divAnsOption.append($('<hr />').addClass('answerSeprator'));
            }
            if (position) {
                divAnsOption.find('input[choice="' + position + '"]').attr('checked', true);
            }
        }
    };

    var FormatTimeFromSeconds = function (secs) {
        var h = parseInt(secs / 3600).toFixed(0);
        var m = secs - (3600 * h);
        m = parseInt(m / 60).toFixed(0);
        var s = secs - (60 * m) - (3600 * h);
        var time = "";
        if (h && h > 0)
            time = (h > 9) ? h + ":" : "0" + h + ":";
        else
            time = "00:";
        if (m && m > 0)
            time += (m > 9) ? m + ":" : "0" + m + ":";
        else
            time += "00:";
        if (s && s > 0)
            time += (s > 9) ? s : "0" + s;
        else
            time += "00";
        return time;
    };

    var timerStart = function () {
        totalRemainingSeconds = 0;
        $('#' + Ids.Label.totalTime).html(totalTime + ' minutes');
        if (enableLastSession == true || IsReactivated == true) {
            if (isSectionWiseTiming == true) {
                totalRemainingSeconds = parseInt(getGroupCookieInfo(sectionIndex, true));
                if (totalRemainingSeconds <= 0 && ((!IsOptionalGroup && sectionIndex < sectionWiseTiming.length - 1) || (IsOptionalGroup && optionalSectionIndex < nonMandatoryCollection.length))) {
                    if (!IsOptionalGroup)
                        sectionWiseTimerStop();
                    else
                        OptionalGroups(nonMandatoryCollection[optionalSectionIndex].JsonGroupcollection.length);
                }
                else if (isNaN(totalRemainingSeconds) || !totalRemainingSeconds) {
                    totalRemainingSeconds = parseInt(totalTime) * 60;
                }
            }
            else {
                if (totalElapsedSeconds == 0 && !IsReactivated)
                    totalRemainingSeconds = parseInt(readCookie("TimeRemaining"));
                else
                    totalRemainingSeconds = (parseInt(totalTime) * 60) - totalElapsedSeconds;
            }
        }
        else {
            totalRemainingSeconds = parseInt(totalTime) * 60;
        }
        if (totalRemainingSeconds != null && totalRemainingSeconds != "-1" && totalRemainingSeconds > 0) {
            totalElapsedSeconds = (parseInt(totalTime) * 60) - totalRemainingSeconds;
            $('#' + Ids.Label.elapsedTime).html(FormatTimeFromSeconds(totalElapsedSeconds));
            $('#' + Ids.Label.remainingTime).html(FormatTimeFromSeconds(totalRemainingSeconds));
            questionStartTime = new Date().getTime();
            if (!timerID) {
                timerID = setInterval(function () { UpdateTimer() }, 1000);
            }
            if (IsintermediateSaveEnabled && !intermediateTimerId)
                intermediateTimerId = setInterval(function () { saveIntimidate() }, (intermediateIntervalInSec * 1000));
            $('#progressCountdown').countdownProgressbar({ totalTime: totalTime * 60, alarmPercentage: [20], setValue: totalRemainingSeconds, stopTimer: false });
        }
        else
            timerStop();
    };

    var UpdateTimer = function () {
        totalRemainingSeconds--;
        totalElapsedSeconds++;
        $('#' + Ids.Label.elapsedTime).html(FormatTimeFromSeconds(totalElapsedSeconds));
        $('#' + Ids.Label.remainingTime).html(FormatTimeFromSeconds(totalRemainingSeconds));
        if (isSectionWiseTiming == true) {
            setGroupCookieInfo(sectionWiseTiming[sectionIndex].SectionId, false, totalRemainingSeconds);
        }
        else {
            createCookie("TimeRemaining", totalRemainingSeconds);
        }
        questionTimeUpdate(currentQuestionNo);
//        if (answerArray[currentQuestionNo] != null && answerArray[currentQuestionNo].TimeSpent != null)
//            answerArray[currentQuestionNo].TimeSpent++;
//        if (IsintermediateSaveEnabled)
//            intermediateAnswerCollection[currentQuestionNo] = answerArray[currentQuestionNo];
       
        if (totalRemainingSeconds > 0) {
            //timerID = setTimeout(function () { UpdateTimer() }, 1000);
        }
        else {
            if (isSectionWiseTiming == true && ((!IsOptionalGroup && sectionIndex < sectionWiseTiming.length - 1) || (IsOptionalGroup && optionalSectionIndex < nonMandatoryCollection.length))) {
                jAlert("Session Time Over You will move to next Section", "Information", function (r) {
                    if (!IsOptionalGroup)
                        sectionWiseTimerStop();
                    else
                        OptionalGroups(nonMandatoryCollection[optionalSectionIndex].JsonGroupcollection.length);
                });
            }
            else
                timerStop();
        }
    };

    var timerStop = function (t) {
        if (timerID) {
            clearInterval(timerID);
            timerID = 0;
        }
        $('#progressCountdown').countdownProgressbar({ stopTimer: true });
        if (intermediateTimerId) {
            clearInterval(intermediateTimerId);
            intermediateTimerId = 0;
        }
        addAnswerArrayToCookie();
        htmlTest.isLoginEnabled = false;
        $('#' + Ids.Label.flagButton).html('<img src="App_Themes/HiRePro/theme1/images/ajax-start-loader.gif" />').attr("align", "right");
        $('#' + Ids.Label.leftContent + ', #' + Ids.Label.answerOptions).html("&nbsp;");
        $('#' + Ids.Button.submitTest + ', #' + Ids.Button.flagUnflag + ', #' + Ids.Button.previous + ', #' + Ids.Button.next + ', #' + Ids.Button.clearAnswer + ', #' + Ids.Button.previewQuestions).button({ disabled: true });
        $("#modalChooseGroup, #modalPreviewGroup").remove();
		if(t==1)
		{
        SubmitResult();
		}
    };

    var questionTimeUpdate = function (questionNo) {
        questionEndTime = new Date().getTime();
        var timeDiffer = ((questionEndTime - questionStartTime) / 1000).toFixed(2);
        questionStartTime = new Date().getTime();
        answerArray[questionNo].TimeSpent += parseFloat(timeDiffer);
        if (IsintermediateSaveEnabled)
            intermediateAnswerCollection[answerArray[questionNo].Q] = answerArray[questionNo];
    };

    var HandlingAnswer = function () {
        var answerText = "";
        if (typeOfQuestion == QuestionTypeObj.Boolean || typeOfQuestion == QuestionTypeObj.MCQ || QuestionTypeObj.Psychometric)
            answerText = $('input[name="question"]:checked').attr('choice');
        if (typeOfQuestion == QuestionTypeObj.Subjective) {
            answerText = $('#' + responseArray[currentQuestionNo].questionId).val();
        }
        answerArray[currentQuestionNo].A = answerText;
        responseArray[currentQuestionNo].isQuestionAnswered = (answerText && answerText != "") ? true : false;
        addAnswerArrayToCookie();
    };

    var addAnswerArrayToCookie = function () {
        if (IsintermediateSaveEnabled)
            intermediateAnswerCollection[answerArray[currentQuestionNo].Q] = answerArray[currentQuestionNo];
        var JsonObjectArray = [];
        if (answerArray != null && answerArray.length != 0) {
            for (var i = 0; i < answerArray.length; i++) {
                var jsonObject = {
                    "Q": answerArray[i].Q,
                    "A": answerArray[i].A,
                    "TimeSpent": answerArray[i].TimeSpent
                }
                JsonObjectArray.push(jsonObject);
            }
            createCookie('CandidateAnswers', $.stringify(JsonObjectArray));
            createCookie('CandidateAnswersTimeStamp', new Date());
            cookiesModified = true;
        }
    };

    var updateFlagBtn = function () {
        var obj = $('#flag_' + currentQuestionNo);
        if (obj.attr('class') != "button_two") {
            if (responseArray[currentQuestionNo].isQuestionAnswered == true) {
                obj.attr('class', 'button_one');
            }
            else {
                obj.attr('class', 'button_three');
            }
        }
    };

    var sectionWiseTimerStop = function () {
        if (timerID) {
            clearTimeout(timerID);
            timerID = 0;
        }
        if (sectionIndex < sectionWiseTiming.length - 1) {
            setGroupCookieInfo(sectionWiseTiming[sectionIndex].SectionId, false, 0);
            sectionIndex++;
            currentQuestionNo++;
            questionIndexer = 0;
            var totalQuestionToView = (isSectionWiseTiming == true) ? sectionWiseTiming[sectionIndex].QuestionCount - 1 : responseArray.length - 1;
            if (questionIndexer == totalQuestionToView && ((!IsOptionalGroup && sectionIndex == sectionWiseTiming.length - 1) || (IsOptionalGroup && optionalSectionIndex == nonMandatoryCollection.length))) {
                $('#' + Ids.Button.next).button({ disabled: true });
                $('#' + Ids.Button.submitTest).button({ disabled: false });
            }
            $('#' + Ids.Button.previous).button({ disabled: true });
            createFlagButtons();
            viewTestQuestions(responseArray[currentQuestionNo], currentQuestionNo - 1);
            totalTime = sectionWiseTiming[sectionIndex].SectionTotalTime;
            timerStart();
        }
    };

    var OptionalGroups = function (options) {
        var sectionAttended = null;
        var sectionAttended = getGroupCookieInfo(optionalSectionIndex, false);
        if (enableLastSession != null && (enableLastSession == true || IsReactivated == true) && sectionAttended != null) {
            optionalShow(sectionAttended);
        }
        else {
            var optionRadio = null;
            $('#modalChooseGroup').remove();

            var modalOptionGroup = $('<div />').attr({ 'id': 'modalChooseGroup', 'title': 'Choose Group' });
            var divOptionGrps = $('<div />').attr({ 'id': 'divChooseGroup' }).css({ 'margin': '5px' }).appendTo(modalOptionGroup);
            var optionList = $('<ol />').appendTo(divOptionGrps);
            var optionListHeight = 0;
            for (var i = 0; i < options; i++) {
                var optionLi = $('<li />').css('margin-bottom', '5px').appendTo(optionList);
                var optionLbl = $('<label />').appendTo(optionLi);
                optionRadio = $('<input />').attr({ 'name': 'preview', 'type': 'radio', 'id': 'opt' + i, 'value': i, 'border': '0px' }).appendTo(optionLbl);
                if (i == 0)
                    $(optionRadio).attr('checked', 'checked');
                var optionSpan = $('<span />').attr({ 'id': 'span' + i }).html(nonMandatoryCollection[optionalSectionIndex].JsonGroupcollection[i].Name).appendTo(optionLbl);
                optionListHeight += 20;
            }

            var divButton = $('<div />').attr({ 'id': 'modalButton' }).css({ 'margin': '5px', 'float': 'right' }).appendTo(modalOptionGroup);
            var dlMsg = $('<dl />').appendTo(divButton);
            var ddMsg = $('<dd />').append($('<div />').attr('id', 'err_txt_group_select').css({ 'color': '#ff0000', 'display': 'none' }).html('<b>* Select an Option to Proceed.</b>')).appendTo(dlMsg);

            $('#divChooseGroupWindow').html(modalOptionGroup);
            jQuery("#modalChooseGroup").dialog({
                height: 150 + optionListHeight,
                width: 400,
                modal: true,
                beforeclose: function (event, ui) { return checkSectionTime(); },
                buttons: {
                    "Add": function () { addGroup(); },
                    "Preview": function () { preview(false); }
                }
            });
        }
    };

    var addGroup = function () {
        var selectedSection = $('input[name="preview"]:checked').attr('value');
        if (selectedSection) {
            jConfirm('Are you sure, you want to add Group - ' + nonMandatoryCollection[optionalSectionIndex].JsonGroupcollection[selectedSection].Name + '. You cannot reselect other section .', 'Information', function (r) {
                if (r) {
                    jQuery("#modalChooseGroup").dialog("destroy");
                    setGroupCookieInfo(nonMandatoryCollection[optionalSectionIndex].JsonGroupcollection[selectedSection].Id, true, 1, selectedSection);
                    optionalShow(selectedSection);
                }
            });
        }
    };

    var optionalShow = function (selectedSection) {
        var optionalGroups = nonMandatoryCollection[optionalSectionIndex].JsonGroupcollection[selectedSection];
        var groupName = optionalGroups.Name;
        var qIdArray = [];
        var sectionQuestionCount = 0;
        for (var i = 0; i < optionalGroups.SectionTypeCollection.length; i++) {
            var sectionName = optionalGroups.SectionTypeCollection[i].Name;
            var instructionString = optionalGroups.SectionTypeCollection[i].Instruction;
            sectionQuestionCount += optionalGroups.SectionTypeCollection[i].QuestionType.length;
            for (var j = 0; j < optionalGroups.SectionTypeCollection[i].QuestionType.length; j++) {
                var TypeOfQuestion = optionalGroups.SectionTypeCollection[i].QuestionType[j].TypeOfQuestion;
                if (TypeOfQuestion == QuestionTypeObj.RTC) {
                    sectionQuestionCount--;
                    questionString = "<b>Passage:</b><br/>" + optionalGroups.SectionTypeCollection[i].QuestionType[j].HTMLString + "<br/><b>Question:</b><br/>";
                    var CQGTC = optionalGroups.SectionTypeCollection[i].QuestionType[j].ChildQuestionGeneralTypeCollection;
                    for (var k = 0; k < CQGTC.length; k++) {
                        sectionQuestionCount++;
                        queId = CQGTC[k].QuestionId;
                        var queStr = questionString + CQGTC[k].HTMLString;
                        answerCollection = CQGTC[k].AnswerChoiceCollection;
                        TypeOfQuestion = CQGTC[k].TypeOfQuestion;
                        QuestionType = CQGTC;
                        var answerTimeObj = getObjFromCookie(queId);
                        var ansString = answerTimeObj[0];
                        var timeSpend = answerTimeObj[1];
                        var sectionId = optionalGroups.SectionTypeCollection[i].SectionId;
                        var questionTenantId = CQGTC[k].QuestionTenantId;
                        createResponseObject(groupName, sectionName, queId, queStr, instructionString, answerCollection, TypeOfQuestion, answerTimeObj.answer);
                        qIdArray.push(queId);
                        var answerObject = {
                            "Q": queId,
                            "A": answerTimeObj.answer,
                            "TimeSpent": (answerTimeObj.timeSpend == null) ? 0 : answerTimeObj.timeSpend,
                            "SecId": sectionId
                        }
                        if (TenantInfo.tenantId != questionTenantId)
                            answerObject.QTenantId = questionTenantId;
                        answerArray.push(answerObject);
                    }
                }
                else if (TypeOfQuestion == QuestionTypeObj.Boolean || TypeOfQuestion == QuestionTypeObj.MCQ || TypeOfQuestion == QuestionTypeObj.Subjective || TypeOfQuestion == QuestionTypeObj.Psychometric) {
                    var queId = optionalGroups.SectionTypeCollection[i].QuestionType[j].QuestionId;
                    var questionString = optionalGroups.SectionTypeCollection[i].QuestionType[j].HTMLString;
                    var answerCollection = optionalGroups.SectionTypeCollection[i].QuestionType[j].AnswerChoiceCollection;
                    var QuestionType = optionalGroups.SectionTypeCollection[i].QuestionType[j];
                    var answerTimeObj = getObjFromCookie(queId);
                    var sectionId = optionalGroups.SectionTypeCollection[i].SectionId;
                    var questionTenantId = optionalGroups.SectionTypeCollection[i].QuestionType[j].QuestionTenantId;
                    createResponseObject(groupName, sectionName, queId, questionString, instructionString, answerCollection, TypeOfQuestion, answerTimeObj.answer);
                    qIdArray.push(queId);
                    var answerObject = {
                        "Q": queId,
                        "A": answerTimeObj.answer,
                        "TimeSpent": (answerTimeObj.timeSpend == null) ? 0 : answerTimeObj.timeSpend,
                        "SecId": sectionId
                    }
                    if (TypeOfQuestion == QuestionTypeObj.Subjective)
                        answerObject.IsSubjective = true;
                    if (TenantInfo.tenantId != questionTenantId)
                        answerObject.QTenantId = questionTenantId;
                    answerArray.push(answerObject);
                }
            }
        }
        optionalSectionIndex++;
        if (isSectionWiseTiming == true) {
            var sectionWiseTimingObj = {
                "SectionId": optionalGroups.Id,
                "SectionName": groupName,
                "QuestionCount": sectionQuestionCount,
                "SectionTotalTime": optionalGroups.TotalTime
            };
            sectionWiseTiming.push(sectionWiseTimingObj);
            sectionWiseTimerStop();
        }
        else {
            currentQuestionNo++;
            questionIndexer++;
            var totalQuestionToView = (isSectionWiseTiming == true) ? sectionWiseTiming[sectionIndex].QuestionCount - 1 : responseArray.length - 1;
            if (IsOptionalGroup && questionIndexer == totalQuestionToView && optionalSectionIndex == nonMandatoryCollection.length) {
                $('#' + Ids.Button.next).button({ disabled: true });
                $('#' + Ids.Button.submitTest).button({ disabled: false });
            }
            createFlagButtons();
            viewTestQuestions(responseArray[currentQuestionNo], currentQuestionNo - 1);
        }
    };

    var preview = function (isFullQuestionPaper) {
        var htmlString = null;
        $('#divQuestionPreview').empty();
        $('#tblContent').find('td').parent('tr').remove();
        if (!isFullQuestionPaper) {
            var selectedSection = $('input[name="preview"]:checked').attr('value');
            htmlString = createPreview(nonMandatoryCollection[optionalSectionIndex].JsonGroupcollection[selectedSection]);
            $('#divQuestionPreview').html($('<div />').html(htmlString).attr({ 'id': 'modalPreviewGroup', 'Title': 'Preview: ' + nonMandatoryCollection[optionalSectionIndex].JsonGroupcollection[selectedSection].Name }));
        }
        else {
            var contentDiv = $('<div />').html('').attr({ 'id': 'modalPreviewGroup', 'Title': 'Preview' })
            for (var groupIndex = 0; groupIndex < mandatoryCollection.length; groupIndex++) {
                htmlString = createPreview(mandatoryCollection[groupIndex]);
                $(contentDiv).append(htmlString);
            }
            $('#divQuestionPreview').append(contentDiv);
        }
        if (htmlString) {
            jQuery("#modalPreviewGroup").dialog({
                height: 650,
                width: 950,
                modal: true
            });
        }
    };

    //    var createPreview = function (previewGroup) {
    //        if (previewGroup) {
    //            var htmlString = $('<div />');
    //            $('#tblContent').append($('<tr>').append($('<td />').html(previewGroup.Name)).append($('<td />')).append($('<td />')));
    //            for (var i = 0; i < previewGroup.SectionTypeCollection.length; i++) {
    //                var headerDiv = $('<div />').html($('<b />').html('Group: ' + previewGroup.Name)).css('padding', '2px').addClass('ui-widget ui-widget-content ui-corner-all').append($('<br />')).appendTo(htmlString);
    //                $(headerDiv).append($('<hr />'));
    //                $(headerDiv).append($('<b />').html('Section: ' + previewGroup.SectionTypeCollection[i].Name)).append($('<br />'));
    //                $('#tblContent').append($('<tr>').append($('<td />')).append($('<td />').html(previewGroup.SectionTypeCollection[i].Name)).append($('<td />').html(previewGroup.SectionTypeCollection[i].QuestionType.length)));
    //                var qType = $('<b />');
    //                $(headerDiv).append(qType);
    //                $(headerDiv).append($('<b />').html('Instruction: ')).append(previewGroup.SectionTypeCollection[i].Instruction).append($('<br />'));

    //                for (var j = 0; j < previewGroup.SectionTypeCollection[i].QuestionType.length; j++) {
    //                    var typeofQuestion = previewGroup.SectionTypeCollection[i].QuestionType[j].TypeOfQuestion;
    //                    var queStr = $('<span />');
    //                    switch (typeofQuestion) {
    //                        case QuestionTypeObj.Boolean:
    //                            $(qType).html($('<b />').html('Question Type: Boolean Question')).append($('<br />'));
    //                            $(queStr).append('<br/>' + previewGroup.SectionTypeCollection[i].QuestionType[j].HTMLString + '<br/>').find('p:first').css('display', 'inline');
    //                            break;
    //                        case QuestionTypeObj.MCQ:
    //                            $(qType).html($('<b />').html('Question Type: Multiple choices Question')).append($('<br />'));
    //                            $(queStr).append('<br/>' + previewGroup.SectionTypeCollection[i].QuestionType[j].HTMLString + '<br/>').find('p:first').css('display', 'inline');
    //                            break;
    //                        case QuestionTypeObj.Subjective:
    //                            $(qType).html($('<b />').html('Question Type: Subjective Question')).append($('<br />'));
    //                            $(queStr).append('<br/>' + previewGroup.SectionTypeCollection[i].QuestionType[j].HTMLString + '<br/>').find('p:first').css('display', 'inline');
    //                            break;
    //                        case QuestionTypeObj.RTC:
    //                            $(qType).html($('<b />').html('Question Type: Reference To Context')).append($('<br />'));
    //                            $(queStr).append('<b> Passage:</b><br/>' + previewGroup.SectionTypeCollection[i].QuestionType[j].HTMLString + '<br/>').find('p:first').css('display', 'inline');
    //                            var CQGTC = previewGroup.SectionTypeCollection[i].QuestionType[j].ChildQuestionGeneralTypeCollection;
    //                            for (var k = 0; k < CQGTC.length; k++) {
    //                                $(queStr).append($('<b />').html('Question: ' + (j + 1) + '.' + (k + 1))).append(CQGTC[k].HTMLString);
    //                            }
    //                            break;
    //                    }
    //                    $(htmlString).append($('<b />').html('Question: ' + (j + 1))).append(queStr).append('<br />');
    //                }
    //            }
    //            return htmlString;
    //        }
    //    };

    var createPreview = function (previewGroup) {
        if (previewGroup) {
            var htmlString = $('<div />');
            $('#tblContent').append($('<tr>').append($('<td />').html(previewGroup.Name)).append($('<td />')).append($('<td />')));
            var groupFieldSet = $('<fieldset />').addClass('ui-widget ui-widget-content ui-corner-all').append($('<legend />').html($('<b />').html('Group: ' + previewGroup.Name))).appendTo(htmlString);
            for (var i = 0; i < previewGroup.SectionTypeCollection.length; i++) {
                $('#tblContent').append($('<tr>').append($('<td />')).append($('<td />').html(previewGroup.SectionTypeCollection[i].Name)).append($('<td />').html(previewGroup.SectionTypeCollection[i].QuestionType.length)));
                var headerDiv = $('<div />').addClass('ui-state-default ui-corner-all').css('padding', '5px 10px').html($('<b />').html('Section: ' + previewGroup.SectionTypeCollection[i].Name)).append($('<br />')).appendTo(groupFieldSet);
                var sectionInfoDiv = $('<div />').css('padding-left', '10px').appendTo(groupFieldSet);
                var qType = $('<b />');
                $(sectionInfoDiv).append(qType);
                $(sectionInfoDiv).append($('<b />').html('Instruction: ')).append(previewGroup.SectionTypeCollection[i].Instruction).append($('<br />'));

                for (var j = 0; j < previewGroup.SectionTypeCollection[i].QuestionType.length; j++) {
                    var typeofQuestion = previewGroup.SectionTypeCollection[i].QuestionType[j].TypeOfQuestion;
                    var queStr = $('<div />').css('padding-left', '10px');
                    switch (typeofQuestion) {
                        case QuestionTypeObj.Boolean:
                            $(qType).html($('<b />').html('Question Type: Boolean Question')).append($('<br />'));
                            $(queStr).append(previewGroup.SectionTypeCollection[i].QuestionType[j].HTMLString + '<br/>').find('p:first').css('display', 'inline');
                            break;
                        case QuestionTypeObj.MCQ:
                            $(qType).html($('<b />').html('Question Type: Multiple choices Question')).append($('<br />'));
                            $(queStr).append(previewGroup.SectionTypeCollection[i].QuestionType[j].HTMLString + '<br/>').find('p:first').css('display', 'inline');
                            break;
                        case QuestionTypeObj.Psychometric:
                            $(qType).html($('<b />').html('Question Type: Psychometric Question')).append($('<br />'));
                            $(queStr).append(previewGroup.SectionTypeCollection[i].QuestionType[j].HTMLString + '<br/>').find('p:first').css('display', 'inline');
                            break;
                        case QuestionTypeObj.Subjective:
                            $(qType).html($('<b />').html('Question Type: Subjective Question')).append($('<br />'));
                            $(queStr).append(previewGroup.SectionTypeCollection[i].QuestionType[j].HTMLString + '<br/>').find('p:first').css('display', 'inline');
                            break;
                        case QuestionTypeObj.RTC:
                            $(qType).html($('<b />').html('Question Type: Reference To Context')).append($('<br />'));
                            $(queStr).append('<b> Passage:</b><br/>' + previewGroup.SectionTypeCollection[i].QuestionType[j].HTMLString + '<br/>').find('p:first').css('display', 'inline');
                            var CQGTC = previewGroup.SectionTypeCollection[i].QuestionType[j].ChildQuestionGeneralTypeCollection;
                            for (var k = 0; k < CQGTC.length; k++) {
                                $(queStr).append($('<b />').html('Question: ' + (j + 1) + '.' + (k + 1))).append(CQGTC[k].HTMLString);
                            }
                            break;
                    }
                    $(groupFieldSet).append($('<b />').css('padding-left', '10px').html('Question: ' + (j + 1))).append(queStr).append('<br />');
                }
            }
            return htmlString;
        }
    };

    var SubmitResult = function () {
        var JsonObjectArray = [];
        if (IsintermediateSaveEnabled) {
            $.each(intermediateAnswerCollection, function (i, item) {
                JsonObjectArray.push(item);
            });
            if (answerArray != null && answerArray.length > 0) {
                for (var i = 0; i < answerArray.length; i++) {
                    if (!responseArray[i].isQuestionAnswered && answerArray[i].TimeSpent == 0)
                        JsonObjectArray.push(answerArray[i]);
                }
            }
        }
        if (!IsintermediateSaveEnabled && answerArray != null && answerArray.length != 0) {
            JsonObjectArray = answerArray;
        }
        //var timeStamp = (new Date()).toString();
        //        newDate.setTime(newDate.getTime() + (3600 * 1000));
        //        var timeStamp = newDate.toGMTString();
        var d = new Date();
        var curr_date = d.getDate();
        var curr_month = d.getMonth() + 1; //Months are zero based
        var curr_year = d.getFullYear();
        var curr_hour = d.getHours();
        var curr_min = d.getMinutes();
        var curr_sec = d.getSeconds();
        var timeStamp = curr_month + "/" + curr_date + "/" + curr_year + " " + curr_hour + ":" + curr_min + ":" + curr_sec;

        var JsonSubmitResult = {
            "TestId": testId,
            "TenantId": TenantInfo.tenantId,
            "UserId": "0",
            "TestUserId": candidateId,
            "IsPartialSubmission": false,
            "TotalTimeSpent": totalElapsedSeconds,
            "Answers": JsonObjectArray,
            "TimeStamp": timeStamp
        };
        var JsonOfflineSubmitResult = {
            "IsOfflineTestResult": true
        };
        $.extend(JsonOfflineSubmitResult, JsonSubmitResult);
        JsonOfflineSubmitResult.Answers = answerArray;

        encString = Base64.encode($.stringify(JsonOfflineSubmitResult));
        callAjaxService("SubmitTestResult", JsonSubmitResult, callBackJsonSubmitResult, callBackJsonSubmitResultFail);
        testSubmissionTimerId = setTimeout(function () { callBackJsonSubmitResultFail({ "Message": "Results Cannot be Submitted Now" }) }, (testSubmissionIntervalInSec * 1000));

        function callBackJsonSubmitResult(response) {
            if (response == null || response.IsException == true || response.IsFailure == true) {
                callBackJsonSubmitResultFail(response);
            }
            else if (response != null) {
                if (testSubmissionTimerId) {
                    clearTimeout(testSubmissionTimerId);
                    testSubmissionTimerId = null;
                }
                cleanupAfterSubmission();
            }
        }

        function callBackJsonSubmitResultFail(response) {
            if (testSubmissionTimerId) {
                clearTimeout(testSubmissionTimerId);
                testSubmissionTimerId = null;
            }
            if (!isSubmit) {
                var errorMsg = (response && response.Message) ? response.Message : "Results Cannot be Submitted Now";
                if (testSubmissionFailure < 3)
                    errorMsg += ", Click OK to retry"
                else
                    errorMsg += ", Click OK to retry  <input type='button' onclick='htmlTest.showCandidateAnswer();' value='Get Candidate Answer' />"
                jAlert(errorMsg, "Network ERROR", function (r) {
                    testSubmissionFailure++;
                    SubmitResult();
                });
                testSubmissionTimerId = setTimeout(function () { callBackJsonSubmitResultFail({ "Message": "Results Cannot be Submitted Now" }) }, (testSubmissionIntervalInSec * 1000));
            }
        }
    };

    var cleanupAfterSubmission = function () {
        isSubmit = true;
        try {
            $("#modalPreviewGroup").dialog("destroy");
            $("#modalChooseGroup").dialog("destroy");
        }
        catch (ex) {
        }
        $('#' + Ids.Label.sectionAndSubsectionName).css('display', 'none');
        $('#' + Ids.Label.candidateName).css('display', 'none');
        $('#' + Ids.Label.testTiming).css('display', 'none');
        $('#' + Ids.PagesDiv.testPage).css('display', 'none');
        $('#' + Ids.Button.flagUnflag).css('display', 'none');
        $('#' + Ids.Button.previous).css('display', 'none');
        $('#' + Ids.Button.next).css('display', 'none');
        $('#' + Ids.Button.clearAnswer).css('display', 'none');
        $('#' + Ids.Button.submitTest).css('display', 'none');
        $('#' + Ids.Label.flagButton).css('display', 'none');
        fixedDivHeight = 320;
        htmlTest.setPageHeightAndWidth();
        $('#' + Ids.Label.leftHeader).html('Test Submission');
        $('#' + Ids.Label.rightHeader).html('&nbsp;');
        $('#' + Ids.Label.leftContent).html('<h1>You have successfully submitted the Online Test</h1><br /><h2>It is now safe to close or navigate away from the current window.</h2>');
        $('#' + Ids.PagesDiv.submitSuccessPage).css('display', '');
        if (instantResult)
            $('#' + Ids.Button.viewResult).css('display', '').click(function () { getCandidateResult(); }).button();
        $('#divTotalCountForEachSection').empty();
        $('#progressCountdown').css('display', 'none');
    };

    var saveIntimidate = function () {
        //var timeStamp = new Date();
        //        newDate.setTime(newDate.getTime() + (3600 * 1000));
        //        var timeStamp = newDate.toGMTString();

        var d = new Date();
        var curr_date = d.getDate();
        var curr_month = d.getMonth() + 1; //Months are zero based
        var curr_year = d.getFullYear();
        var curr_hour = d.getHours();
        var curr_min = d.getMinutes();
        var curr_sec = d.getSeconds();
        var timeStamp = curr_month + "/" + curr_date + "/" + curr_year + " " + curr_hour + ":" + curr_min + ":" + curr_sec;
        var JsonSubmitResult = {
            "TestId": testId,
            "TenantId": TenantInfo.tenantId,
            "UserId": "0",
            "TestUserId": candidateId,
            "IsPartialSubmission": true,
            "TotalTimeSpent": totalElapsedSeconds,
            "TimeStamp": timeStamp
        };
        var answersCollection = [];
        $.each(intermediateAnswerCollection, function (i, item) {
            answersCollection.push(item);
        });
        var groupSelectionCollection = [];
        $.each(intermediateGroupCollection, function (i, item) {
            groupSelectionCollection.push(item);
        });
        JsonSubmitResult.Answers = answersCollection;
        JsonSubmitResult.TestUserGroupInfoCollection = groupSelectionCollection;
        var index = intermediateIndexer;
        callAjaxService("SubmitTestResult", JsonSubmitResult, function (response) { callBackJsonSaveIntimidate(response, index) }, function (response) { callBackJsonSaveIntimidateFail(response, index) });

//        var intermediateObj = {
//            ansRequest: intermediateAnswerCollection,
//            selGroups: intermediateGroupCollection,
//            status: false
//        };
//        intermediateAnswerObjCollection[intermediateIndexer++] = intermediateObj;
//        intermediateAnswerCollection = {};
//        intermediateGroupCollection = {};

        function callBackJsonSaveIntimidate(response, index) {
//            if ((response != null && (response.IsException == true || response.IsFailure == true)) || response == null) {
//                callBackJsonSaveIntimidateFail(response, index);
//            }
//            else {
//                intermediateAnswerObjCollection[index].status = true;
//            }
        }

        function callBackJsonSaveIntimidateFail(response, index) {
//            $.each(intermediateAnswerObjCollection[index].ansRequest, function (i, item) {
//                if (!intermediateAnswerCollection[parseInt(item.Q)])
//                    intermediateAnswerCollection[parseInt(item.Q)] = item;
//            });
//            $.each(intermediateAnswerObjCollection[index].selGroups, function (i, item) {
//                if (!intermediateGroupCollection[parseInt(item.GroupId)])
//                    intermediateGroupCollection[parseInt(item.GroupId)] = item;
//            });
        }
    };

    var setGroupCookieInfo = function (id, isSelectedOptionalGroup, timeRemains, selectedSection) {
        if (IsintermediateSaveEnabled) {
            var obj = {
                "GroupId": id,
                "IsSelectedOptionalGroup": isSelectedOptionalGroup,
                "TimeSpent": timeRemains
            };
            intermediateGroupCollection[id] = obj;
        }
        if (isSelectedOptionalGroup) {
            var cokieName = "optionalgrp_" + loginName + optionalSectionIndex;
            createCookie(cokieName, selectedSection);
        }
        else {
            createCookie(jQuery.trim(loginName + sectionWiseTiming[sectionIndex].SectionId), timeRemains);
        }
    };

    var getGroupCookieInfo = function (index, returnTime) {
        if (IsintermediateSaveEnabled && lostSessionGroupObj.length > 0) {
            if (returnTime) {
                for (var i = 0; i < lostSessionGroupObj.length; i++) {
                    if (sectionWiseTiming[sectionIndex].SectionId == lostSessionGroupObj[i].GroupId)
                        return lostSessionGroupObj[i].TimeSpent;
                }
            }
            else {
                for (var i = 0; i < nonMandatoryCollection[optionalSectionIndex].JsonGroupcollection.length; i++) {
                    for (var j = 0; j < lostSessionGroupObj.length; j++) {
                        if (nonMandatoryCollection[optionalSectionIndex].JsonGroupcollection[i].Id == lostSessionGroupObj[j].GroupId)
                            return i;
                    }
                }
            }
        }
        else {
            if (returnTime) {
                var sectionCookie = jQuery.trim(loginName) + sectionWiseTiming[sectionIndex].SectionId;
                return parseInt(readCookie(jQuery.trim(sectionCookie)));
            }
            else {
                var cokieName = "optionalgrp_" + loginName + optionalSectionIndex;
                return readCookie(cokieName);
            }
        }
    };

    var getCandidateResult = function () {
        var jsonRequest = {
            "TestId": testId,
            "CandidateId": candidateId,
            "TenantId": TenantInfo.tenantId,
            "UserId": "0"
        }
        callAjaxService("GetCandidateResults", jsonRequest, callBackCandidateResult, callBackGetCandidateResultsFail);

        function callBackCandidateResult(response) {
            if (response != null && (response.IsException == true || response.IsFailure == true)) {
                return;
            }
            if (null != response && null != response.CandidateScore && null != response.CandidateScore.TotalCandidateScore && response.CandidateScore.TotalCandidateScore.length > 0) {
                if (null != response.CandidateScore.TotalCandidateScore) {
                    var tabledata = $('<table/>').attr({ cellspacing: "0", cellpadding: "5", width: "100%" }).addClass("ui-widget-content").append($('<tr/>').attr("class", "ui-widget-header").append($('<td/>')).append($('<td/>').html('Group').css({ 'font-weight': 'bold', 'width': '20%' })).append($('<td/>').html("Correct").css({ 'font-weight': 'bold', 'width': '20%' })).append($('<td/>').html("Wrong").css({ 'font-weight': 'bold', 'width': '20%' })).append($('<td/>').html("UnAttempted").css({ 'font-weight': 'bold', 'width': '20%' })).append($('<td/>').html("Score").css({ 'font-weight': 'bold', 'width': '20%' })));
                    for (var i = 0; i < response.CandidateScore.TotalCandidateScore.length; i++) {
                        if (response.CandidateScore.TotalCandidateScore[i].ParentGroupId == 0) {
                            var trscoredetails = $('<tr/>').attr("id", "trscore" + i).append($('<td/>').html('&nbsp;')).append($('<td/>').html(response.CandidateScore.TotalCandidateScore[i].GroupName)).append($('<td/>').html(response.CandidateScore.TotalCandidateScore[i].CorrectAnswer)).append($('<td/>').html(response.CandidateScore.TotalCandidateScore[i].InCorrectAnswer)).append($('<td/>').html(response.CandidateScore.TotalCandidateScore[i].UnAttendedQuestions)).append($('<td/>').html(response.CandidateScore.TotalCandidateScore[i].Score)).appendTo(tabledata);
                        }
                        var sectiontabledata = $('<table/>').attr({ cellspacing: "0", cellpadding: "0", width: "100%" }).addClass("ui-corner-all ui-widget-content").attr("style", "margin-bottom:5px", "margin-top:5px").append($('<tr/>').addClass("ui-widget-header").append($('<td/>').html("Section Name").css({ 'font-weight': 'bold', 'width': '20%' })).append($('<td/>').html("Correct").css({ 'font-weight': 'bold', 'width': '20%' })).append($('<td/>').html("Wrong").css({ 'font-weight': 'bold', 'width': '20%' })).append($('<td/>').html("UnAttempted").css({ 'font-weight': 'bold', 'width': '20%' })).append($('<td/>').html("Score").css({ 'font-weight': 'bold', 'width': '20%' })));
                        for (var j = 1; j < response.CandidateScore.TotalCandidateScore.length; j++) {
                            if (response.CandidateScore.TotalCandidateScore[j].ParentGroupId > 0 && response.CandidateScore.TotalCandidateScore[i].GroupId == response.CandidateScore.TotalCandidateScore[j].ParentGroupId) {
                                sectiontabledata.append($('<tr/>').append($('<td/>').html(response.CandidateScore.TotalCandidateScore[j].GroupName)).append($('<td/>').html(response.CandidateScore.TotalCandidateScore[j].CorrectAnswer)).append($('<td/>').html(response.CandidateScore.TotalCandidateScore[j].InCorrectAnswer)).append($('<td/>').html(response.CandidateScore.TotalCandidateScore[j].UnAttendedQuestions)).append($('<td/>').html(response.CandidateScore.TotalCandidateScore[j].Score)));
                            }
                        }
                        if ($(sectiontabledata).find('tr').length > 1)
                            var trScoreDetails = $('<tr/>').attr('id', "ScoreRelated" + i).append($('<td/>')).append($('<td/>').attr({ colspan: "5" }).append(sectiontabledata)).appendTo(tabledata);
                    }
                    var div = $('<div />').attr('id', 'modalScore').append(tabledata);
                    $('#divChooseGroupWindow').html(div);

                    jQuery("#modalScore").dialog({
                        height: 350,
                        width: 950,
                        title: candidateName,
                        modal: true
                    });
                }
            }
        }

        function callBackGetCandidateResultsFail(response) {
        }
    };

    this.ShowSectionScore = function (i) {
        var anchorvalue = $("#ScoreAnchor" + i).html();
        if (anchorvalue == "+") {
            $("#ScoreAnchor" + i).html("-")
            $("#ScoreRelated" + i).css('display', '');
        } else {
            $("#ScoreAnchor" + i).html("+")
            $("#ScoreRelated" + i).css('display', 'none');
        }
    };
    /*Methods Private*/
}

//Other Public methods
function getTenantInfo() {
    var urlString = window.location.search.substring(1);
    var urlArray = urlString.split("&");
    for (i = 0; i < urlArray.length; i++) {
        var ft = urlArray[i].split("=");
        if (ft[0] == 'alias')
            TenantInfo.alias = ft[1];
    }
    TenantInfo.tenantId = $('#ctl00_DefaultContent_tenant_id').val();
}

function getBrowser() {
    var name;
    $.each($.browser, function (i, val) {
        if (val == true) {
            name = i;
        }
    });
    return name;
}

function getOS(obj) {
    var os = new Array("unknown", "unknown");
    ((obj == null || obj == "") ? brs = navigator.userAgent.toLowerCase() : brs = obj);
    if (brs.search(/windows\sce/) != -1) {
        os[0] = "WinCE";
        try {
            os[1] = brs.match(/windows\sce\/(\d+(\.?\d)*)/)[1];
        } catch (e) { }
        return os;
    } else if ((brs.search(/windows/) != -1) || ((brs.search(/win9\d{1}/) != -1))) {
        os[0] = "Windows";
        if (brs.search(/nt\s5\.1/) != -1) {
            os[1] = "XP";
        } else if (brs.search(/nt\s5\.0/) != -1) {
            os[1] = "2000";
        } else if ((brs.search(/win98/) != -1) || (brs.search(/windows\s98/) != -1)) {
            os[1] = "98";
        } else if (brs.search(/windows\sme/) != -1) {
            os[1] = "ME";
        } else if (brs.search(/nt\s5\.2/) != -1) {
            os[1] = "Win2K3";
        } else if ((brs.search(/windows\s95/) != -1) || (brs.search(/win95/) != -1)) {
            os[1] = "95";
        } else if ((brs.search(/nt\s4\.0/) != -1) || (brs.search(/nt4\.0/)) != -1) {
            os[1] = "NT4";
        }
        else if (brs.search(/nt\s6\.0/) != -1) {
            os[1] = "Vista/WinServer2008";
        }
        else if (brs.search(/nt\s6\.1/) != -1) {
            os[1] = "Windows7";
        }
        return os;
    } else if (brs.search(/linux/) != -1) {
        os[0] = "Linux";
        try {
            os[1] = brs.match(/linux\s?(\d+(\.?\d)*)/)[1];
        } catch (e) { }
        return os;
    } else if (brs.search(/mac\sos\sx/) != -1) {
        os[0] = "MacOS-X";
        return os;
    } else if (brs.search(/freebsd/) != -1) {
        os[0] = "FreeBsd";
        try {
            os[1] = brs.match(/freebsd\s(\d(\.\d)*)*/)[1];
        } catch (e) { }
        return os;
    } else if (brs.search(/sunos/) != -1) {
        os[0] = "SunOS";
        try {
            os[1] = brs.match(/sunos\s(\d(\.\d)*)*/)[1];
        } catch (e) { }
        return os;
    } else if (brs.search(/irix/) != -1) {
        os[0] = "Irix";
        try {
            os[1] = brs.match(/irix\s(\d(\.\d)*)*/)[1];
        } catch (e) { }
        return os;
    } else if (brs.search(/openbsd/) != -1) {
        os[0] = "OpenBsd";
        try {
            os[1] = brs.match(/openbsd\s(\d(\.\d)*)*/)[1];
        } catch (e) { }
        return os;
    } else if ((brs.search(/macintosh/) != -1) || (brs.search(/mac\x5fpowerpc/) != -1)) {
        os[0] = "Mac-Classic";
        return os;
    } else if (brs.search(/os\/2/) != -1) {
        os[0] = "OS2";
        try {
            os[1] = brs.match(/warp\s((\d(\.\d)*)*)/)[1];
        } catch (e) { }
        return os;
    } else if (brs.search(/openvms/) != -1) {
        os[0] = "Open-Vms";
        try {
            os[1] = brs.match(/openvms\sv((\d(\.\d)*)*)/)[1];
        } catch (e) { }
        return os;
    } else if ((brs.search(/amigaos/) != -1) || (brs.search(/amiga/) != -1)) {
        os[0] = "AmigaOS";
        try {
            os[1] = brs.match(/amigaos\s?(\d(\.\d)*)*/)[1];
        } catch (e) { }
        return os;
    } else if (brs.search(/hurd/) != -1) {
        os[0] = "Hurd";
        return os;
    } else if (brs.search(/hp\-ux/) != -1) {
        os[0] = "Hpux";
        try {
            os[1] = brs.match(/hp\-ux\sb\.[\/\s]?(\d+([\._]\d)*)/)[1];
        } catch (e) { }
        return os;
    } else if ((brs.search(/unix/) != -1) || (brs.search(/x11/) != -1)) {
        os[0] = "Unix";
        return os;
    } else if (brs.search(/cygwin/) != -1) {
        os[0] = "CygWin";
        return os;
    } else if (brs.search(/java[\/\s]?(\d+([\._]\d)*)/) != -1) {
        os[0] = "Java";
        try {
            os[1] = brs.match(/java[\/\s]?(\d+([\._]\d)*)/)[1];
        } catch (e) { }
        return os;
    } else if (brs.search(/palmos/) != -1) {
        os[0] = "Palmos";
        return os;
    } else if (brs.search(/symbian\s?os\/(\d+([\._]\d)*)/) != -1) {
        os[0] = "Symbian";
        try {
            os[1] = brs.match(/symbian\s?os\/(\d+([\._]\d)*)/)[1];
        } catch (e) { }
        return os;
    } else {
        os[0] = "unknown";
        return os;
    }
}

jQuery.cookie = function (name, value, options) {
    if (typeof value != 'undefined') {
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString();
        }
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        if ($.browser.mozilla || $.browser.msie)
            document.cookie = [name, '=', value, expires, path, domain, secure].join('');
        else
            document.cookie = [name, '=', lzw_encode(value), expires, path, domain, secure].join('');
    } else {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    if ($.browser.mozilla || $.browser.msie)
                        cookieValue = cookie.substring(name.length + 1);
                    else
                        cookieValue = lzw_decode(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};

jQuery.extend({
    stringify: function stringify(obj) {
        if ("JSON" in window) {
            return JSON.stringify(obj);
        }
        var t = typeof (obj);
        if (t != "object" || obj === null) {
            // simple data type
            if (t == "string") obj = '"' + obj + '"';
            return String(obj);
        } else {
            // recurse array or object
            var n, v, json = [], arr = (obj && obj.constructor == Array);
            for (n in obj) {
                v = obj[n];
                t = typeof (v);
                if (obj.hasOwnProperty(n)) {
                    if (t == "string") {
                        v = '"' + v + '"';
                    } else if (t == "object" && v !== null) {
                        v = jQuery.stringify(v);
                    }
                    json.push((arr ? "" : '"' + n + '":') + String(v));
                }
            }
            return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
        }
    }
});

function validateInput(type, id, errmsgDivId, emptycheck, minLength, maxLength, minVal, maxVal, PCID) {
    var data = jQuery.trim($(id).val());
    var re = /[a-zA-Z\s]+$/;
    var phone = /^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1}){0,1}[8-9][1-9](\s){0,1}(\-){0,1}(\s){0,1}[0-9]{1}[0-9]{7}$/;
    var dtype = $(PCID).val();
    if ((dtype == "CGPA" || dtype == "percentage") && ((emptycheck == true && data == "") || (minLength != null && data.length < minLength) || (maxLength != null && data.length > maxLength) || (maxLength != null && data.length > maxLength) || (minVal != null && data < minVal) || (maxVal != null && data > maxVal))) {
        toggleErrClass("error", errmsgDivId, id);
        IsValidate = false;
    }
    else {
        switch (type) {
            case "String":
                if (!re.test(data)) {
                    toggleErrClass("error", errmsgDivId, id);
                    IsValidate = false;
                }
                else {
                    toggleErrClass("noerror", errmsgDivId, id);
                }
                break;
            case "AlphaNumerical":
                toggleErrClass("noerror", errmsgDivId, id);
                break;
            case "Number":
                if (data != "" && isNaN(data)) {
                    toggleErrClass("error", errmsgDivId, id);
                    IsValidate = false;
                }
                else {
                    toggleErrClass("noerror", errmsgDivId, id);
                }
                break;
            case "PercentageCGPA":
                if (data != "" && isNaN(data) || (dtype == "CGPA" && data < 0) || (dtype == "CGPA" && data > 10) || (dtype == "percentage" && data < 35) || (dtype == "percentage" && data > 100)) {
                    toggleErrClass("error", errmsgDivId, id);
                    IsValidate = false;
                }
                else {
                    toggleErrClass("noerror", errmsgDivId, id);
                }
                break;
            case "Mobile":
                if (!phone.test(data)) {
                    toggleErrClass("error", errmsgDivId, id);
                    IsValidate = false;
                }
                else {
                    toggleErrClass("noerror", errmsgDivId, id);
                }
                break;
            case "Email":
                if (data != "" && !/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i.test(data)) {
                    toggleErrClass("error", errmsgDivId, id);
                    IsValidate = false;
                }
                else {
                    toggleErrClass("noerror", errmsgDivId, id);
                }
                break;
        }
    }
}

function toggleErrClass(type, errorId, inputId) {
    if (type == "error") {
        $(errorId).removeClass("correct").addClass("wrong");
        $(inputId).addClass("errmsg");
    }
    else if (type == "clear") {
        $(errorId).removeClass("correct").removeClass("wrong");
        $(inputId).removeClass("errmsg");
    }
    else {
        $(inputId).removeClass("errmsg");
        $(errorId).removeClass("wrong").addClass("correct");
    }
}

String.prototype.stripTags = function () {
    return this.replace(/<([^>]+)>/g, '').replace(/[\t]{1,}/g, '').replace(/[\n]{1,}/g, '\n').replace(/&nbsp;/g, ' ').trimSpace();
};

String.prototype.trimSpace = function () {
    if (this != null)
        return this.replace(/(^\s*)|(\s*$)/g, "");
    else
        return null;
}


// LZW-compress a string
function lzw_encode(s) {
    if (s) {
        var dict = {};
        var data = (s + "").split("");
        var out = [];
        var currChar;
        var phrase = data[0];
        var code = 256;
        for (var i = 1; i < data.length; i++) {
            currChar = data[i];
            if (dict[phrase + currChar] != null) {
                phrase += currChar;
            }
            else {
                out.push(phrase.length > 1 ? dict[phrase] : phrase.charCodeAt(0));
                dict[phrase + currChar] = code;
                code++;
                phrase = currChar;
            }
        }
        out.push(phrase.length > 1 ? dict[phrase] : phrase.charCodeAt(0));
        for (var i = 0; i < out.length; i++) {
            out[i] = String.fromCharCode(out[i]);
        }
        return out.join("");
    }
    return "";
}

// Decompress an LZW-encoded string
function lzw_decode(s) {
    if (s) {
        var dict = {};
        var data = (s + "").split("");
        var currChar = data[0];
        var oldPhrase = currChar;
        var out = [currChar];
        var code = 256;
        var phrase;
        for (var i = 1; i < data.length; i++) {
            var currCode = data[i].charCodeAt(0);
            if (currCode < 256) {
                phrase = data[i];
            }
            else {
                phrase = dict[currCode] ? dict[currCode] : (oldPhrase + currChar);
            }
            out.push(phrase);
            currChar = phrase.charAt(0);
            dict[code] = oldPhrase + currChar;
            code++;
            oldPhrase = phrase;
        }
        return out.join("");
    }
    return "";
}

var Base64 = {
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    encode: function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;
        input = Base64._utf8_encode(input);
        while (i < input.length) {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);
            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;
            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }
            output = output +
			this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
			this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
        }
        return output;
    },
    decode: function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
        while (i < input.length) {
            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }
        }
        output = Base64._utf8_decode(output);
        return output;
    },
    _utf8_encode: function (string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    },
    _utf8_decode: function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;
        while (i < utftext.length) {
            c = utftext.charCodeAt(i);
            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }
        return string;
    }
}

function breakPoint() {
    asd;
}

function DateDiff(startDate, endDate) {
    if (endDate && startDate)
        return (endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24);
}
