﻿var serviceCallMethodNames = new Array();
var testCandidateIds = new Array();

function setServiceCallMethods() {
    var virtualPath = "";
    serviceCallMethodNames["GetCandidateCredentials"] = virtualPath + "JSONServices/JSONAssessmentManagementService.svc/GetCandidateCredentials";
    serviceCallMethodNames["GetLoginCredentialsByCandidateId"] = virtualPath + "JSONServices/JSONAssessmentManagementService.svc/GetLoginCredentialsByCandidateId";
}

function loadApplication() {
    testCandidateIds = [];
    var toBeDeducted = parseInt($("#header").height());
    toBeDeducted += parseInt($("#footer").height());
    $('#container').css("height", $(window).height() - toBeDeducted + (.25 * toBeDeducted) - (6 * $(window).height() / 100) + "px");
    if ($("#getCredentials").attr("disabled") == "disabled") {
        $("#getCredentials").button({ "disabled": true });
        $("#getCredentials_cand_id").button({ "disabled": true });
    } else {
        $("#getCredentials").button();
        $("#getCredentials_cand_id").button();
    }
    $("#cand_email").attr("disabled", false);
    showHideLoading("#loadingdiva", false);
}

function showHideLoading(id, flag) {
    if (flag)
        $(id).show();
    else
        $(id).hide();
}

function GetCandidateCredential() {

    $("#credentials_div").html("");
    var email = $.trim($("#cand_email").val());
    var tenantId = $("#hdnTenantId").val();
    if ($("#tdcand_id").css("display") != "none") {
        var cand_id = $("#cand_id").val();
        if (cand_id == "") {
            $("#msgCandId").html("Enter Your Candidate Id");
        } else if (!isValidInt(cand_id)) {
            $("#msgCandId").html("Enter a Valid Candidate Id");
        } else {
            cand_id = parseInt(cand_id);
            if ($.inArray(cand_id, testCandidateIds) != -1) {
                var candidates = new Array();
                var candidate = {
                    "Id": cand_id
                };
                candidates.push(candidate);
                var CandidateCollection = {
                    "Candidates": candidates,
                    "RecordCount": 1
                };

                var request = {
                    "CandidateCollection": CandidateCollection,
                    "IsException": false,
                    "IsFailure": false
                };
                var getCandidateCredentialsRequest = {
                    "CandidateId": cand_id,
                    "TenantId": tenantId
                };
                callAjaxService("GetLoginCredentialsByCandidateId", getCandidateCredentialsRequest, getTestByCandidateIdCallBack, errorHandler);
            } else {
                $("#msgCandId").html("No Candidate match for entered Candidate Id and Email");
                $("#getCredentials").attr({ 'href': 'javascript:GetCandidateCredential()' });
            }

            // getAllCandidatesCallBack(request);


        }
    }
    else if (email == "") {
        $("#msgEmail").html("Enter Your Email");
    } else if (!isValidEmailAddress(email)) {
        $("#msgEmail").html("Enter a Valid Email");
    } else {
        $("#getCredentials").attr({ 'href': 'javascript:void(0)' });
        $("#cand_email").attr({ 'onkeypress': 'javascript:void(0)' });
        showHideLoading("#loadingdiva", true);
        $("#msgEmail").html("");
        var quickSearch = {
            "Email": email
        }
        var smartSearchCriteria = {
            "QuickSearch": quickSearch
        }
        var getAllCandidatesRequest =
        {
            "PagingCriteria": null,
            "ListId": 0,
            "SmartSearchCriteria": smartSearchCriteria,
            "JsonCandidateFilter": 0,
            "TenantId": tenantId,
            "UserId": "0"
        };
        callAjaxService("GetCandidateCredentials", getAllCandidatesRequest, getTestByCandidateIdCallBack, errorHandler)
        $("#getCredentials").attr({ 'href': 'javascript:GetCandidateCredential()' });
    }
}

//function getAllCandidatesCallBack(response) {
//    showHideLoading("#loadingdiva", false);
//    $("#getCredentials").attr({ 'href': 'javascript:GetCandidateCredential()' });
//    $("#cand_email").attr({ 'onkeypress': 'return quickGetCandidateCredential(event)' });
//    var tenantId = $("#hdnTenantId").val();
//    if (response != null && response.IsException == true || response.IsFailure == true) {
//        alert("Server Busy Unable to fetch test");
//    }
//    else if (response.CandidateCollection && response.CandidateCollection && response.CandidateCollection != null) {
//        if (!response.CandidateCollection || response.CandidateCollection.Candidates.length == 0 || response.CandidateCollection.RecordCount == 0) {
//            $("#msgEmail").html("No Candidate active for your email");
//        } else if (response.CandidateCollection.RecordCount > 1) {
//            $("#msgEmail").html("More than one candidate is active for your email, please enter Candidate Id");
//            $("#tdcand_id").css("display", "");
//            $("#cand_email").attr("disabled", true);
//            $("#getCredentials").css('display', 'none');
//            $("#getCredentials_cand_id").css('display', '');
//            for (var i = 0; i < response.CandidateCollection.Candidates.length; i++) {
//                testCandidateIds.push(response.CandidateCollection.Candidates[i].Id);
//            }
//        } else if (response.CandidateCollection.Candidates && response.CandidateCollection.Candidates.length > 0) {
//            var getTestByCandidateIdRequest =
//            {
//                "CandidateId": response.CandidateCollection.Candidates[0].Id,
//                "TenantId": tenantId
//            };
//            showHideLoading("#loadingdiva", true);
//            callAjaxService("GetLoginCredentialsByCandidateId", getTestByCandidateIdRequest, getTestByCandidateIdCallBack, errorHandler)
//        }
//    }
//}

function getTestByCandidateIdCallBack(response) {
    $("#getCredentials").attr({ 'href': 'javascript:GetCandidateCredential()' });
    showHideLoading("#loadingdiva", false);
    if (response != null && response.IsException == true || response.IsFailure == true) {
        alert("Server Busy Unable to fetch test");
    }
    else {
        if (response.TestUserLoginCredentialCollection && response.TestUserLoginCredentialCollection != null && response.TestUserLoginCredentialCollection.length > 0) {
            for (var i = 0; i < response.TestUserLoginCredentialCollection.length; i++) {
                if (response.TestUserLoginCredentialCollection[i].LoginId != null || response.TestUserLoginCredentialCollection[i].LoginId != undefined) {
                    var loginid = btoa(response.TestUserLoginCredentialCollection[i].LoginId);
                    var password = btoa(response.TestUserLoginCredentialCollection[i].Password);
                    var QueryString = "&a=" + loginid + "&b=" + password;


                    var test_div = $("<div id=credentials_div" + i + " style='padding-top:5px'/>")
                    test_div.append($("<fieldset class='ui-corner-all'/>").append("<legend><b>Test Name : " + response.TestUserLoginCredentialCollection[i].TestName + "</b></legend>")
                                                .append("<span style='padding-left:12px'>Login Id : </span><b>" + response.TestUserLoginCredentialCollection[i].LoginId + "</b><br>")
                                                .append("Password : <b>" + response.TestUserLoginCredentialCollection[i].Password + "</b><br>")
                                                .append("<span style='padding-left:33px'>Link : </span><b> <a href='" + location.href.toLowerCase().replace("candidatetestcredentials.aspx", "Default.aspx") + QueryString + "'target='_blank'>" + location.href.toLowerCase().replace("candidatetestcredentials.aspx", "Default.aspx") + "</b></a> ")
                    );
                    $("#credentials_div").append(test_div);
                }
            }
            if ($("#tdcand_id").css("display") != "none") {
                $("#msgCandId").html("");
            } else {
                $("#msgEmail").html("");
            }
        }
    }
    if (response.IsMultipleCandidates == true) {
        $("#msgEmail").html(response.Message);
        $("#tdcand_id").css("display", "");
        $("#cand_email").attr("disabled", true);
        $("#getCredentials").css('display', 'none');
        $("#getCredentials_cand_id").css('display', '');
        if (response.CandidateArray.length > 1) {
            for (var i = 0; i < response.CandidateArray.length; i++) {
                testCandidateIds.push(response.CandidateArray[i]);
            }
        }
    }
    else if (response.IsMultipleCandidates == false) {
        $("#msgEmail").html(response.Message);
    }
    //        else {
    //            if ($("#tdcand_id").css("display") != "none") {
    //                $("#msgCandId").html("No Test activate on your Candidate Id");
    //            } else {
    //                $("#msgEmail").html("No Test active for your Email");
    //            }
    //        }

    var toBeDeducted = parseInt($("#header").height());
    toBeDeducted += parseInt($("#footer").height());
    $('#container').css("height", $(window).height() - toBeDeducted + (.25 * toBeDeducted) - (9 * $(window).height() / 100) + "px");
}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/);
    return pattern.test(emailAddress);
};

function isValidInt(integer) {
    var pattern = new RegExp(/^\d*$/);
    return pattern.test(integer);
};

function callAjaxService(serviceCallMethodName, request, successHandler, errorHandler, dataType) {
    var returnDataType = "json";
    if (dataType) {
        returnDataType = dataType;
    }
    var ajaxValue = {
        "type": "POST",
        "url": serviceCallMethodNames[serviceCallMethodName],
        "data": JSON.stringify(request),
        "contentType": "application/json; charset=utf-8",
        "dataType": returnDataType,
        "success": successHandler,
        "error": errorHandler,
        "complete": function (XMLHttpRequest, textStatus) {
            this; // the options for this ajax request
        }
    }
    if ($.ajax) {
        $.ajax(ajaxValue);
    } else {
        jQuery.ajax(ajaxValue);
    }
}

function errorHandler(response) {
    alert(response.Message);
}

function quickGetCandidateCredential(e) {
    var keynum;
    var keychar;
    var numcheck;
    if (window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }
    if (keynum == 13) {
        GetCandidateCredential();
        return false;
    }
    else if (keynum == 27) {
        $("#cand_email").val("");
    }
}