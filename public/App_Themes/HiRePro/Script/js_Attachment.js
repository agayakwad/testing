function deleteFile(controlId, fileToDelete)
{
    var control = document.getElementById(controlId);
    var tempArray = new Array();
    var tempArray1 = new Array();
    
    if(control != null)
    {
        var files = control.getAttribute("uploadedFilePaths");
        var filesWithoutVirtualDir = control.getAttribute("uploadedFilePathsWithoutVirtualDir");
        if(files != null)
        {
            // Next 11 lines of code are done to eliminate a weired problem... I know this doesn't make any sense.
             try
            {
                files = files.split(",");
            }
            catch(e){}
             try
            {
                filesWithoutVirtualDir = filesWithoutVirtualDir.split(",");
            }
            catch(e){}
            var n23 = files.length;
            for(var i=0; i<n23;i++)
            {
                tempArray[i] = files[i];
                tempArray1[i] = filesWithoutVirtualDir[i];
            }
            var n24 = tempArray.length;
            for(var i=0; i<n24;i++)
            {
                if(tempArray[i] == fileToDelete)
                {
                    tempArray.splice(i,1);
                    tempArray1.splice(i,1);
                    control.setAttribute("uploadedFilePaths", tempArray);
                    control.setAttribute("uploadedFilePathsWithoutVirtualDir", tempArray1);
                    var anchors = control.getElementsByTagName("a");
                    if (anchors != null)
                    {
                        var n25 = anchors.length;
                        for(j=0;j<n25;j++)
                        {
                            var onclick = anchors[j].attributes["onclick"].value;
                            if(onclick.indexOf(fileToDelete) > -1)
                            {
                                anchors[j].onclick = "";
                                anchors[j].disabled = true;
                                anchors[j].style.cursor = "none";
                                anchors[j].style.textDecoration = "line-through";
                                anchors[j+1].onclick = "";
                                anchors[j+1].disabled = true;
                                anchors[j+1].style.cursor = "none";
                                anchors[j+1].style.textDecoration = "line-through";
                                break;
                            }
                        }
                    }
                    break;
                }
            }
        }
    }
}

function allowDeletion(controlId, flag)
{
    var control = document.getElementById(controlId);
    if(control != null && flag == false)
    {
        var anchors = control.getElementsByTagName("a");
        if(anchors != null)
        {
            var n26 = anchors.length;
            for(j=0;j<n26;j++)
            {
                var onclick = anchors[j].attributes["onclick"].value;
                if(onclick.indexOf("deleteFile(") > -1)
                {
                    anchors[j].onclick = "";
                    anchors[j].disabled = true;
                    anchors[j].style.cursor = "none";
                }
            }
        }
    }
}

function clearAttributeOfControl(controlId, attributeName)
{
    var control = document.getElementById(controlId);
    if(control != null && attributeName != null && attributeName.length > 0)
    {
        control.setAttribute(attributeName, null);
    }
}
