// For Candidate QuichSearch

function quickSearchCandidates()
{
    var fulltext = "\"" + $get(GetClientId("txtBoxFullTextSearch")).value + "\"";
    var quickSearch = "" +
    "{" +
        "\"FullTextSearch\""  +   " : " + fulltext +
    "}";

     var searchCriteria = "" +
    "{" +
        "\"QuickSearch\""  +   " : " + quickSearch +
    "}";
    return searchCriteria;
}

function quickSearchCandidatesCriteria()
{
    var fulltext = "\"" + $get(GetClientId("txtBoxFullTextSearch")).value + "\"";
    var quickSearch = 
    {
        "FullTextSearch":fulltext
    }

    var searchCriteria =
    {
        "QuickSearch":quickSearch,
        "PagingCriteria":null
    }
    return searchCriteria;
}

function quickSearchCandidateKeyPress(e)
{
    var keynum;
    var keychar;
    var numcheck;
    if(window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if(e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }
    if(keynum == 13)
    {
        CallGetGridSrv('Candidate', '43',false,false,false,false);
    }
}

function callGridConstructForQuickCandidateSearch()
{
    var fulltext = $get("txtBoxFullTextSearch").value;
    if(fulltext.length > 2)
    {
        constructJavascriptGrid('Candidate', "QuickSearch", 1, false)
    }
    else
    {
        alert("Please enter atleast 3 characters to search!");
    }
    return false;
}

function getListCriteria()
{
    var listName = "";
    if($get("txtBoxSmartListName") != null)
    {
        listName = $get("txtBoxSmartListName").value;
    }
    var list =
    {
        "ListName":listName,
        "IsSmart":false,
        "TargetType":3
    }
    return list;
}

function getListName()
{
    if($get("txtBoxListName") == null)
    {
        return "";
    }
    return $get("txtBoxListName").value;
}

function fillMoreActionDropDownList()
{
    if(GetClientId("moreActionsDropDownList") == null)
    {
        return;
    }
    var dropDownMoreAction = $get(GetClientId("moreActionsDropDownList"));
    dropDownMoreAction.innerHTML = "";
    var optionMoreAction = document.createElement("option");
    optionMoreAction.value = "_MoreActions";
    optionMoreAction.text = "More Actions";
    optionMoreAction.style.color = "#999999";
    dropDownMoreAction.options[dropDownMoreAction.options.length] = optionMoreAction;
    
    optionMoreAction = document.createElement("option");
    optionMoreAction.value = "_";
    optionMoreAction.text = "";
    optionMoreAction.style.color = "#999999";
    optionMoreAction.style.textAlign = "center";
    dropDownMoreAction.options[dropDownMoreAction.options.length] = optionMoreAction;
    
    var smartListRequest = 
    {
        "IsSmart":true,
        "TargetType":3,
        "PageSize":100
    }
    hirepro.JSONServiceLayer.ServiceContracts.IJsonCommonManagementService.GetAllLists(smartListRequest, getTenantAdamInfo(), callBackGetAllSmartLists, callBackFail);
    
    var listRequest = 
    {
        "IsSmart":false,
        "TargetType":3,
        "PageSize":100
    }
    hirepro.JSONServiceLayer.ServiceContracts.IJsonCommonManagementService.GetAllLists(listRequest, getTenantAdamInfo(), callBackGetAllLists, callBackFail);
    
    ///TODO : For filling my job.. this has been temporarily commented.. Need some refactoring in service.
    var myJobListRequest =
    {
        "ObjectState":0,
        "IsSpecificToUser":true,
        "MaxResults":1000
    }
    hirepro.JSONServiceLayer.ServiceContracts.IJsonJobManagementService.GetAllJobNames(myJobListRequest,getTenantAdamInfo(), callBackGetAllMyJobLists, callBackFail);
    
    
    // For filling tags -- Don't Remove
    //hirepro.JSONServiceLayer.ServiceContracts.IJsonCommonManagementService.GetAllTags(100, getTenantAdamInfo(), callBackGetAllTags, callBackFail);
}

function callBackGetAllMyJobLists(response)
{
    if(response!=null && response.length>0)
    {
        var dropDownMoreAction = $get(GetClientId("moreActionsDropDownList"));
        var optionMoreAction = document.createElement("option");
        optionMoreAction.value = "_JobList";
        optionMoreAction.text = "   My Jobs";
        optionMoreAction.style.color = "#999999";
        dropDownMoreAction.options[dropDownMoreAction.options.length] = optionMoreAction;
        var n27 = response.length;
        for(var i=0;i<n27;i++)
        {
            var optionDropDown = document.createElement("option");
            optionDropDown.text = "       " + response[i].JobName;
            optionDropDown.value = "J" + response[i].JobId;
            optionDropDown.style.color = "#333333";
            dropDownMoreAction.options[dropDownMoreAction.options.length] = optionDropDown;
        }
    }
}

function callBackGetAllSmartLists(response)
{
    if(response != null)
    {
        // Add Smart Lists
        
        var dropDownMoreAction = $get(GetClientId("moreActionsDropDownList"));
        var optionMoreAction = document.createElement("option");
        optionMoreAction.value = "_SmartList";
        optionMoreAction.text = "   Smart List";
        optionMoreAction.style.color = "#999999";
        dropDownMoreAction.options[dropDownMoreAction.options.length] = optionMoreAction;
        var n29 = response.length;
        for(var i=0;i<n29;i++)
        {
            var optionDropDown = document.createElement("option");
            optionDropDown.text = "       " + response[i].ListName;
            optionDropDown.value = "S" + response[i].ListId;
            optionDropDown.style.color = "#333333";
            dropDownMoreAction.options[dropDownMoreAction.options.length] = optionDropDown;
        }
    }
}

function callBackGetAllLists(response)
{
    if(response != null)
    {
        // Add Lists
        
        var dropDownMoreAction = $get(GetClientId("moreActionsDropDownList"));
        var optionMoreAction = document.createElement("option");
        optionMoreAction.value = "_List";
        optionMoreAction.text = "   List";
        optionMoreAction.style.color = "#999999";
        dropDownMoreAction.options[dropDownMoreAction.options.length] = optionMoreAction;
        var n27 =response.length;
        for(var i=0;i<n27;i++)
        {
            var optionDropDown = document.createElement("option");
            optionDropDown.text = "       " + response[i].ListName;
            optionDropDown.value = "L" + response[i].ListId;
            optionDropDown.style.color = "#333333";
            dropDownMoreAction.options[dropDownMoreAction.options.length] = optionDropDown;
        }
    }
}

function callBackGetAllTags(response)
{
    if(response != null)
    {
        // Add Tags
        
        var dropDownMoreAction = $get(GetClientId("moreActionsDropDownList"));
        var optionMoreAction = document.createElement("option");
        optionMoreAction.value = "_Tag";
        optionMoreAction.text = "   Tag";
        optionMoreAction.style.color = "#999999";
        dropDownMoreAction.options[dropDownMoreAction.options.length] = optionMoreAction;
        var n28 = response.length;
        for(var i=0;i<n28;i++)
        {
            var optionDropDown = document.createElement("option");
            optionDropDown.text = "       " + response[i].Name;
            optionDropDown.value = "T" + response[i].Id;
            optionDropDown.style.color = "#333333";
            dropDownMoreAction.options[dropDownMoreAction.options.length] = optionDropDown;
        }
    }
}

function viewSelectedMoreAction()
{
    var dropDownMoreAction = $get(GetClientId("moreActionsDropDownList"));
    var selectedValue = dropDownMoreAction.options[dropDownMoreAction.selectedIndex].value;
    selectedMoreActionText = trim(dropDownMoreAction.options[dropDownMoreAction.selectedIndex].innerText);
    if(selectedValue.charAt(0) == "_")
    {
        alert("Please select correct action to view");
    }
    else
    {
        var type = selectedValue.charAt(0);
        var selectedId = selectedValue.substring(1, selectedValue.length);
        selectedMoreActionId = selectedId;
        switch(type)
        {
            case "S":  // Smart List
                //CallGetGridSrv('Candidate', '19',false,false,false,false);
                constructJavascriptGrid("Candidate", "SmartList", 1, false);
            break;
            case "L":  // List
                //CallGetGridSrv('Candidate', '20',false,false,false,false);
                constructJavascriptGrid("Candidate", "List", 1, false);
            break;
            case "T":  // Tags
                CallGetGridSrv('Common', '13',false,false,false,false);
            break;
            case "J":  //My Jobs
                constructJavascriptGrid("Candidate", "JobList", 1, false);
                //CallGetGridSrv('Candidate', '24',false,false,false,false);
            break;
            
        }
    }
}

var selectedMoreActionId = 0;
function getSelectedMoreActionValue()
{
    return selectedMoreActionId;
}
var selectedMoreActionText = "";
function getSelectedMoreActionText()
{
    var clientId = GetClientId("moreActionsDropDownList");
    if(clientId == null)
    {
        return "";
    }
    var dropDownMoreAction = $get(clientId);
    if(dropDownMoreAction != null && dropDownMoreAction.selectedIndex > -1)
    {
        var selectedValue = dropDownMoreAction.options[dropDownMoreAction.selectedIndex].value;
        var selectedMoreActionText = trim(dropDownMoreAction.options[dropDownMoreAction.selectedIndex].innerText);
        if(selectedValue.charAt(0) == "_")
        {
            return "";        
        }
        return trim(dropDownMoreAction.options[dropDownMoreAction.selectedIndex].innerText);
    }
    return "";
}

var catalogMatchingid =0;
var myjobDropDownSelectedValue =0;
function addSelectedItemsToSelectedList()
{
    var dropDownMoreAction = $get(GetClientId("moreActionsDropDownList"));
    var selectedValue = dropDownMoreAction.options[dropDownMoreAction.selectedIndex].value;
    if(selectedValue.charAt(0) == "_")
    {
        alert("Please select correct action!");
    }
    else
    {
        var type = selectedValue.charAt(0);
        var selectedId = selectedValue.substring(1, selectedValue.length);
        selectedMoreActionId = selectedId;
        var jsonList = getList(trim(dropDownMoreAction.options[dropDownMoreAction.selectedIndex].text));
        if(jsonList != null)
        {
           
            switch(type)
            {
                case "S":  // Smart List
                    alert("Can't add to a smart list!");
                break;
                case "L":  // List
                    jsonList.ListId = selectedId;
                    enableLoading();
                    hirepro.JSONServiceLayer.ServiceContracts.IJsonCommonManagementService.UpdateList(jsonList, getTenantAdamInfo(), callBackUpdateList, callBackFail);
                break;
                case "J": //My Jobs
                         enableLoading();
                         myjobDropDownSelectedValue = selectedId;
                         var catalogNames = new Array("ResumeStatus");
                          if(catalogNames.length > 0)
                          {  
                              hirepro.JSONServiceLayer.ServiceContracts.IJsonCommonManagementService.GetCatalogValues(catalogNames, getTenantAdamInfo(), callBackFillApplicantDdlWithCatalog,callBackFail);
                          }
                           
                        
                    break;
                case "T":  // Tags
                    alert("Please select correct action!");
                break;
            }
        }
        else
        {
            alert("Please Select an Item to Add!");
        }
    }
}

function CallBackApplyForCandidates(result)
{
    disableLoading();
    if(result!=null)
    {
         if(result.ApplicantsCreated>0)
         {
           alert("Applicant status created successfully");
         }
         else if (result.ApplicatantsNotCreated>0)
         alert("Applicant status for the candidate cannot be created");
    }
}

function CallBackApplyForCandidate(result)
{
     disableLoading();
     if(result>0)
     {
        alert("Applicant status created successfully");
     }
     else
     {
        alert("Applicant status for the candidate cannot be created");
     }
}

function callBackFillApplicantDdlWithCatalog(response)
{
    if (response != null && response.length > 0) 
    {
        var n29 = response.length; 
        for(var i=0;i<n29;i++)
        {
            switch(response[i].Name)
            {
                case "ResumeStatus":
                    if (response[i].CatalogValues != null) 
                    {
                        var n30 = response[i].CatalogValues.length;
                        for (var j = 0; j < n30; j++) {
                            if (response[i].CatalogValues[j].Name == "Matching") {
                                catalogMatchingid = response[i].CatalogValues[j].CatalogValueId;
                                break;
                            }
                        }
                    }

                    break;
            }
        }
    }
    
     var JsonApplicantStatusItem =
                            {
                                "CandidateIds":getSelectedRows("ObjectId"),
                                "JobId":myjobDropDownSelectedValue,
                                "StatusId":catalogMatchingid,
                                "AttachmentCollection":null
                            }
    if(getSelectedRows("ObjectId").length>1)
    {
      hirepro.JSONServiceLayer.ServiceContracts.IJsonJobManagementService.ApplyForCandidates(JsonApplicantStatusItem, getTenantAdamInfo(), CallBackApplyForCandidates,callBackFail);
    }
    else
    {
        hirepro.JSONServiceLayer.ServiceContracts.IJsonJobManagementService.ApplyForCandidate(JsonApplicantStatusItem, getTenantAdamInfo(), CallBackApplyForCandidate,callBackFail);
    }
}            

function callBackUpdateList(response)
{
    disableLoading();
    if(response != null)
    {
        alert(response.Message);
    }
}

function getList(listName)
{
    var selectedRows = getSelectedRows("ObjectId");
    var listItems = new Array();
    if (selectedRows != null && selectedRows.length > 0) 
    {
        var n31 = selectedRows.length;
        for(var i=0;i<n31;i++)
        {
            var listItem = 
            {
                "ItemId":selectedRows[i],
                "TargetItem":3
            }
            listItems[listItems.length] =  listItem;
        }
    }
    else
    {
        return null;
    }
    var list =
    {
        "ListName":listName,
        "ListItems":listItems,
        "IsSmart":false,
        "ListId":0,
        "TargetType":3
    }
    return list;
}

function deleteSelectedMoreAction()
{
    var dropDownMoreAction = $get(GetClientId("moreActionsDropDownList"));
    var selectedValue = dropDownMoreAction.options[dropDownMoreAction.selectedIndex].value;
    if(dropDownMoreAction != null)
    {
        if(selectedValue.charAt(0) == "_")
        {
            alert("Please select correct action to delete!");
        }
        else
        {
            var type = selectedValue.charAt(0);
            var selectedId = selectedValue.substring(1, selectedValue.length);
            var listIds = new Array();
            listIds[listIds.length] = selectedId;
            switch(type)
            {
                case "S":  // Smart List
                case "L":  // List
                    enableLoading();
                    hirepro.JSONServiceLayer.ServiceContracts.IJsonCommonManagementService.DeleteList(listIds, getTenantAdamInfo(), callBackDeleteList, callBackFail);
                break;
                case "J":
                    alert("Cannot Delete My Jobs");
                break;
                case "T":  // Tags
                    alert("Please select correct action!");
                break;
            }
        }
    }
}

function callBackDeleteList(response)
{
    disableLoading();
    if(response != null)
    {
        if(response.IsException == false && response.IsFailure == false)
        {
            fillMoreActionDropDownList();
        }
        alert(response.Message);
    }
}

function removeItemsFromSelectedMoreAction()
{
    var dropDownMoreAction = $get(GetClientId("moreActionsDropDownList"));
    var selectedValue = dropDownMoreAction.options[dropDownMoreAction.selectedIndex].value;
    if(selectedValue.charAt(0) == "_")
    {
        alert("Please select correct action!");
    }
    else
    {
        var type = selectedValue.charAt(0);
        var selectedId = selectedValue.substring(1, selectedValue.length);
        selectedMoreActionId = selectedId;
        var jsonList = getList(trim(dropDownMoreAction.options[dropDownMoreAction.selectedIndex].text));
        if(jsonList != null)
        {
            if (jsonList.ListItems != null) 
            {
                var n32 = jsonList.ListItems.length;
               for(var i=0;i<n32;i++)
               {
                    jsonList.ListItems[i].Deleted = true;  
               }
            }
            switch(type)
            {
                case "L":  // List
                    jsonList.ListId = selectedId;
                    enableLoading();
                    hirepro.JSONServiceLayer.ServiceContracts.IJsonCommonManagementService.UpdateList(jsonList, getTenantAdamInfo(), callBackUpdateList, callBackFail);
                break;
                default:
                    alert("Please select correct action!");
                break;
            }
        }
        else
        {
            alert("Please select some items to remove!");
        }
    }
}
