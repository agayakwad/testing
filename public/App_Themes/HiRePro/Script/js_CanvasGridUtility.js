﻿
/*

    CallBack Methods

*/

function actionOnGridItemsCallBack(retVal, context)
{
    
}

function gridPageChangeCallBack(retVal, context)
{
    document.getElementById("ctl00_gridDiv").innerHTML = retVal;
}


/*

    calling script for handling canvas grid page change requests

*/


function GetGridOnPageChange()
{
    alert('GetGridOnPageChange');
    gridName = document.getElementById("CurrentGridName").Value;
    pageNumber = document.getElementById("ctl00$ctl10$TopPageTracker")[document.getElementById("ctl00$ctl10$TopPageTracker").options.selectedIndex].value;
    gridMode = document.getElementById("CurrentGridMode").Value;
    
    document.getElementById("CurrentGridName").Value = gridName;
    document.getElementById("CurrentGridMode").Value = gridMode;
    
    var jsonGridRequestString = "" +
    "{" +
        "\"GridName\""       +   " : " + "\"" + gridName + "\"" + "," +
        "\"CurrentPage\""    +   " : " + pageNumber + "," +
        "\"GridMode\""       +   " : " + gridMode + "," +
        "\"IsGridModeSet\""  +   " : " + false + "," +
        "\"IsListIdSet\""    +   " : " + false + "," +
        "\"Refresh\""        +   " : " + false + "," +
        "\"ListId\""         +   " : " + 0 + 
    "}" ;

    onGridPageChangeRequest(jsonGridRequestString, 'true');
}

function TakeActionOnGridItems(verb)
{
    onActionOnGridItemsRequest(verb)
}

function moveFirst()
{
    var selectedPageIndex = parseInt(document.getElementById("ctl00$ctl10$TopPageTracker").options.selectedIndex);
    if ( selectedPageIndex == 0 )
    {
        alert("You are already in the first page");
    }
    else if ( selectedPageIndex > 0 )
    {    
        document.getElementById("ctl00$ctl10$TopPageTracker").options.selectedIndex = "0";
        GetGridOnPageChange();
    }
}

function moveLast()
{
    var selectedPageIndex = parseInt(document.getElementById("ctl00$ctl10$TopPageTracker").options.selectedIndex);
    var lastPageIndex = parseInt(document.getElementById("ctl00$ctl10$TopPageTracker").options.length) - 1;
    if ( selectedPageIndex == lastPageIndex )
    {
        alert("You are already in the last page");
    }
    else if ( selectedPageIndex < lastPageIndex )
    {    
        document.getElementById("ctl00$ctl10$TopPageTracker").options.selectedIndex = lastPageIndex;
        GetGridOnPageChange();
    }
}

function moveNext()
{
    var totalPage = document.getElementById("ctl00$ctl10$TopPageTracker").options.length;
    var currentPage = parseInt(document.getElementById("ctl00$ctl10$TopPageTracker")[document.getElementById("ctl00$ctl10$TopPageTracker").options.selectedIndex].value);
    if ( currentPage < totalPage )
    {
        
        document.getElementById("ctl00$ctl10$TopPageTracker").options.selectedIndex = document.getElementById("ctl00$ctl10$TopPageTracker").options.selectedIndex + 1;
        GetGridOnPageChange();
    }
    else if ( currentPage == totalPage )
    {
        alert("You already in the last page");
    }
    
}

function movePrev()
{
    var currentPage = parseInt(document.getElementById("ctl00$ctl10$TopPageTracker")[document.getElementById("ctl00$ctl10$TopPageTracker").options.selectedIndex].value);
    if ( currentPage > 1 )
    {
        document.getElementById("ctl00$ctl10$TopPageTracker").options.selectedIndex = document.getElementById("ctl00$ctl10$TopPageTracker").options.selectedIndex - 1;
        GetGridOnPageChange();
    }
    else if ( currentPage == 1 )
    {
        alert("You already in the first page");
    }
}
