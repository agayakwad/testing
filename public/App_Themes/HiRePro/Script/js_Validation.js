
// Default Operational Values, Modify these to change default actions of
// the data validation library.

// This is the rule to use to validate a value if none is specified.
defaultValidationRule	= "free";

// This is the first part of the error message, set to blank to just show a list
// of errors.
defaultErrorPrefix	= "This form cannot be submitted with the following errors;\n";

// [ELEMENT] is replaced with the field name when displayed.
defaultErrorMessage	= "The value in the '[ELEMENT]' field is invalid.";

var MSGTIMER = 20;
var MSGSPEED = 5;
var MSGOFFSET = 3;
var MSGHIDE = 3;
var _isValidate = true;

//var alpha = 0;

// ==============================================================================
//							   ACTUAL VALIDATION CODE
// ==============================================================================

// Actual code for determining and then applying a regular expression
// to the designated form element. Returns true if the pattern matches,
// false if it doesn't.
function validateField(field_name, validation_code) {
	// This is a switch used for reversing the desired regular
	// expression result.
	// Set to "true" if matching a regexp successfully should
	// result in a failure.
	invertResult = false;
  
	// Check if they supplied a custom RegExp or just a rule name
	if (validation_code.charAt(0) != "/") {
		// These are the pre-defined rules available, you can
		// edit this list by using one as an example.
		switch (validation_code.toLowerCase()) {
			case "txt" : 			// Text only allowed
				re = /^[a-z ]*$/i;
				break;
	    	case "tenant" :
				re =/^[a-zA-Z ]*[0-9 ]*[a-zA-Z]*$/;
				break;
			case "name" :
				re =/^[a-zA-Z ]*[0-9]*[a-zA-Z ]*$/;
				break;
		   case "UserName" :
				re =/^[a-zA-Z ]*$/;
				break;
	    	case "experience" :
				re = /^[0-9]{0,2}$|^[0-9]{0,2}[\.]{1}[0-9]{0,2}$/;
				break;
			case "num" :			// Only numbers (allow decimal)
				re = /^[\d\.]*$/;
				break;
			case "float" :	
				re = /^[0-9]{0,3}$|^[0-9]{0,3}[\.]{1}[0-9]{0,2}$/;
				break;
			case "int" :			// Integers only
				re = /^\d*$/;
				break;
			case "phone" :			// Phone number characters
				re = /^[\d \-\(\)\+]*$/;
				break;
			case "mobile" :			// Mobile number characters
				re = /^[0-9]{6,15}$/;
				break;
			case "date" :			// Well-formed Dates
				re = /^[0-9]{1,2}(\/)[0-9]{1,2}(\/)[0-9]{1,4}[ ][0-9]{1,2}(:)[0-9]{1,2}$/; ///^[0-9]{1,2}(-)[A-Z]{1}[a-z ]{2}(-)[0-9]{4}[ ][0-9]{1,2}(:)[0-9]{2}(:)[0-9]{2}$/; ///^(()|(\d{1,2}(\/|-)\d{1,2}(\/|-)\d{2,4}))$/;
				break;
			case "email" :			// Something valid-ish for an email
				re = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
				break;
			case "curr" :			// Currency (using "$", "," and ".")
				re = /^\$?\d*,?\.?\d{0,2}$/i;
				break;
			case "alnum" :			// Alpha-numeric characters
				re = /^[\w ]*$/i;
				break;
			
			case "addr" :			// Base Address rule
				re = /^[\w- \.,#]*$/gi;
				break;
			case "name" :			// Good for validating names
				re = /^[a-z,\-\. ]*$/gi;
				break;			
			case "multiemail":
			    re = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$|^[\w]{0}$/;
			    break;
			case "nobadsql" :		// Denies SQL which could be harmful
				re = /((delete|drop|update|replace|kill|lock) )/gi;
				invertResult = true;
				break;
			case "notnull" :		// Requires *anything*
				re = /.+/;
				break;
			case "password" :
				re = /^[\w . @ ! # $ % ^ & *]{6,20}$/;
				break;
		    case "nonzerono" :      //Non Zero float
				re =/^[1-9]*$|^[0,1-9]{0,3}[\.]{1}[1-9]{0,2}$|^[0,1-9]{2,4}$/;
				break;
		   case "newdate" :
			    re = /^[0-9]{1,2}[-][a-zA-z]{3}[-][0-9]{4}[ ][0-9]{1,2}[:][0-9]{1,2}[:][0-9]{1,2}$/;
				break;
		   case "actionname":
		        re=/^[a-zA-Z]*[0-9]*$/;
		        break;				
			default :			// Default - See "free"
				re = /.+/;
				break;
		}
	}
	
	// This means they specified a RegExp of their own, which should be
	// in the form "/<RegExp>/" and needs to be "eval"ed before using.
	else 
	{
		re = eval(validation_code);
	}
	
	// Do the actual regular expression testing against the string
	// and return the result.
	if (re.test(field_name.value)) 
	{
	    return true;
	}
	else 
	{
        return false;
	}
}


function validateControls(control_list){
_isValidate = true;
if(defaultErrorPrefix!= null ){
masterErrorMsg = defaultErrorPrefix;
}
	checkedFields = new Array('', '', '');
		
	// Loop through every element in the form specified.
	for (var i=0; i<control_list.length; i++) {
		rule = "";
		msg  = "";
		form_element = document.getElementById(control_list[i]);// document[form_name][form_element];

		// Ensure the form element exists
		if (form_element) {
			// Make sure the field is supposed to be validated.
			if (form_element.validate && !in_array("+" + form_element.id, checkedFields) ) {
				// add this field into the "checkedFields" array to avoid
				// checking it again.
				
				checkedFields[checkedFields.length] = "+" + form_element.id;
				
				// Set default validation rule if none is specified
				if (!form_element.rule) {
					//rule = defaultValidationRule;
				}
				else {
					rule = form_element.rule;
				}
				
				// And set the default error message
				// Replaces [ELEMENT] with the field name in the default
				// error message string if it is used.
				if (!form_element.error) {
					error = defaultErrorMessage.replace('\[ELEMENT\]', form_element.name);
				}
				else {
					error = form_element.error;
				}
				
				// If the validation fails, add this error message
				// to the running message.
				if(form_element.required == "true")
				{
				    var errorDisplayControl = document.getElementById(form_element.errorcontrolid);
				    var msgControl = document.getElementById(form_element.msgcontrolid);
				    errorDisplayControl.style.color = "Red";
					errorDisplayControl.style.fontSize = 10 + "px";
				    if( validateField(form_element, "notnull" ))
				    {
				        if (rule != "" && !validateField(form_element, rule) || (form_element.required && form_element.value == "")) 
				        {
					        errorDisplayControl.innerHTML = error;
					        inlineMsg(msgControl,errorDisplayControl,error,2);
					        _isValidate = false;
					        alpha = 0;
					        return _isValidate;
				        }
				    }
				    else
				    {
					    inlineMsg(msgControl,errorDisplayControl,form_element.notnullerror,2);
					     _isValidate = false;
					     alpha = 0;
					        return _isValidate;
				    }
				 

				}
				else
				{
				    var errorDisplayControl = document.getElementById(form_element.errorcontrolid);
				    var msgControl = document.getElementById(form_element.msgcontrolid);
				    if(errorDisplayControl && msgControl ){
				        errorDisplayControl.style.color = "Red";
					    errorDisplayControl.style.fontSize = 10 + "px";
    					
				        if( validateField(form_element, "notnull" ))
				        {
				            if (rule != "" && !validateField(form_element, rule)) 
				            {
					            errorDisplayControl.innerHTML = error;
					            inlineMsg(msgControl,errorDisplayControl,error,2);
					             _isValidate = false;
					             alpha = 0;
					            return _isValidate;
    					       
				            }
				            else
				            {
				            }
				        }
				    }
				}
			}
		}
		
    }
    return _isValidate;
}
//for Compare Ranges (Exprience,Salary,Etc)
function oncompare(compareto,comparefrom,compareType)
{ 

    if(compareto.compareType !=null && compareto.compareType!="")
    {
        if ( compareType == "Eq" ) 
        {
            if ( compareto.value == comparefrom.value)
                return true;
            else
                return false;
        }
        else if(compareType == "Ge" )
        {
            if ( compareto.value > comparefrom.value)
                return true;
            else
                return false;
        }    
        else if (compareType == "Le" )
        {
            if ( compareto.value < comparefrom.value)
                return true;
            else
                return false;
        }        
    }
}

function datavalidation(single_element)
{
   rule = "";
   
   
   try
   {
   if ( single_element.length != null && single_element.length > 0 )
          single_element = single_element[0];               
        
   if ( !single_element )
   {
      single_element = document.getElementById(single_element);
   }
   
   else
   {
     rule = single_element.rule;
   }
   
var comparelike;
var compareto;
var comparefrom;
    compareto = document.getElementById(single_element.id);
    comparefrom = document .getElementById (single_element.comparecontrolid);
    comparelike= single_element .compareType;
   if (!single_element.error)
   {
	  error = defaultErrorMessage.replace('\[ELEMENT\]', single_element.name);
   }
   else 
   {
	  error = single_element.error;
	  
   }
   
   if(single_element)
   {
      var errorDisplayControl;
      var msgControl;
      errorDisplayControl = document.getElementById(single_element.errorcontrolid);
      msgControl = document.getElementById(single_element.msgcontrolid);
      
      if ( errorDisplayControl && msgControl )
      {
         errorDisplayControl.style.color = "Red";
		 errorDisplayControl.style.fontSize = 10 + "px";
				    
	     if(single_element.required == "true")
		 {
		     if( validateField(single_element, "notnull" ))
		     {
		         if (rule != "" && !validateField(single_element, rule))
		         {
		            errorDisplayControl.innerHTML = error;
			        inlineMsg(msgControl,errorDisplayControl,error,2);
			        _isValidate = false;
			        //single_element.focus();
			        return _isValidate;
		         }
                 if(oncompare(compareto,comparefrom,comparelike))
		         {
		            errorDisplayControl.innerHTML =  single_element.errorcomparecontrol;
			        inlineMsg(msgControl,errorDisplayControl,single_element.errorcomparecontrol,2);
			        _isValidate = false;
			        //single_element.focus();
			        return _isValidate;
		         }
		         
		         
		      }
		      else
		      {
			      inlineMsg(msgControl,errorDisplayControl,single_element.notnullerror,2);
			      _isValidate = false;
			       //single_element.focus();
			      return _isValidate;
		      }
		  }
		  else
		  {
		        if ( single_element.validate == "true" )
		        {
		            if (rule != "" && !validateField(single_element, rule))
		            {
		                errorDisplayControl.innerHTML = error;
			            inlineMsg(msgControl,errorDisplayControl,error,2);
			            _isValidate = false;
			            //single_element.focus();
			            return _isValidate;
		            }
		            
		            if(oncompare(compareto,comparefrom,comparelike))
		            {
		                errorDisplayControl.innerHTML =  single_element.errorcomparecontrol;
			            inlineMsg(msgControl,errorDisplayControl,single_element.errorcomparecontrol,2);
			            _isValidate = false;
			            //single_element.focus();
			            return _isValidate;
		            }
		       }
		   }
	    }
     }
     }
     catch(ex)
     {
     }
}




// This is the mainline function which needs to be called on form submission to
// check required fields
function validateForm(form_name) {
	masterErrorMsg = defaultErrorPrefix;
	checkedFields = new Array('', '', '');
		
	// Loop through every element in the form specified.
	for (form_element in document[form_name].elements) {
		rule = "";
		msg  = "";
		form_element = document[form_name][form_element];

		// Ensure the form element exists
		if (form_element) {
			// Make sure the field is supposed to be validated.
			if (form_element.validate && !in_array("+" + form_element.name, checkedFields)) {
				// add this field into the "checkedFields" array to avoid
				// checking it again.
				checkedFields[checkedFields.length] = "+" + form_element.name;
				
				// Set default validation rule if none is specified
				if (!form_element.rule) {
					//rule = defaultValidationRule;
				}
				else {
					rule = form_element.rule;
				}
				
				// And set the default error message
				// Replaces [ELEMENT] with the field name in the default
				// error message string if it is used.
				if (!form_element.error) {
					error = defaultErrorMessage.replace('\[ELEMENT\]', form_element.name);
				}
				else {
					error = form_element.error;
				}
				
				// If the validation fails, add this error message
				// to the running message.
				if(form_element.required == "true")
				{
				    var errorDisplayControl = document.getElementById(form_element.errorcontrolid);
				    if(errorDisplayControl){
				        errorDisplayControl.style.color = "Red";
					    errorDisplayControl.style.fontSize = 10 + "px";
				        if( validateField(form_element, "notnull" ))
				        {
				            if (rule != "" && !validateField(form_element, rule) || (form_element.required && form_element.value == "")) 
				            {
					            masterErrorMsg += " - " + error + "\n";
					            errorDisplayControl.innerHTML = error;
				            }
				            else
				            {
				                errorDisplayControl.innerHTML = "";
				            }
				        }
				        else
				        {
					        errorDisplayControl.innerHTML = form_element.notnullerror;
					        masterErrorMsg += " - " + form_element.notnullerror + "\n";
				        }
				    }
				}
				else
				{
				    var errorDisplayControl = document.getElementById(form_element.errorcontrolid);
				    if(errorDisplayControl){
				        errorDisplayControl.style.color = "Red";
					    errorDisplayControl.style.fontSize = 10 + "px";
				        if( validateField(form_element, "notnull" ))
				        {
				            if (rule != "" && !validateField(form_element, rule)) 
				            {
					            masterErrorMsg += " - " + error + "\n";
					            errorDisplayControl.innerHTML = error;
				            }
				            else
				            {
				                errorDisplayControl.innerHTML = "";
				            }
				        }
				     }
				}
			}
		}
	}
	
	// This checks the current error message against the default,
	// if it has been changed (which means an error occurred) then
	// it displays the error and then returns false to prevent the
	// form from being submitted.
	if (masterErrorMsg != defaultErrorPrefix) {
		// Consider implementing customisable pop-up window
		// here rather than basic alert.
		// Customise window title, colours
		//alert(masterErrorMsg);
		return false;
	}
	// If the current error message is the same as the default,
	// then that means nothing has gone wrong, so just return
	// true and allow the form to be submitted and be processed
	// as per normal.
	else {
		return true;
	}
}


// Returns true or false based on whether the specified string is found
// in the array.
// This is based on the PHP function of the same name.
function in_array(stringToSearch, arrayToSearch) {
	for (s = 0; s < arrayToSearch.length; s++) {
		thisEntry = arrayToSearch[s];
		if (thisEntry.indexOf(stringToSearch) != -1) {
			return true;
			exit;
		}
	}
	return false;
}



//this function check the specified field whether it is empty or not
// send the whole object of that control and errorDisplayControl 
function isEmptyField(controlValue,errorMessage,errorDisplayControl)
{

       if(controlValue.value == "")
	   {	        
	        errorDisplayControl.style.color = "Red";
		    errorDisplayControl.style.fontSize = 10 + "px"; 		  
		    errorDisplayControl.innerHTML = errorMessage;   
		    return true;      
	           
	   }
	  else
	  {
	         errorDisplayControl.innerHTML = "";
	         return false;
	  }
} 

//this function check the specified field whether it is valid or not to given rule
// send the whole object of that control and errorDisplayControl 
function isNotValidField(controlValue,rule,errorMessage,errorDisplayControl)
{
    if( validateField(controlValue, "notnull" ))
    {
        if (rule != "" && !validateField(controlValue, rule)) 
        {	  
            errorDisplayControl.style.color = "Red";
		    errorDisplayControl.style.fontSize = 10 + "px";     
	        errorDisplayControl.innerHTML = errorMessage;
	        return true;
        }
        else
        {
            errorDisplayControl.innerHTML = "";
            return false;
        }
    }
}

function validate_int_only(e) {
    var keynum;
    var keychar;
    var numcheck;
    if (window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }
    keychar = String.fromCharCode(keynum);
    if (keynum >= 48 && keynum <= 57 || keynum >= 37 && keynum <= 40 || keynum == 8 || keynum == 9 || keynum == 18 || keynum == 20 || keynum == 46 || keynum == 144) 
    {
        // ASCII 48 - 57 => 0-9
        // ASCII 37 - 40 => arrow keys
        // ASCII 8, 9, 18, 20, 46, 144 => backspace, tab, alt, Caps Lock , delete, NumLock
        return true;
    }
    else 
    {
        alert("Only integers are allowed");
        return false;
    }
}

function validate_char_only(e) 
{
    var keynum;
    var keychar;
    var numcheck;
    if (window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }
    keychar = String.fromCharCode(keynum);
    if (keynum >= 65 && keynum <= 90 || keynum >= 97 && keynum <= 122 || keynum >= 37 && keynum <= 40 || keynum == 8 || keynum == 9 || keynum == 18 || keynum == 20 || keynum == 46 || keynum == 144) 
    {
        // ASCII 65 - 90 => A-Z
        // ASCII 97 - 122 => a-z
        // ASCII 37 - 40 => arrow keys
        // ASCII 8, 9, 18, 20, 46, 144 => backspace, tab, alt, Caps Lock , delete, NumLock
        return true;
    }
    else 
    {
        alert("Only chars are allowed");
        return false;
    }
}

function inlineMsg(msg,msgcontent,string,autohide) {
        var msg;
        var msgcontent;
        msg.className = 'msg';
        msgcontent.className = 'msgcontent';
        msg.appendChild(msgcontent);
        msg.style.filter = 'alpha(opacity=0)';
        msg.style.opacity = 0;
        msg.alpha = 0;
        msgcontent.innerHTML = string;
        msg.style.display = 'block';
        var msgheight = msgcontent.offsetHeight;
        var targetdiv = document.getElementById(msgcontent);
        clearInterval(msg.timer);
        msg.timer = setInterval("fadeMsg(1,'"+ msg.id +"')", MSGTIMER);
        if(!autohide) {
            autohide = MSGHIDE;  
        }
        window.setTimeout("hideMsg('"+ msgcontent.id +"')", (autohide * 20000));
}

// hide the form alert //
function hideMsg(id) {
  var msg = document.getElementById(id);
  if(msg){
      if(!msg.timer) {
        msg.timer = setInterval("fadeMsg(0,'" + msg.id + "')", MSGTIMER);
      }
  }
}

// face the message box //
function fadeMsg(flag,id) {

  if(flag == null) {
    flag = 1;
  }
  var msg = document.getElementById(id);
  if(msg)
  {
      var value;
        if(flag == 1) {
           value = msg.alpha + MSGSPEED ;
      } else {
            value = msg.alpha - MSGSPEED;
      }
      msg.alpha = value;
      msg.style.opacity = (value / 100);
     
      msg.style.filter = 'alpha(opacity=' + value + ')';
      if(value >= 5000 )
       {
         clearInterval(msg.timer);
         msg.style.display = "none";
         msg.timer = null;
       } 
       else if(value <= 1)
        {
         msg.style.display = "none";
         clearInterval(msg.timer);
      }
  }
}

// calculate the position of the element in relation to the left of the browser //
function leftPosition(target) {
  var left = 0;
  if(target){
      if(target.offsetParent) {
        while(1) {
          left += target.offsetLeft;
          if(!target.offsetParent) {
            break;
          }
          target = target.offsetParent;
        }
      } else if(target.x) {
        left += target.x;
      }
  }
  return left;
}

// calculate the position of the element in relation to the top of the browser window //
function topPosition(target)
 {
 if(target){
       var top = 0;
       if(target.offsetParent)
	     {
           while(1)
		    {
              top += target.offsetTop;
              if(!target.offsetParent) 
			    {
                 break;
                }
             target = target.offsetParent;
            }
         }
      else if(target.y)
	    {
         top += target.y;
        }
     }
   return top;
}

// preload the arrow //
if(document.images) {
  arrow = new Image(7,80); 
  arrow.src = "App_Themes/HirePro/images/msg_arrow.gif"; 
}




