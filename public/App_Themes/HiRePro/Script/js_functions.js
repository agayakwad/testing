function showup(id_of_link,id_of_tab,id_of_link_timeout,id_of_tab_timeout)
{
	document.getElementById('secondlevel_one').style.display='none';
	document.getElementById('secondlevel_two').style.display='none';
	document.getElementById('secondlevel_three').style.display='none';
	document.getElementById('secondlevel_four').style.display='none';
	document.getElementById('secondlevel_five').style.display='none';
	document.getElementById('secondlevel_six').style.display='none';
	
	
	document.getElementById(id_of_link).style.display='block';	
	
	document.getElementById('mainlvl_Dashboard').className='empty';
	document.getElementById('mainlvl_Admin').className='empty';
	document.getElementById('mainlvl_Candidate').className='empty';
	document.getElementById('mainlvl_Requisition').className='empty';
	document.getElementById('mainlvl_Source').className='empty';
	document.getElementById('mainlvl_Organization').className='empty';
	
	document.getElementById(id_of_tab).className='selected';
	
if(id_of_link!=id_of_link_timeout)
{
	setTimeout(function () {
showup(id_of_link_timeout, id_of_tab_timeout,id_of_link_timeout, id_of_tab_timeout);
}, 15000);
}

}

function hidden_p_unhide(id_of_p) {	
	for (var x = 1; x <= 50; x++)
  {
  	 if (document.getElementById('hidden_p_'+x) != null) 
  	 {
   		this.document.getElementById('hidden_p_'+x).style.display='none';
   	 }
   	 else
   	 {
   		break;
   	 }
  }	
	document.getElementById(id_of_p).style.display='block';	
	
}

function getElementsByClassName(oElm, strTagName, oClassNames){
	var arrElements = (strTagName == "*" && oElm.all)? oElm.all : oElm.getElementsByTagName(strTagName);
	var arrReturnElements = new Array();
	var arrRegExpClassNames = new Array();
	if(typeof oClassNames == "object"){
		for(var i=0; i<oClassNames.length; i++){
			arrRegExpClassNames.push(new RegExp("(^|\\s)" + oClassNames[i].replace(/\-/g, "\\-") + "(\\s|$)"));
		}
	}
	else{
		arrRegExpClassNames.push(new RegExp("(^|\\s)" + oClassNames.replace(/\-/g, "\\-") + "(\\s|$)"));
	}
	var oElement;
	var bMatchesAll;
	for(var j=0; j<arrElements.length; j++){
		oElement = arrElements[j];
		bMatchesAll = true;
		for(var k=0; k<arrRegExpClassNames.length; k++){
			if(!arrRegExpClassNames[k].test(oElement.className)){
				bMatchesAll = false;
				break;
			}
		}
		if(bMatchesAll){
			arrReturnElements.push(oElement);
		}
	}
	return (arrReturnElements)
}

function hide_unhide_object()
{
	var matches = getElementsByClassName(document, "p", "transport_details");
	for (var i = 0, j = matches.length; i < j; i++) 
	{	
		 if(matches[i].style.display == 'block')
  	 		matches[i].style.display = 'none';
  	 else
  	 		matches[i].style.display = 'block';
  	 		
	}

}


var toggle = 0;
var runTime = 0;
function lblError_ChangeColor() {
    var lblError = document.getElementById("ctl00_lblError");

    if (lblError != null) {
        if (toggle == 0) {
            lblError.style.color = "#82a5e9";
            toggle = 1;
            setTimeout(lblError_ChangeColor, 400);
        }
        else if (toggle == 1) {
            lblError.style.color = "#ff0000";
            toggle = 0;
            setTimeout(lblError_ChangeColor, 1000);
        }
        runTime++;
        if (runTime >= 20) {
            runTime = 0;
            toggle = 2;
        }
    }


}

function enableLoading() {
    var loadingDiv = document.getElementById("LoadingDivTag");
    loadingDiv.style.display = "block";
    loadingDiv.innerHTML = "L o a d i n g";
    var lblError = document.getElementById("ctl00_lblError");
    if (lblError != null) {
        lblError.innerHTML = String.Empty;
    }
    rotateLoading();
}

function rotateLoading() {
    var loadingDiv = document.getElementById("LoadingDivTag");
    if (loadingDiv.innerHTML == "L o a d i n g") {
        loadingDiv.innerHTML = "L o a d i n g .";
    }
    else if (loadingDiv.innerHTML == "L o a d i n g .") {
        loadingDiv.innerHTML = "L o a d i n g . .";
    }
    else if (loadingDiv.innerHTML == "L o a d i n g . .") {
        loadingDiv.innerHTML = "L o a d i n g . . .";
    }
    else if (loadingDiv.innerHTML == "L o a d i n g . . .") {
        loadingDiv.innerHTML = "L o a d i n g . . . .";
    }
    else if (loadingDiv.innerHTML == "L o a d i n g . . . .") {
        loadingDiv.innerHTML = "L o a d i n g . . . . .";
    }
    else if (loadingDiv.innerHTML == "L o a d i n g . . . . .") {
        loadingDiv.innerHTML = "L o a d i n g";
    }
    setTimeout(rotateLoading, 300);
}



function ShowViewStateSize() {
    var buf = document.forms[0]["__VIEWSTATE"].value;
    alert("View state is " + buf.length + " bytes");
}


function Hide(Cell) {
    Cell.style.display = "none"
}

function Show(Cell) {
    Cell.style.display = String.Empty
}
function ChangeCell(Cell) {
    Cell.style.background = "gainsboro"
}
function ChangeBack(Cell) {
    Cell.style.background = "none"
}
function RolloverImage(e) {
    try {
        e = e || window.event;

        var image;
        if (e.srcElement)
            image = e.srcElement;
        else
            image = e.target;

        //Separate the source into a path and a file name.
        var sourcePath = GetPath(image.src);  //

        if (e.type == 'mouseout')
            image.src = sourcePath + image.getAttribute('image');
        else
            image.src = sourcePath + image.getAttribute('rolloverImage');
    }
    catch (ex) {
        alert("Error occurred: [RolloverImage]\n\n" + ex.message);
    }
}

function GetPath(pathAndFile) {
    var index = pathAndFile.lastIndexOf('/');
    if (index > -1)
        return pathAndFile.substring(0, index + 1);
    else
        return String.Empty;
}
function openNewWindow(filePath) {
    var confirmWin = null;
    confirmWin = window.open(filePath, 'anycontent', 'width=600,height=500,resizable=yes, scrollbars=yes');
    if ((confirmWin != null) && (confirmWin.opener == null)) {
        confirmWin.opener = self;
    }
}

function OnClientResizing(sender, eventArgs) {
    var e = sender.get_element();
    e.parentNode.style.width = e.style.width;

}
function OnLoadCommonFunction() {
    lblError_ChangeColor();
    var PreviousPagePath = document.cookie;
    var CurrentPagePath = document.URL;
    }

var Tooltip = {
    followMouse: true,
    offX: 8,
    offY: 12,
    tipID: "tipDiv",
    showDelay: 100,
    hideDelay: 200,

    ready: false, timer: null, tip: null,

    init: function() {
        if (document.createElement && document.body && typeof document.body.appendChild != "undefined") {
            if (!document.getElementById(this.tipID)) {
                var el = document.createElement("DIV");
                el.id = this.tipID; document.body.appendChild(el);
            }
            this.ready = true;
        }
    },

    show: function(e, msg) {
        if (this.timer) { clearTimeout(this.timer); this.timer = 0; }
        this.tip = document.getElementById(this.tipID);
        if (this.followMouse) // set up mousemove 
            dw_event.add(document, "mousemove", this.trackMouse, true);
        this.writeTip(String.Empty);  // for mac ie
        this.writeTip(msg);
        viewport.getAll();
        this.positionTip(e);
        this.timer = setTimeout("Tooltip.toggleVis('" + this.tipID + "', 'visible')", this.showDelay);
    },

    writeTip: function(msg) {
        if (this.tip && typeof this.tip.innerHTML != "undefined") this.tip.innerHTML = msg;
    },

    positionTip: function(e) {
        if (this.tip && this.tip.style) {
            // put e.pageX/Y first! (for Safari)
            var x = e.pageX ? e.pageX : e.clientX + viewport.scrollX;
            var y = e.pageY ? e.pageY : e.clientY + viewport.scrollY;

            if (x + this.tip.offsetWidth + this.offX > viewport.width + viewport.scrollX) {
                x = x - this.tip.offsetWidth - this.offX;
                if (x < 0) x = 0;
            } else x = x + this.offX;

            if (y + this.tip.offsetHeight + this.offY > viewport.height + viewport.scrollY) {
                y = y - this.tip.offsetHeight - this.offY;
                if (y < viewport.scrollY) y = viewport.height + viewport.scrollY - this.tip.offsetHeight;
            } else y = y + this.offY;

            this.tip.style.left = x + "px"; this.tip.style.top = y + "px";
        }
    },

    hide: function() {
        if (this.timer) { clearTimeout(this.timer); this.timer = 0; }
        this.timer = setTimeout("Tooltip.toggleVis('" + this.tipID + "', 'hidden')", this.hideDelay);
        if (this.followMouse) // release mousemove
            dw_event.remove(document, "mousemove", this.trackMouse, true);
        this.tip = null;
    },

    toggleVis: function(id, vis) { // to check for el, prevent (rare) errors
        var el = document.getElementById(id);
        if (el) el.style.visibility = vis;
    },

    trackMouse: function(e) {
        e = dw_event.DOMit(e);
        Tooltip.positionTip(e);
    }

}



var dw_event = {

    add: function(obj, etype, fp, cap) {
        cap = cap || false;
        if (obj.addEventListener) obj.addEventListener(etype, fp, cap);
        else if (obj.attachEvent) obj.attachEvent("on" + etype, fp);
    },

    remove: function(obj, etype, fp, cap) {
        cap = cap || false;
        if (obj.removeEventListener) obj.removeEventListener(etype, fp, cap);
        else if (obj.detachEvent) obj.detachEvent("on" + etype, fp);
    },

    DOMit: function(e) {
        e = e ? e : window.event;
        e.tgt = e.srcElement ? e.srcElement : e.target;

        if (!e.preventDefault) e.preventDefault = function() { return false; }
        if (!e.stopPropagation) e.stopPropagation = function() { if (window.event) window.event.cancelBubble = true; }

        return e;
    }

}



var viewport = {
    getWinWidth: function() {
        this.width = 0;
        if (window.innerWidth) this.width = window.innerWidth - 18;
        else if (document.documentElement && document.documentElement.clientWidth)
            this.width = document.documentElement.clientWidth;
        else if (document.body && document.body.clientWidth)
            this.width = document.body.clientWidth;
    },

    getWinHeight: function() {
        this.height = 0;
        if (window.innerHeight) this.height = window.innerHeight - 18;
        else if (document.documentElement && document.documentElement.clientHeight)
            this.height = document.documentElement.clientHeight;
        else if (document.body && document.body.clientHeight)
            this.height = document.body.clientHeight;
    },

    getScrollX: function() {
        this.scrollX = 0;
        if (typeof window.pageXOffset == "number") this.scrollX = window.pageXOffset;
        else if (document.documentElement && document.documentElement.scrollLeft)
            this.scrollX = document.documentElement.scrollLeft;
        else if (document.body && document.body.scrollLeft)
            this.scrollX = document.body.scrollLeft;
        else if (window.scrollX) this.scrollX = window.scrollX;
    },

    getScrollY: function() {
        this.scrollY = 0;
        if (typeof window.pageYOffset == "number") this.scrollY = window.pageYOffset;
        else if (document.documentElement && document.documentElement.scrollTop)
            this.scrollY = document.documentElement.scrollTop;
        else if (document.body && document.body.scrollTop)
            this.scrollY = document.body.scrollTop;
        else if (window.scrollY) this.scrollY = window.scrollY;
    },

    getAll: function() {
        this.getWinWidth(); this.getWinHeight();
        this.getScrollX(); this.getScrollY();
    }

}

function doTooltip(e, ele, msg) {
    var el1 = document.getElementById(ele);
    el1.style.backgroundColor = '#cccccc';
    if (typeof Tooltip == "undefined" || !Tooltip.ready) return;
    Tooltip.show(e, msg);
}

function hideTip(ele1, ulternateRow) {
    var el2 = document.getElementById(ele1);
    if (ulternateRow == 'Normal') {
        el2.style.backgroundColor = '#EFF3FB';
    }
    else {
        el2.style.backgroundColor = '#ffffff';
    }
    if (typeof Tooltip == "undefined" || !Tooltip.ready) return;
    Tooltip.hide();
}

var rowColour;
function igtbl_MouseOverHandler(gridName, id, type) {
    var cell = igtbl_getCellById(id);
    var thisRow = cell.getRow();
    rowColour = thisRow.Element.style.backgroundColor;
    thisRow.Element.style.backgroundColor = '#ebebfa';
}

function igtbl_MouseOutHandler(gridName, id, type) {
    var cell = igtbl_getCellById(id);
    var thisRow = cell.getRow();
    thisRow.Element.style.backgroundColor = rowColour;
}


function SwitchMenuReport() {

    if (document.getElementById) {

        var el1 = document.getElementById('CandidateReportDiv');

        if (el1.style.display != "block") {
            el1.style.display = "block";
        }
        else {
            el1.style.display = "none";
        }
    }
}

function SwitchMenuReport1() {
    if (document.getElementById) {
        var el1 = document.getElementById('JobReportDiv');
        if (el1.style.display != "block") {
            el1.style.display = "block";
        }
        else {
            el1.style.display = "none";
        }
    }
}

function SwitchMenuTags() {
    if (document.getElementById) {
        var el1 = document.getElementById('TagsDiv');
        if (el1.style.display != "block") {
            el1.style.display = "block";
        }
        else {
            el1.style.display = "none";
        }
    }
}
function ToggleSearch() {
    if (document.getElementById) {
        var el1 = document.getElementById('Toggle');
        if (el1.style.display != 'block') {
            el1.style.display = 'block';
        }
        else {
            el1.style.display = 'none';
        }
    }
}
function ToggleReverse() {
    if (document.getElementById) {
        var el1 = document.getElementById('Toggle');
        el1.style.display = 'none';
    }
}

//the function for changing the name of the bar on click
function name_change(val) {
    document.getElementById('ctl00_CanvasPanel').innerHTML = val;
}

function TabLinkButton_MouseOver(obj) {
    var getButton = document.getElementById('ctl00_' + obj);
    if (getButton.style.backgroundImage.indexOf('x1') > 0) {
       
    }
    else {
        getButton.style.backgroundImage = 'url(App_Themes/HireProNew/images/hover_center.gif)';
        getButton.style.backgroundPosition = "center";
       	
        getButton.style.backgroundRepeat = 'repeat-x';
    }

}

function TabLinkButton_MouseOut(obj, name) {
    var getButton = document.getElementById('ctl00_' + obj);
    if (getButton.style.backgroundImage.indexOf('x1') > 0) {
       
    }
    else {
        getButton.style.backgroundImage = 'url(App_Themes/HireProNew/images/hi_topmenu_x.gif)';
        getButton.style.backgroundPosition = "0% 0%";
        getButton.style.backgroundRepeat = 'repeat';
    }

}

function CheckAllRows(gridId) {
    var grid = igtbl_getGridById(gridId);
    var checkBox = document.getElementById('selectAllCheckBox');
    if (checkBox != null)
        if (checkBox.checked == true) {
        for (i = 0; i < grid.Rows.length; i++) {
            var cell = grid.Rows.getRow(i).getCell(0);
            if (cell != null)
                cell.setValue(true);
        }
    }
    else {
        for (i = 0; i < grid.Rows.length; i++) {
            var cell = grid.Rows.getRow(i).getCell(0);
            if (cell != null)
                cell.setValue(false);
        }
    }
}

function EnableDisableButton(buttonId, gridId) {
    var count = document.forms[0].length;
    var i = 0;
    var eleName;
    for (i = 0; i < count; i++) {
        eleName = document.forms[0].elements[i].id;
        pos = eleName.indexOf(buttonId);
        if (pos >= 0) break;
    }
    var grid = igtbl_getGridById(gridId);
    var flag = false;
    for (i = 0; i < grid.Rows.length; i++) {
        var cellValue = grid.Rows.getRow(i).getCell(0).getValue();
        if (cellValue) {
            flag = true;
            break;
        }
    }
    if (flag) {
        document.getElementById(eleName).disabled = false;
    }
    else {
        document.getElementById(eleName).disabled = true;
    }
}
function FindReportName(_radioList, j) {
    var name = document.getElementById('ctl00_ReportHiddenField');
    var _list = document.getElementById(_radioList);

    for (var i = 0; i < j; i++) {
        if (_list.rows[i].cells[0].innerHTML.indexOf('CHECKED') != -1) {
            name.value = _list.rows[i].cells[0].innerHTML;
            break;
        }
    }



}

function setUserControlUrlHiddenField(url, id) {
    var hiddenFieldForUrl = document.getElementById('ctl00_UserControlUrlHiddenField');
    var hiddenFieldForId = document.getElementById('ctl00_UserControlIdHiddenField');
    hiddenFieldForUrl.value = url;
    hiddenFieldForId.value = id;
    __doPostBack('Default.aspx', '');
}
            